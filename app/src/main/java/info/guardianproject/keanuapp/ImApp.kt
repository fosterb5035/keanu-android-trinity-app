package info.guardianproject.keanuapp

import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Configuration
import android.os.Build
import android.os.Handler
import android.os.Message
import android.os.RemoteException
import android.text.TextUtils
import androidx.multidex.MultiDexApplication
import androidx.preference.PreferenceManager
import com.vanniktech.emoji.EmojiManager
import com.vanniktech.emoji.google.GoogleEmojiProvider
import info.guardianproject.keanu.core.KeanuConstants
import info.guardianproject.keanu.core.Preferences
import info.guardianproject.keanu.core.cacheword.ICacheWordSubscriber
import info.guardianproject.keanu.core.cacheword.PRNGFixes
import info.guardianproject.keanu.core.service.Broadcaster
import info.guardianproject.keanu.core.service.ImServiceConstants
import info.guardianproject.keanu.core.service.RemoteImService
import info.guardianproject.keanu.core.service.StatusBarNotifier
import info.guardianproject.keanu.core.util.Debug
import info.guardianproject.keanu.core.util.Languages
import info.guardianproject.keanu.core.verification.VerificationManager
import org.cleaninsights.sdk.CleanInsights
import org.matrix.android.sdk.api.Matrix
import org.matrix.android.sdk.api.MatrixConfiguration
import org.matrix.android.sdk.api.failure.GlobalError
import org.matrix.android.sdk.api.session.Session
import timber.log.Timber
import java.util.*

class ImApp : MultiDexApplication(), ICacheWordSubscriber, Session.Listener {

    companion object {
        const val URL_UPDATER = "https://gitlab.com/keanuapp/keanuapp-android/-/raw/master/appupdater.xml"

        var sImApp: ImApp? = null

        const val EVENT_SERVICE_CONNECTED = 100
        const val EVENT_CONNECTION_CREATED = 150
        const val EVENT_CONNECTION_LOGGING_IN = 200
        const val EVENT_CONNECTION_LOGGED_IN = 201
        const val EVENT_CONNECTION_LOGGING_OUT = 202
        const val EVENT_CONNECTION_DISCONNECTED = 203
        const val EVENT_CONNECTION_SUSPENDED = 204
        const val EVENT_USER_PRESENCE_UPDATED = 300
        const val EVENT_UPDATE_USER_PRESENCE_ERROR = 301

        @JvmStatic
        fun resetLanguage(activity: Activity?, language: String?) {
            if (!TextUtils.equals(language, Preferences.getLanguage())) {
                // Set the preference after setting the locale in case something goes
                // wrong. If setting the locale causes an Exception, it should not be set in
                // the preferences, otherwise this will be stuck in a crash loop.
                Languages.setLanguage(activity, language, true)
                Preferences.setLanguage(language)
                Languages.forceChangeLanguage(activity)
            }
        }
    }

    private var mBroadcaster: Broadcaster? = null

    /**
     * A queue of messages that are waiting to be sent when service is
     * connected.
     */
    private var mQueue = ArrayList<Message>()

    private lateinit var mApplicationContext: Context

    lateinit var cleanInsights: CleanInsights
        private set

    private val mPrefs: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(this)

    fun isRoomInForeground(roomId: String): Boolean {
        return roomId == currentForegroundRoom
    }

    @JvmField
    var currentForegroundRoom = ""

    lateinit var matrix: Matrix
        private set

    var matrixSession: Session? = null
        set(value) {
            field = value

            if (value != null) {
                startSession()
                startService()
            }
        }


    private fun initChannel() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) return

        val nm = getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        var nc = NotificationChannel(KeanuConstants.NOTIFICATION_CHANNEL_ID_SERVICE, getString(R.string.app_name), NotificationManager.IMPORTANCE_MIN)
        nc.setShowBadge(false)

        nm.createNotificationChannel(nc)

        nc = NotificationChannel(KeanuConstants.NOTIFICATION_CHANNEL_ID_MESSAGE, getString(R.string.notifications), NotificationManager.IMPORTANCE_HIGH)
        nc.lightColor = -0x670000
        nc.enableLights(true)
        nc.enableVibration(false)
        nc.setShowBadge(true)
        nc.setSound(null, null)

        nm.createNotificationChannel(nc)
    }

    override fun getContentResolver(): ContentResolver {
        return if (mApplicationContext === this) {
            super.getContentResolver()
        }
        else {
            mApplicationContext.contentResolver
        }
    }

    override fun onCreate() {
        super.onCreate()

        StatusBarNotifier.defaultMainClass = "info.guardianproject.keanuapp.MainActivity"

        Preferences.setup(this)

        initChannel()

        initMeasurement()

        Languages.setup(MainActivity::class.java, R.string.use_system_default)
        Languages.setLanguage(this, Preferences.getLanguage(), false)

        sImApp = this

        Debug.onAppStart()
        PRNGFixes.apply() //Google's fix for SecureRandom bug: http://android-developers.blogspot.com/2013/08/some-securerandom-thoughts.html
        EmojiManager.install(GoogleEmojiProvider())

        mApplicationContext = this

        mBroadcaster = Broadcaster()

        if (BuildConfig.DEBUG) {
//            StrictMode.setVmPolicy(VmPolicy.Builder().detectAll().penaltyLog().build())
            Timber.plant(Timber.DebugTree())
        }

        val config = MatrixConfiguration()
        Matrix.initialize(this, config)
        matrix = Matrix.getInstance(this)

        matrixSession = matrix.authenticationService().getLastAuthenticatedSession()
    }

    private fun startSession() {
        if (matrixSession == null) return

        matrixSession?.open()
        matrixSession?.addListener(this)
        matrixSession?.startSync(true)
        matrixSession?.startAutomaticBackgroundSync(60L,60L)
        VerificationManager.instance.register(this)

        matrixSession?.getSyncStateLive()?.let {
            Timber.d("Sync State: %s", it.value?.toString())
        }

        matrixSession?.getInitialSyncProgressStatus()?.let {
            Timber.d("Initial Sync State: %s", it.value?.toString())
        }
    }

    private fun startService() {
        val intentService = Intent(this, RemoteImService::class.java)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intentService)
        }
        else {
            startService(intentService)
        }
    }

    private fun initMeasurement() {

        try {
            val inputStream = assets.open("cleanInsights.json")

            cleanInsights = CleanInsights(
                inputStream.reader().readText(),
                filesDir
            )
        }
        catch (ex: Exception)
        {
            // Couldn't find or load config.
            Timber.d("couldn't find cleanInsights config file")
        }
    }

    fun logout() {
        matrixSession?.stopSync()
        matrixSession?.stopAnyBackgroundSync()
        matrixSession?.close()
        matrixSession?.removeListener(this)
        matrixSession = null

        VerificationManager.instance.deregister()
    }

    fun setAppTheme(activity: Activity?) {
        if (mPrefs.getBoolean("themeDark", false)) {
            setTheme(R.style.AppThemeDark)
            activity?.setTheme(R.style.AppThemeDark)
        }
        else {
            setTheme(R.style.AppTheme)
            activity?.setTheme(R.style.AppTheme)
        }

        resources.updateConfiguration(resources.configuration, resources.displayMetrics)

        if (RemoteImService.mImService != null) {
            try {
                RemoteImService.mImService.enableDebugLogging(
                        mPrefs.getBoolean("prefDebug", false))
            }
            catch (e: RemoteException) {
                e.printStackTrace()
            }
        }
    }

    fun startImServiceIfNeed() {
        Timber.d("start ImService")

        val intent = Intent(applicationContext, RemoteImService::class.java)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mApplicationContext.startForegroundService(intent)
        }
        else {
            mApplicationContext.startService(intent)
        }

        for (msg in mQueue) {
            msg.sendToTarget()
        }
        mQueue.clear()

        mBroadcaster?.broadcast(Message.obtain(null, EVENT_SERVICE_CONNECTED))
    }

    fun forceStopImService() {
        Timber.d("stop ImService")

        try {
            RemoteImService.mImService?.shutdownAndLock()
        }
        catch (ignored: RemoteException) {
        }

        val intent = Intent(this, RemoteImService::class.java)
        intent.putExtra(ImServiceConstants.EXTRA_CHECK_AUTO_LOGIN, true)
        mApplicationContext.stopService(intent)
    }

    val isServiceConnected: Boolean
        get() = RemoteImService.mImService != null

    fun registerForBroadcastEvent(what: Int, target: Handler?) {
        mBroadcaster?.request(what, target, what)
    }

    fun unregisterForBroadcastEvent(what: Int, target: Handler?) {
        mBroadcaster?.cancelRequest(what, target, what)
    }

    fun registerForConnEvents(handler: Handler?) {
        mBroadcaster?.request(EVENT_CONNECTION_CREATED, handler, EVENT_CONNECTION_CREATED)
        mBroadcaster?.request(EVENT_CONNECTION_LOGGING_IN, handler, EVENT_CONNECTION_LOGGING_IN)
        mBroadcaster?.request(EVENT_CONNECTION_LOGGED_IN, handler, EVENT_CONNECTION_LOGGED_IN)
        mBroadcaster?.request(EVENT_CONNECTION_LOGGING_OUT, handler, EVENT_CONNECTION_LOGGING_OUT)
        mBroadcaster?.request(EVENT_CONNECTION_SUSPENDED, handler, EVENT_CONNECTION_SUSPENDED)
        mBroadcaster?.request(EVENT_CONNECTION_DISCONNECTED, handler, EVENT_CONNECTION_DISCONNECTED)
        mBroadcaster?.request(EVENT_USER_PRESENCE_UPDATED, handler, EVENT_USER_PRESENCE_UPDATED)
        mBroadcaster?.request(EVENT_UPDATE_USER_PRESENCE_ERROR, handler,
                EVENT_UPDATE_USER_PRESENCE_ERROR)
    }

    fun unregisterForConnEvents(handler: Handler?) {
        mBroadcaster?.cancelRequest(EVENT_CONNECTION_CREATED, handler, EVENT_CONNECTION_CREATED)
        mBroadcaster?.cancelRequest(EVENT_CONNECTION_LOGGING_IN, handler,
                EVENT_CONNECTION_LOGGING_IN)
        mBroadcaster?.cancelRequest(EVENT_CONNECTION_LOGGED_IN, handler, EVENT_CONNECTION_LOGGED_IN)
        mBroadcaster?.cancelRequest(EVENT_CONNECTION_LOGGING_OUT, handler,
                EVENT_CONNECTION_LOGGING_OUT)
        mBroadcaster?.cancelRequest(EVENT_CONNECTION_SUSPENDED, handler, EVENT_CONNECTION_SUSPENDED)
        mBroadcaster?.cancelRequest(EVENT_CONNECTION_DISCONNECTED, handler,
                EVENT_CONNECTION_DISCONNECTED)
        mBroadcaster?.cancelRequest(EVENT_USER_PRESENCE_UPDATED, handler,
                EVENT_USER_PRESENCE_UPDATED)
        mBroadcaster?.cancelRequest(EVENT_UPDATE_USER_PRESENCE_ERROR, handler,
                EVENT_UPDATE_USER_PRESENCE_ERROR)
    }

    fun dismissChatNotification(providerId: Long, username: String?) {
        if (RemoteImService.mImService != null) {
            try {
                RemoteImService.mImService.dismissChatNotification(providerId, username)
            }
            catch (ignored: RemoteException) {
            }
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)

        Languages.setLanguage(this, Preferences.getLanguage(), true)
    }

    override fun onCacheWordUninitialized() {
        // unused
    }

    override fun onCacheWordLocked() {
        // unused
    }

    override fun onCacheWordOpened() {
        // TODO?
    }

    override fun onGlobalError(globalError: GlobalError) {
        Timber.d("Error on Session: %s", globalError)
    }
}