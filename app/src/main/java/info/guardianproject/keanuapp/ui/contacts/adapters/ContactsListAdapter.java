package info.guardianproject.keanuapp.ui.contacts.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;

import org.matrix.android.sdk.api.session.Session;
import org.matrix.android.sdk.api.session.content.ContentUrlResolver;
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams;
import org.matrix.android.sdk.api.session.room.model.RoomSummary;
import org.matrix.android.sdk.api.session.user.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import info.guardianproject.keanuapp.ImApp;
import info.guardianproject.keanuapp.R;
import info.guardianproject.keanuapp.ui.contacts.ContactDisplayActivity;
import info.guardianproject.keanuapp.ui.contacts.ContactListItem;
import info.guardianproject.keanuapp.ui.contacts.ContactViewHolder;

public class ContactsListAdapter implements ListAdapter {

    private Activity mContext;
    private Session mSession;
    private ArrayList<User> mContactList = new ArrayList<>();
    List<RoomSummary> mListRoomSummaries;
    private String mFilter = null;

    public ContactsListAdapter (Activity context, String filter)
    {
        mContext = context;
        mFilter = filter;
        initData();
    }

    public void setFilter (String filter)
    {
        mFilter = filter;
        initData();
    }

    private void initData () {
        mSession = ((ImApp)mContext.getApplication()).getMatrixSession();

        RoomSummaryQueryParams.Builder builder = new RoomSummaryQueryParams.Builder();

        if (mListRoomSummaries == null)
            mListRoomSummaries = mSession.getRoomSummaries(builder.build());

        HashMap<String, User> userMap = new HashMap<>();

        for (RoomSummary rs : mListRoomSummaries)
        {
            if (rs.isDirect()||(rs.getJoinedMembersCount()<=2&&rs.getOtherMemberIds().size()>0)) {

                if (!rs.getOtherMemberIds().isEmpty()) {

                    if (!TextUtils.isEmpty(mFilter))
                        if (!rs.getDisplayName().contains(mFilter))
                            continue;

                    userMap.put(rs.getOtherMemberIds().get(0), mSession.getUser(rs.getOtherMemberIds().get(0)));
                }
            }
        }

        mContactList = new ArrayList<>(userMap.values());


    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return mContactList.size();
    }

    @Override
    public User getItem(int position) {
        return mContactList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_view, parent, false);

        ContactViewHolder viewHolder = (ContactViewHolder)view.getTag();

        if (viewHolder == null) {
            viewHolder = new ContactViewHolder(view);
            view.setTag(viewHolder);
        }

        User user = mContactList.get(position);
        String avatarUrl = null;


        final String nickname = user.getDisplayName();
        final String address = user.getUserId();

        avatarUrl = user.getAvatarUrl();
        avatarUrl = mSession.contentUrlResolver().resolveThumbnail(avatarUrl,120,120, ContentUrlResolver.ThumbnailMethod.SCALE);

        ContactListItem clItem = ((ContactListItem) viewHolder.itemView);

        clItem.bind(viewHolder,address,nickname,avatarUrl,null,null);


        return view;
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return mContactList.isEmpty();
    }
}
