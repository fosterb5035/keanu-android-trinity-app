package info.guardianproject.keanuapp.ui.accounts;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.jetbrains.annotations.NotNull;
import org.matrix.android.sdk.api.MatrixCallback;
import org.matrix.android.sdk.api.session.content.ContentUrlResolver;
import org.matrix.android.sdk.api.session.user.model.User;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import info.guardianproject.keanu.core.util.SecureMediaStore;
import info.guardianproject.keanuapp.ImApp;
import info.guardianproject.keanuapp.R;
import info.guardianproject.keanuapp.ui.contacts.DevicesActivity;
import info.guardianproject.keanu.core.util.GlideUtils;
import kotlin.Unit;

import static android.content.Context.CLIPBOARD_SERVICE;
import static info.guardianproject.keanu.core.KeanuConstants.LOG_TAG;

@SuppressLint("LogNotTimber")
public class AccountFragment extends Fragment {

    private final static String DEFAULT_PASSWORD_TEXT = "*************";
    private final static int MY_PERMISSIONS_REQUEST_CAMERA = 1;
    ImageView mIvAvatar;
    CropImageView mCropImageView;
    TextView mTvPassword, mTvNickname, mTvUsername;
    View mView;
    String mMyUserId;
    String mNickname;
    ImApp mApp;

    public AccountFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Activity a = getActivity();
        if (a == null) return;

        mApp = ((ImApp) a.getApplication());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.awesome_fragment_account, container, false);

        mTvNickname = mView.findViewById(R.id.tvNickname);

        mTvUsername = mView.findViewById(R.id.edtName);
        mTvUsername.setOnLongClickListener(v -> {
            Activity a = getActivity();
            if (a == null) return false;

            ClipboardManager clipboard = (ClipboardManager) a.getSystemService(CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText(getActivity().getString(R.string.app_name), mMyUserId);
            clipboard.setPrimaryClip(clip);
            Toast.makeText(getActivity(), R.string.action_copied, Toast.LENGTH_SHORT).show();

            return true;
        });

        mTvPassword = mView.findViewById(R.id.edtPass);
        mTvPassword.setText(DEFAULT_PASSWORD_TEXT);

        ImageView btnShowPassword = mView.findViewById(R.id.btnShowPass);
        btnShowPassword.setOnClickListener(view -> {
            if (mTvPassword.getText().toString().equals(DEFAULT_PASSWORD_TEXT)) {
                mTvPassword.setText("");
                btnShowPassword.setImageResource(R.drawable.eye_slash_icon);
            } else {
                mTvPassword.setText(DEFAULT_PASSWORD_TEXT);
                btnShowPassword.setImageResource(R.drawable.ic_visibility_black_24dp);
            }
        });

        View btnEditAccountNickname = mView.findViewById(R.id.edit_account_nickname);
        btnEditAccountNickname.setOnClickListener(view -> showChangeNickname());

        View btnEditAccountPassword = mView.findViewById(R.id.edit_account_password);
        btnEditAccountPassword.setOnClickListener(view -> showChangePassword());

        mIvAvatar = mView.findViewById(R.id.imageAvatar);
        mIvAvatar.setOnClickListener(view -> startAvatarTaker());

        mView.findViewById(R.id.btnViewDevices).setOnClickListener(v -> viewDevicesClicked());

        String version = "Unknown";
        try {
            version = getString(R.string.version_label) + ": " + getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(),0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        ((TextView)mView.findViewById(R.id.tbBuildNumber)).setText(version);

        updateInfo();

        return mView;
    }

    private void updateInfo() {
        mMyUserId = mApp.getMatrixSession().getMyUserId();
        User myUser = mApp.getMatrixSession().getUser(mMyUserId);

        if (myUser != null) mNickname = myUser.getDisplayName();

        if (mTvUsername != null) {
            mTvUsername.setText(mMyUserId);
            mTvNickname.setText(mNickname);

            String avatarUrl = myUser != null ? myUser.getAvatarUrl() : "";

            if (!TextUtils.isEmpty(avatarUrl)) {
                avatarUrl = mApp.getMatrixSession().contentUrlResolver().resolveThumbnail(avatarUrl, 512, 512, ContentUrlResolver.ThumbnailMethod.SCALE);
                GlideUtils.loadImageFromUri(getContext(), Uri.parse(avatarUrl), mIvAvatar);
            }
        }
    }

    private void showChangeNickname() {
        Context c = getContext();
        if (c == null) return;

        AlertDialog.Builder alert = new AlertDialog.Builder(c);

        // Set an EditText view to get user input
        final EditText input = new EditText(getContext());
        input.setText(mNickname);
        alert.setView(input);

        alert.setPositiveButton(getString(R.string.ok), (dialog, whichButton) -> {
            String newNickname = input.getText().toString();

            mApp.getMatrixSession().setDisplayName(mMyUserId, newNickname, new MatrixCallback<Unit>() {
                @Override
                public void onSuccess(Unit unit) {

                }

                @Override
                public void onFailure(@NotNull Throwable throwable) {

                }
            });

            mTvNickname.setText(newNickname);
            // Do something with value!
        });

        alert.setNegativeButton(getString(R.string.cancel), (dialog, whichButton) -> {
            // Canceled.
        });

        alert.show();
    }

    private void showChangePassword() {
        Context c = getContext();
        if (c == null) return;

        AlertDialog.Builder alert = new AlertDialog.Builder(c);
        alert.setTitle(R.string.lock_screen_create_passphrase);

        // Set an EditText view to get user input
        final EditText input = new EditText(getContext());
        alert.setView(input);

        alert.setPositiveButton(getString(R.string.ok), (dialog, whichButton) -> {
            // TODO
//            String newPassword = input.getText().toString();
//
//            if (!TextUtils.isEmpty(newPassword)) {
//            }
        });

        alert.setNegativeButton(getString(R.string.cancel), (dialog, whichButton) -> {
            // Canceled.
        });

        alert.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == 200) {

            Uri imageUri = getPickImageResultUri(data);

            if (imageUri == null)
                return;

            Context c = getContext();
            if (c == null) return;

            mCropImageView = new CropImageView(c);

            try {
                Bitmap bmpThumbnail = SecureMediaStore.getThumbnailFile(c, imageUri, 512);
                mCropImageView.setImageBitmap(bmpThumbnail);

                // Use the Builder class for convenient dialog construction
                AlertDialog.Builder builder = new AlertDialog.Builder(c);
                builder.setView(mCropImageView)
                        .setPositiveButton(R.string.ok, (dialog, id) -> setAvatar(mCropImageView.getCroppedImage()))
                        .setNegativeButton(R.string.cancel, (dialog, id) -> {
                            // User cancelled the dialog
                        });
                // Create the AlertDialog object and return it
                AlertDialog dialog = builder.create();
                dialog.show();
            } catch (IOException ioe) {
                Log.e(LOG_TAG, "couldn't load avatar", ioe);
            }
        }
    }

    private void setAvatar(Bitmap bmp) {
        BitmapDrawable avatar = new BitmapDrawable(getResources(), bmp);
        mIvAvatar.setImageDrawable(avatar);

        new Thread ()
        {
            public void run ()
            {

                try {
                    File fileTmpAvatar = File.createTempFile("avatar","jpg");

                    FileOutputStream stream = new FileOutputStream(fileTmpAvatar);
                    bmp.compress(Bitmap.CompressFormat.JPEG, 90, stream);
                    mApp.getMatrixSession().updateAvatar(mApp.getMatrixSession().getMyUserId(), Uri.fromFile(fileTmpAvatar), fileTmpAvatar.getName(), new MatrixCallback<Unit>() {
                        @Override
                        public void onSuccess(Unit unit) {

                        }

                        @Override
                        public void onFailure(@NotNull Throwable throwable) {

                        }
                    });

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();

        try {

            // TODO
//            byte[] avatarBytesCompressed = stream.toByteArray();

        } catch (Exception e) {
            Log.w(LOG_TAG, "error loading image bytes", e);
        }
    }

    /**
     * Create a chooser intent to select the source to get image from.<br/>
     * The source can be camera's (ACTION_IMAGE_CAPTURE) or gallery's (ACTION_GET_CONTENT).<br/>
     * All possible sources are added to the intent chooser.
     */
    public Intent getPickImageChooserIntent() {
        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        Activity a = getActivity();
        if (a == null) return null;

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = a.getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, getString(R.string.choose_photos));

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[0]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Activity a = getActivity();
        if (a == null) return null;

        Uri outputFileUri = null;
        File getImage = a.getExternalCacheDir();

        if (getImage != null) {
            getImage = (new File(getImage.getPath(), "pickImageResult.jpg"));
            outputFileUri = FileProvider.getUriForFile(getActivity(),
                    a.getPackageName() + ".provider",
                    getImage);
        }

        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera;

        if (data != null) {
            if (data.getData() == null)
                return getCaptureImageOutputUri();
            else {
                String action = data.getAction();
                isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
                return isCamera ? getCaptureImageOutputUri() : data.getData();
            }
        } else {
            return getCaptureImageOutputUri();
        }
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {
            //Only manually call onResume if fragment is already visible
            //Otherwise allow natural fragment lifecycle to call onResume
            updateInfo();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }

        updateInfo();
    }

    void startAvatarTaker() {
        Activity a = getActivity();
        if (a == null) return;

        int permissionCheck = ContextCompat.checkSelfPermission(a, Manifest.permission.CAMERA);

        if (permissionCheck == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.CAMERA)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                Snackbar.make(mView, R.string.grant_perms, Snackbar.LENGTH_LONG).show();
            } else {
                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            startActivityForResult(getPickImageChooserIntent(), 200);
        }
    }

    public void viewDevicesClicked() {
        Intent intent = new Intent(getActivity(), DevicesActivity.class);
        intent.putExtra("userId", mMyUserId);

        startActivity(intent);
    }
}
