/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package info.guardianproject.keanuapp.ui.contacts;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import info.guardianproject.keanuapp.MainActivity;
import info.guardianproject.keanuapp.R;
import info.guardianproject.keanuapp.ui.contacts.adapters.ContactsRecyclerViewAdapter;


public class ContactsListFragment extends Fragment {

    private ContactsRecyclerViewAdapter mAdapter = null;
    private static RecyclerView mRecView;
    private View mEmptyView;
    String mSearchString = null;

    private int mType = 0;//Imps.Contacts.TYPE_NORMAL;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.awesome_fragment_contacts_list, container, false);

        mRecView = (RecyclerView)view.findViewById(R.id.recyclerView);
        mEmptyView = view.findViewById(R.id.empty_view);
        mEmptyView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity)getActivity()).inviteContact();
            }
        });

        setupRecyclerView(mRecView);



        return view;
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    public int getCurrentType ()
    {
        return mType;
    }

    public void setArchiveFilter (boolean filterAchive)
    {
        //TODO
        /**
       if (filterAchive)
           setType(Imps.Contacts.TYPE_NORMAL | Imps.Contacts.TYPE_FLAG_HIDDEN);
        else
           setType(Imps.Contacts.TYPE_NORMAL);
         **/
    }

    public void setType (int type)
    {
        mType = type;


    }

    public void doSearch (String searchString)
    {
        mSearchString = searchString;


    }

    public int getContactCount ()
    {
        if (mAdapter != null)
            return mAdapter.getItemCount();
        else
            return 1;
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));

        mAdapter = new ContactsRecyclerViewAdapter(this);

        mRecView.setAdapter(mAdapter);

        if (mAdapter.getItemCount() == 0) {
            mRecView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);

        }
        else {
            mRecView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);

        }

        // init swipe to dismiss logic

        ItemTouchHelper swipeToDismissTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(
                ItemTouchHelper.RIGHT, ItemTouchHelper.RIGHT) {

            public static final float ALPHA_FULL = 1.0f;

            @Override
            public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
                // We only want the active item to change
                if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
                    if (viewHolder instanceof ContactViewHolder) {
                        // Let the view holder know that this item is being moved or dragged
                        ContactViewHolder itemViewHolder = (ContactViewHolder) viewHolder;
                        //itemViewHolder.onItemSelected();
                    }
                }

                super.onSelectedChanged(viewHolder, actionState);
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                    int contactType = 0;//Imps.Contacts.TYPE_NORMAL;

                    if (viewHolder instanceof ContactViewHolder) {
                        // Let the view holder know that this item is being moved or dragged
                        ContactViewHolder itemViewHolder = (ContactViewHolder) viewHolder;
                        contactType = itemViewHolder.mType;
                    }

                    // Get RecyclerView item from the ViewHolder
                    View itemView = viewHolder.itemView;

                    Paint p = new Paint();
                    Bitmap icon;

                    if (dX > 0) {

                        if ((contactType) != 0)
                        {
                            icon = BitmapFactory.decodeResource(
                                    getActivity().getResources(), R.drawable.ic_unarchive_white_24dp);

                            p.setColor(getResources().getColor(R.color.holo_green_dark));


                        }
                        else
                        {
                            icon = BitmapFactory.decodeResource(
                                    getActivity().getResources(), R.drawable.ic_archive_white_24dp);

                            p.setARGB(255, 150, 150, 150);

                        }

                        // Draw Rect with varying right side, equal to displacement dX
                        c.drawRect((float) itemView.getLeft(), (float) itemView.getTop(), dX,
                                (float) itemView.getBottom(), p);

                        // Set the image icon for Right swipe
                        c.drawBitmap(icon,
                                (float) itemView.getLeft() + convertDpToPx(16),
                                (float) itemView.getTop() + ((float) itemView.getBottom() - (float) itemView.getTop() - icon.getHeight())/2,
                                p);
                    }


                    // Fade out the view as it is swiped out of the parent's bounds
                    final float alpha = ALPHA_FULL - Math.abs(dX) / (float) viewHolder.itemView.getWidth();
                    viewHolder.itemView.setAlpha(alpha);
                    viewHolder.itemView.setTranslationX(dX);

                } else {
                    super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }
            }

            private int convertDpToPx(int dp){
                return Math.round(dp * (getResources().getDisplayMetrics().xdpi / DisplayMetrics.DENSITY_DEFAULT));
            }

            @Override
            public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                super.clearView(recyclerView, viewHolder);

                viewHolder.itemView.setAlpha(ALPHA_FULL);

                if (viewHolder instanceof ContactViewHolder) {
                    // Tell the view holder it's time to restore the idle state
                    ContactViewHolder itemViewHolder = (ContactViewHolder) viewHolder;
                    //itemViewHolder.onItemClear();
                }
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                // callback for drag-n-drop, false to skip this feature
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {


                // callback for swipe to dismiss, removing item from data and adapter
                int position = viewHolder.getAdapterPosition();

                if (viewHolder instanceof ContactViewHolder) {
                    // Tell the view holder it's time to restore the idle state
                    ContactViewHolder itemViewHolder = (ContactViewHolder) viewHolder;
                    //itemViewHolder.onItemClear();

                    final boolean doArchive = ((itemViewHolder.mType) == 0);
                    final int contactType = itemViewHolder.mType;
                    final String address = itemViewHolder.mAddress;
                    final long providerId = itemViewHolder.mProviderId;
                    final long accountId = itemViewHolder.mAccountId;

                    if (doArchive) {
                        Snackbar snack = Snackbar.make(mRecView, getString(R.string.are_you_sure), Snackbar.LENGTH_LONG);
                        snack.setAction(R.string.yes, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                archiveContact(getActivity(), address, contactType, providerId, accountId);
                            }
                        });

                        snack.show();
                    }
                    else
                    {
                        Snackbar snack = Snackbar.make(mRecView, getString(R.string.action_unarchived), Snackbar.LENGTH_SHORT);
                        unarchiveContact(getActivity(), address, contactType, providerId, accountId);
                        snack.show();

                    }
                }
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return true;
            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return true;
            }
        });
        swipeToDismissTouchHelper.attachToRecyclerView(recyclerView);




    }

    public void updateListVisibility ()
    {
        if (mAdapter != null) {
            if (mAdapter.getItemCount() == 0) {
                mRecView.setVisibility(View.GONE);
                mEmptyView.setVisibility(View.VISIBLE);

            } else if (mRecView.getVisibility() == View.GONE) {
                mRecView.setVisibility(View.VISIBLE);
                mEmptyView.setVisibility(View.GONE);

            }
        }
    }

    private static void archiveContact (Activity activity, String address, int contactType, long providerId, long accountId)
    {


            //then delete the contact from our list
            /**
            IContactListManager manager = mConn.getContactListManager();

            int res = manager.archiveContact(address,contactType,true);
            if (res != ImErrorInfo.NO_ERROR) {
                //mHandler.showAlert(R.string.error,
                //      ErrorResUtils.getErrorRes(getResources(), res, address));
            }**/




    }

    /**
     * TODO REIMPLEMENT
     * @param activity
     * @param address
     * @param contactType
     * @param providerId
     * @param accountId
     */
    private static void unarchiveContact (Activity activity, String address, int contactType, long providerId, long accountId)
    {



            //then delete the contact from our list
            /**
            IContactListManager manager = mConn.getContactListManager();

            int res = manager.archiveContact(address,contactType,false);
            if (res != ImErrorInfo.NO_ERROR) {
                //mHandler.showAlert(R.string.error,
                //      ErrorResUtils.getErrorRes(getResources(), res, address));
            }
            **/




    }

    private static void deleteContact (Activity activity, long itemId, String address, long providerId, long accountId)
    {





            //then delete the contact from our list**
            /**
            IContactListManager manager = mConn.getContactListManager();

            int res = manager.removeContact(address);
            if (res != ImErrorInfo.NO_ERROR) {
                //mHandler.showAlert(R.string.error,
                  //      ErrorResUtils.getErrorRes(getResources(), res, address));
            }
            **/




    }




}
