package info.guardianproject.keanuapp.ui.contacts;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import agency.tango.android.avatarview.views.AvatarView;
import info.guardianproject.keanuapp.R;

/**
 * Created by n8fr8 on 3/29/16.
 */
public class ContactViewHolder extends RecyclerView.ViewHolder
{
    public ContactViewHolder(View view) {
        super(view);
        mLine1 = view.findViewById(R.id.line1);
        mLine2 = view.findViewById(R.id.line2);
        mAvatar = view.findViewById(R.id.avatar);
        mAvatarCheck = view.findViewById(R.id.avatarCheck);

        mSubBox = view.findViewById(R.id.subscriptionBox);
        mButtonSubApprove = view.findViewById(R.id.btnApproveSubscription);
        mButtonSubDecline = view.findViewById(R.id.btnDeclineSubscription);

        mContainer = view.findViewById(R.id.message_container);
    }

    public String mAddress;
    public String mNickname;
    public int mType;

    public long mProviderId;
    public long mAccountId;
    public long mContactId;

    public View mContainer;
    public TextView mLine1;
    public TextView mLine2;
    //public TextView mStatusText;
    public ImageView mAvatar;
    public ImageView mAvatarCheck;
    //public ImageView mStatusIcon;
    public ImageView mMediaThumb;

    public View mSubBox;
    public Button mButtonSubApprove;
    public Button mButtonSubDecline;
}
