package info.guardianproject.keanuapp.ui.onboarding;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.ListPopupWindow;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.google.android.material.snackbar.Snackbar;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import info.guardianproject.keanu.core.auth.AuthenticationActivity;
import info.guardianproject.keanu.core.model.Server;
import info.guardianproject.keanu.core.ui.RoundedAvatarDrawable;
import info.guardianproject.keanu.core.util.Languages;
import info.guardianproject.keanu.core.util.SecureMediaStore;
import info.guardianproject.keanuapp.ImApp;
import info.guardianproject.keanuapp.MainActivity;
import info.guardianproject.keanuapp.R;
import info.guardianproject.keanuapp.databinding.AwesomeOnboardingBinding;
import info.guardianproject.keanuapp.ui.BaseActivity;

import static info.guardianproject.keanu.core.KeanuConstants.LOG_TAG;

@SuppressWarnings("deprecation")
@SuppressLint("LogNotTimber")
public class OnboardingActivity extends BaseActivity implements OnboardingListener {

    private static final String USERNAME_ONLY_ALPHANUM = "[^A-Za-z0-9]";
    private final static int MY_PERMISSIONS_REQUEST_CAMERA = 1;
    CropImageView mCropImageView;
    Uri mOutputFileUri = null;
    private OnboardingAccount mNewAccount;
    private boolean mShowSplash = true;
    private ListPopupWindow mDomainList;
    private FindServerTask mCurrentFindServerTask;
    private boolean mLoggingIn = false;
    private final Handler mOnboardingHandler = new Handler(Looper.getMainLooper());
    private OnboardingManager mManager;

    private AwesomeOnboardingBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mShowSplash = getIntent().getBooleanExtra("showSplash", true);

        mBinding = AwesomeOnboardingBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());

        ActionBar ab = getSupportActionBar();

        if (ab != null) {
            if (mShowSplash) {
                ab.hide();
            } else {
                ab.setDisplayHomeAsUpEnabled(true);
                ab.setHomeButtonEnabled(true);
            }

            ab.setTitle("");
        }

        mDomainList = new ListPopupWindow(this);
        mDomainList.setAdapter(new ArrayAdapter<>(
                this,
                android.R.layout.simple_dropdown_item_1line, Server.getServersText(this)));
        mDomainList.setAnchorView(mBinding.flipViewAdvanced.spinnerDomains);
        mDomainList.setWidth(600);
        mDomainList.setHeight(400);

        mDomainList.setModal(false);
        mDomainList.setOnItemClickListener((parent, view, position, id) -> {
            mBinding.flipViewAdvanced.spinnerDomains.setText(Server.getServersText(OnboardingActivity.this)[position]);
            mDomainList.dismiss();
        });

        mBinding.flipViewAdvanced.spinnerDomains.setText(Server.getServersText(OnboardingActivity.this)[0]);


        mBinding.flipViewAdvanced.spinnerDomains.setOnClickListener(v -> mDomainList.show());
        mBinding.flipViewAdvanced.spinnerDomains.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus)
                mDomainList.show();
        });

        mBinding.flipViewSuccess.imageAvatar.setOnClickListener(view -> startAvatarTaker());

        setAnimLeft();

        ImageView imageLogo = mBinding.flipViewMain.imageLogo;
        imageLogo.setOnClickListener(v -> {
            setAnimLeft();
            showOnboarding();
        });

        mBinding.flipViewMain.nextButton.setOnClickListener(v -> {
            setAnimLeft();
            showOnboarding();
        });

        mBinding.flipViewRegister.btnShowRegister.setOnClickListener(v -> {
            setAnimLeft();

            Intent intent = new Intent(this, AuthenticationActivity.class);
            intent.putExtra(AuthenticationActivity.EXTRA_IS_SIGN_UP, true);
            startActivityForResult(intent, OnboardingManager.REQUEST_SIGN_UP);
//            showSetupScreen();
        });

        mBinding.flipViewRegister.btnShowLogin.setOnClickListener(v -> {
            setAnimLeft();

            startActivityForResult(new Intent(this, AuthenticationActivity.class),
                    OnboardingManager.REQUEST_SIGN_IN);
//            showLoginScreen();
        });

        // Set up language chooser button.
        mBinding.flipViewMain.languageButton.setOnClickListener(v -> {
            final Activity activity = OnboardingActivity.this;
            final Languages languages = Languages.get(activity);
            final ArrayAdapter<String> languagesAdapter = new ArrayAdapter<>(activity,
                    android.R.layout.simple_list_item_single_choice, languages.getAllNames());
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setIcon(R.drawable.ic_settings_language);
            builder.setTitle(R.string.KEY_PREF_LANGUAGE_TITLE);
            builder.setAdapter(languagesAdapter, (dialog, position) -> {
                String[] languageCodes = languages.getSupportedLocales();
                ImApp.resetLanguage(activity, languageCodes[position]);
                dialog.dismiss();
            });
            builder.show();
        });

        mBinding.flipViewAdvanced.btnNewRegister.setOnClickListener(v -> {
            View viewEdit = findViewById(R.id.edtNameAdvanced);
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

            if (imm != null) {
                imm.hideSoftInputFromWindow(viewEdit.getWindowToken(), 0);
            }

            startAdvancedSetup();
        });

        mBinding.flipViewLogin.btnSignIn.setOnClickListener(v -> doExistingAccountRegister());

        if (!mShowSplash) {
            setAnimLeft();
            showOnboarding();
        }

    }

    private void setAnimLeft() {
        Animation animIn = AnimationUtils.loadAnimation(this, R.anim.push_left_in);
        Animation animOut = AnimationUtils.loadAnimation(this, R.anim.push_left_out);
        mBinding.viewFlipper1.setInAnimation(animIn);
        mBinding.viewFlipper1.setOutAnimation(animOut);
    }

    private void setAnimRight() {
        Animation animIn = AnimationUtils.loadAnimation(OnboardingActivity.this, R.anim.push_right_in);
        Animation animOut = AnimationUtils.loadAnimation(OnboardingActivity.this, R.anim.push_right_out);
        mBinding.viewFlipper1.setInAnimation(animIn);
        mBinding.viewFlipper1.setOutAnimation(animOut);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_onboarding, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            showPrevious();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (mDomainList != null && mDomainList.isShowing())
            mDomainList.dismiss();
        else
            showPrevious();
    }

    // Back button should bring us to the previous screen, unless we're on the first screen.
    private void showPrevious() {
        setAnimRight();

        ActionBar ab = getSupportActionBar();
        if (ab != null) ab.setTitle("");

        if (mCurrentFindServerTask != null) {
            mCurrentFindServerTask.cancel(true);
        }

        if (mBinding.viewFlipper1.getCurrentView().getId() == R.id.flipViewMain) {
            finish();
        } else if (mBinding.viewFlipper1.getCurrentView().getId() == R.id.flipViewRegister) {
            if (mShowSplash) {
                showSplashScreen();
            } else {
                finish();
            }
        }
        else if (mBinding.viewFlipper1.getCurrentView().getId() == R.id.flipViewLogin) {
            showOnboarding();
        } else if (mBinding.viewFlipper1.getCurrentView().getId() == R.id.flipViewAdvanced) {
            showOnboarding();
        }
    }

    private void showSplashScreen() {
        setAnimRight();

        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.hide();
            ab.setTitle("");
        }

        mBinding.viewFlipper1.setDisplayedChild(0);
    }

    private void showOnboarding() {
        mBinding.viewFlipper1.setDisplayedChild(1);
    }

    private void showSetupScreen() {
        mBinding.viewFlipper1.setDisplayedChild(3);

        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.show();
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setHomeButtonEnabled(true);
        }
    }

    private void showLoginScreen() {
        mBinding.viewFlipper1.setDisplayedChild(2);
        findViewById(R.id.progressExistingUser).setVisibility(View.GONE);
        findViewById(R.id.progressExistingImage).setVisibility(View.GONE);

        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.show();
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setHomeButtonEnabled(true);
        }
    }

    private void startAdvancedSetup() {
        String nickname = ((EditText) findViewById(R.id.edtNameAdvanced)).getText().toString();
        String username = nickname.replaceAll(USERNAME_ONLY_ALPHANUM, "").toLowerCase();

        if (TextUtils.isEmpty(username)) {
            // If there are no alphanum then just use a series of numbers with the app name.
            username = getString(R.string.app_name) + "=" + (int) (Math.random() * 1000000f);
        }

        String domain = ((EditText) findViewById(R.id.spinnerDomains)).getText().toString();

        String password = ((EditText) findViewById(R.id.edtNewPass)).getText().toString();
        String passwordConfirm = ((EditText) findViewById(R.id.edtNewPassConfirm)).getText().toString();

        if (!TextUtils.isEmpty(password)) {
            if (!TextUtils.isEmpty(passwordConfirm)) {
                if (password.equals(passwordConfirm)) {
                    mBinding.viewFlipper1.setDisplayedChild(4);

                    if (mCurrentFindServerTask != null)
                        mCurrentFindServerTask.cancel(true);

                    mCurrentFindServerTask = new FindServerTask();
                    mCurrentFindServerTask.execute(nickname, username, domain, password);
                } else {
                    Toast.makeText(this, R.string.lock_screen_passphrases_not_matching, Toast.LENGTH_LONG).show();

                    findViewById(R.id.edtNewPassConfirm).setBackgroundColor(getResources().getColor(R.color.holo_red_dark));
                }
            } else {
                findViewById(R.id.edtNewPassConfirm).setBackgroundColor(getResources().getColor(R.color.holo_red_dark));
                Toast.makeText(this, "The confirm password field cannot be blank.", Toast.LENGTH_LONG).show();
            }
        } else {
            findViewById(R.id.edtNewPass).setBackgroundColor(getResources().getColor(R.color.holo_red_dark));
            Toast.makeText(this, "The password field cannot be blank.", Toast.LENGTH_LONG).show();
        }
    }

    private void showMainScreen(boolean isNewAccount) {
        finish();

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("firstTime", isNewAccount);
        startActivity(intent);
    }

    private synchronized void doExistingAccountRegister() {
        String username = ((TextView) findViewById(R.id.edtName)).getText().toString();
        String password = ((TextView) findViewById(R.id.edtPass)).getText().toString();
        String server = ((TextView) findViewById(R.id.edtServer)).getText().toString();

        if (!mLoggingIn) {
            mLoggingIn = true;

            findViewById(R.id.progressExistingUser).setVisibility(View.VISIBLE);
            findViewById(R.id.progressExistingImage).setVisibility(View.VISIBLE);

            hideKeyboard();

            if (mManager == null) mManager = new OnboardingManager(this, this);

//            ((ImApp) getApplication()).login(username, password, server, null, this);
        }
    }

    private void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        if (view != null && imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void showErrorMessage(String message) {
        Snackbar.make(mBinding.viewFlipper1, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == OnboardingManager.REQUEST_SIGN_IN || requestCode == OnboardingManager.REQUEST_SIGN_UP) {
                registrationSuccessful(data.getParcelableExtra(AuthenticationActivity.EXTRA_ONBOARDING_ACCOUNT));
            }
            else if (requestCode == OnboardingManager.REQUEST_SCAN) {

                ArrayList<String> resultScans = data.getStringArrayListExtra("result");

                if (resultScans != null && resultScans.size() > 0) {
                    for (String resultScan : resultScans) {
                        try {
                            // Parse each string and if they are for a new user then add the user.
                            OnboardingManager.DecodedInviteLink diLink = OnboardingManager.decodeInviteLink(resultScan);

//                            new AddContactAsyncTask(mNewAccount.providerId, mNewAccount.accountId).execute(diLink.username, diLink.fingerprint, diLink.nickname);

                            // If they are for a group chat, then add the group.
                        } catch (Exception e) {
                            Log.w(LOG_TAG, "error parsing QR invite link", e);
                        }
                    }

                    showMainScreen(false);
                }
            } else if (requestCode == OnboardingManager.REQUEST_CHOOSE_AVATAR) {
                Uri imageUri = getPickImageResultUri(data);

                if (imageUri == null)
                    return;

                mCropImageView = new CropImageView(OnboardingActivity.this);

                try {
                    Bitmap bmpThumbnail = SecureMediaStore.getThumbnailFile(OnboardingActivity.this, imageUri, 512);
                    mCropImageView.setImageBitmap(bmpThumbnail);

                    // Use the Builder class for convenient dialog construction.
                    androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(OnboardingActivity.this);
                    builder.setView(mCropImageView)
                            .setPositiveButton(R.string.ok, (dialog, id) -> {
                                setAvatar(mCropImageView.getCroppedImage(), mNewAccount);
                                showMainScreen(false);

                                delete(mOutputFileUri);
                            })
                            .setNegativeButton(R.string.cancel, (dialog, id) -> {
                                // User cancelled the dialog.
                                delete(mOutputFileUri);
                            });

                    // Create the AlertDialog object and return it.
                    androidx.appcompat.app.AlertDialog dialog = builder.create();
                    dialog.show();
                } catch (IOException ioe) {
                    Log.e(LOG_TAG, "couldn't load avatar", ioe);
                }
            }
        }
    }

    private void setAvatar(Bitmap bmp, OnboardingAccount account) {

        RoundedAvatarDrawable avatar = new RoundedAvatarDrawable(bmp);
        mBinding.flipViewSuccess.imageAvatar.setImageDrawable(avatar);

        try {

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 90, stream);

            byte[] avatarBytesCompressed = stream.toByteArray();


        } catch (Exception e) {
            Log.w(LOG_TAG, "error loading image bytes", e);
        }
    }

    /**
     * Create a chooser intent to select the source to get image from.<br/>
     * The source can be camera's (ACTION_IMAGE_CAPTURE) or gallery's (ACTION_GET_CONTENT).<br/>
     * All possible sources are added to the intent chooser.
     */
    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();

        // Collect all camera intents.
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // Collect all gallery intents.
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // The main intent is the last in the list (fucking android) so pickup the useless one.
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            ComponentName componentName = intent.getComponent();

            if (componentName != null && componentName.getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent.
        Intent chooserIntent = Intent.createChooser(mainIntent, getString(R.string.choose_photos));

        // Add all other intents.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[0]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private synchronized Uri getCaptureImageOutputUri() {

        if (mOutputFileUri == null) {
            File photo = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "kavatar.jpg");
            mOutputFileUri = FileProvider.getUriForFile(this,
                    getPackageName() + ".provider",
                    photo);

        }

        return mOutputFileUri;
    }


    private void delete(Uri uri) {
        String scheme = uri.getScheme();

        if (scheme != null) {
            if (scheme.equals("content")) {
                getContentResolver().delete(uri, null, null);
                return;
            }

            if (scheme.equals("file")) {
                File file = new File(uri.toString().substring(5));

                if (file.exists()) {
                    //noinspection ResultOfMethodCallIgnored
                    file.delete();
                }
            }
        }
    }


    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            boolean isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
            return isCamera ? getCaptureImageOutputUri() : data.getData();
        }

        return getCaptureImageOutputUri();
    }

    void startAvatarTaker() {
        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);

        if (permissionCheck == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                Snackbar.make(mBinding.viewFlipper1, R.string.grant_perms, Snackbar.LENGTH_LONG).show();
            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            startActivityForResult(getPickImageChooserIntent(), OnboardingManager.REQUEST_CHOOSE_AVATAR);
        }
    }

    @Override
    public void registrationSuccessful(final OnboardingAccount account) {
        mNewAccount = account;
        showMainScreen(!mLoggingIn);

        mLoggingIn = false;
    }

    @Override
    public void registrationFailed(final String err) {
        if (mLoggingIn) {
            showErrorMessage(getString(R.string.invalid_password));

            findViewById(R.id.progressExistingUser).setVisibility(View.GONE);
            findViewById(R.id.progressExistingImage).setVisibility(View.GONE);

            mLoggingIn = false;
        } else {
            mOnboardingHandler.post(() -> {
                showSetupScreen();

                String text = getString(R.string.account_setup_error_server) + ": " + err;
                ((TextView) findViewById(R.id.statusError)).setText(text);
            });
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class FindServerTask extends AsyncTask<String, Void, OnboardingAccount> {

        @Override
        protected OnboardingAccount doInBackground(String... setupValues) {
            try {
                Server[] servers = Server.getServers(OnboardingActivity.this);

                Server myServer = new Server();
                String password = null;

                if (setupValues.length > 2)
                    myServer.domain = setupValues[2]; // User can specify the domain they want to be on for a new account.

                if (setupValues.length > 3)
                    password = setupValues[3];

                if (setupValues.length > 4)
                    myServer.server = setupValues[4];

                if (myServer.domain == null && servers != null) {
                    myServer = servers[0];
                }

                String nickname = setupValues[0];
                String username = setupValues[1];

                if (mManager == null)
                    mManager = new OnboardingManager(OnboardingActivity.this, OnboardingActivity.this);

                mManager.registerAccount(nickname, username, password, myServer.domain, myServer.domain, myServer.port);

                return null;
            } catch (Exception e) {
                Log.e(LOG_TAG, "auto onboarding fail", e);
                return null;
            }
        }

        @Override
        protected void onCancelled(OnboardingAccount onboardingAccount) {
            super.onCancelled(onboardingAccount);

            startAdvancedSetup();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();

            startAdvancedSetup();
        }

        @Override
        protected void onPostExecute(OnboardingAccount account) {
        }
    }
}
