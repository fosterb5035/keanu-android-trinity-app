/*
 * Copyright (C) 2007-2008 Esmertec AG. Copyright (C) 2007-2008 The Android Open
 * Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package info.guardianproject.keanuapp.ui.contacts;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import agency.tango.android.avatarview.AvatarPlaceholder;
import info.guardianproject.keanu.core.KeanuConstants;
import info.guardianproject.keanuapp.R;
import info.guardianproject.keanu.core.util.GlideUtils;

public class ContactListItem extends FrameLayout {

    private ContactViewHolder mHolder;

    private boolean mShowPresence = false;

    public ContactListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ContactViewHolder getViewHolder() {
        return mHolder;
    }

    public void setViewHolder(ContactViewHolder viewHolder) {
        mHolder = viewHolder;
    }

    public void bind (ContactViewHolder holder, String address, String nickname, String avatarUrl, OnClickListener confirmListener, OnClickListener denyListener) {

        int typeUnmasked = KeanuConstants.Contacts.TYPE_NORMAL;
        final int type = typeUnmasked & KeanuConstants.Contacts.TYPE_MASK;

        //    final String lastMsg = cursor.getString(COLUMN_LAST_MESSAGE);

        //   long lastMsgDate = cursor.getLong(COLUMN_LAST_MESSAGE_DATE);

        String statusText = "";


        if (TextUtils.isEmpty(nickname))
        {
            nickname = address.split(":")[0];
        }

         holder.mLine1.setText(nickname);

        holder.mAvatar.setVisibility(View.VISIBLE);

        GlideUtils.loadAvatar(getContext(), avatarUrl, new AvatarPlaceholder(nickname), holder.mAvatar);

        statusText = address;

        if ((typeUnmasked & KeanuConstants.Contacts.TYPE_FLAG_HIDDEN) != 0)
        {
            statusText += " | " + getContext().getString(R.string.action_archive);
        }

        if (holder.mLine2 != null)
            holder.mLine2.setText(statusText);



        /**
        if (mShowPresence) {
            switch (presence) {
                case Presence.AVAILABLE:
                    if (holder.mLine2 != null)
                        holder.mLine2.setText(getContext().getString(R.string.presence_available));

                case Presence.IDLE:
                    if (holder.mLine2 != null)
                        holder.mLine2.setText(getContext().getString(R.string.presence_available));

                default:
            }
        }
            **/

        holder.mLine1.setVisibility(View.VISIBLE);
    }








    public void applyStyleColors (ContactViewHolder holder)
    {
        //not set color
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getContext());
        int themeColorHeader = settings.getInt("themeColor",-1);
        int themeColorText = settings.getInt("themeColorText",-1);
        int themeColorBg = settings.getInt("themeColorBg",-1);


        if (themeColorText != -1)
        {
            if (holder.mLine1 != null)
                holder.mLine1.setTextColor(themeColorText);

            if (holder.mLine2 != null)
                holder.mLine2.setTextColor(themeColorText);

            //holder.mLine2.setTextColor(darker(themeColorText,2.0f));

        }

    }

    public void setShowPresence (boolean showPresence)
    {
        mShowPresence = showPresence;
    }



}
