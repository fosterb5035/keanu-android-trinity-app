package info.guardianproject.keanuapp.ui.contacts

import agency.tango.android.avatarview.AvatarPlaceholder
import android.Manifest
import android.content.ComponentName
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Parcelable
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.theartofdev.edmodo.cropper.CropImageView
import info.guardianproject.keanu.core.Preferences
import info.guardianproject.keanu.core.model.impl.BaseAddress
import info.guardianproject.keanu.core.util.SecureMediaStore
import info.guardianproject.keanu.core.util.SnackbarExceptionHandler
import info.guardianproject.keanuapp.ImApp
import info.guardianproject.keanuapp.MainActivity
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.*
import info.guardianproject.keanuapp.ui.BaseActivity
import info.guardianproject.keanuapp.ui.onboarding.OnboardingManager
import info.guardianproject.keanuapp.ui.qr.QrShareAsyncTask
import info.guardianproject.keanu.core.util.GlideUtils
import info.guardianproject.keanuapp.ui.widgets.GroupAvatar
import kotlinx.coroutines.*
import org.matrix.android.sdk.api.MatrixCallback
import org.matrix.android.sdk.api.query.QueryStringValue
import org.matrix.android.sdk.api.query.QueryStringValue.NoCondition
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.content.ContentUrlResolver.ThumbnailMethod
import org.matrix.android.sdk.api.session.events.model.Event
import org.matrix.android.sdk.api.session.events.model.EventType.STATE_ROOM_POWER_LEVELS
import org.matrix.android.sdk.api.session.room.Room
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.members.RoomMemberQueryParams
import org.matrix.android.sdk.api.session.room.model.*
import org.matrix.android.sdk.api.session.room.notification.RoomNotificationState
import org.matrix.android.sdk.api.session.room.powerlevels.PowerLevelsHelper
import org.matrix.android.sdk.api.session.room.powerlevels.Role
import org.matrix.android.sdk.api.session.room.state.isPublic
import org.matrix.android.sdk.api.util.Optional
import org.matrix.android.sdk.internal.di.MoshiProvider
import timber.log.Timber
import java.io.File
import java.io.IOException
import java.net.URLEncoder
import java.util.*

class GroupDisplayActivity : BaseActivity() {

    companion object {
        private const val MY_PERMISSIONS_REQUEST_CAMERA = 1
    }

    inner class GroupMember(
            var username: String? = null,
            nickname: String? = null,
            var role: Role = Role.Default,
            var affiliation: String? = null,
            var avatarUrl: String? = null,
            var online: Boolean = false
    ) {
        private var _nickname: String? = nickname

        var nickname: String?
            get() = if (_nickname.isNullOrBlank()) username else _nickname
            set(value) {
                _nickname = value
            }
    }

    private val mApp: ImApp?
        get() = application as? ImApp

    private val mSession: Session?
        get() = mApp?.matrixSession

    private lateinit var mAddress: String

    private var mYou = GroupMember()

    private var mMembers = ArrayList<GroupMember>()

    private var mHandler: Handler = Handler(Looper.getMainLooper())
    private var mRoomSummary: RoomSummary? = null
    private var mRoom: Room? = null

    private var mPlHelper: PowerLevelsHelper? = null

    private var mActionAddFriends: View? = null
    private var mActionShare: View? = null
    private var headerViewHolder: HeaderViewHolder? = null
    private var mCropImageView: CropImageView? = null

    private lateinit var mBinding: AwesomeActivityGroupBinding

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO + SnackbarExceptionHandler(mBinding.root))
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Timber.d("in #onCreate!")

        mBinding = AwesomeActivityGroupBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mAddress = intent.getStringExtra("address") ?: ""

        mRoom = mSession?.getRoom(mAddress)
        mRoomSummary = mSession?.getRoomSummary(mAddress)

        val query = RoomSummaryQueryParams.Builder()
        query.roomId = QueryStringValue.Equals(mAddress, QueryStringValue.Case.SENSITIVE)

        mSession?.getRoomSummariesLive(query.build())?.observe(this) {
            it.firstOrNull()?.let {
                mRoomSummary = it
                updateSession()
            }
        }

        supportActionBar?.title = ""
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun initData() {
        mYou.username = mSession?.myUserId
        mYou.affiliation = "none"
        mYou.role = Role.Default
        mYou.avatarUrl = null

        updateMembers()
    }

    private fun initRecyclerView() {
        mBinding.root.adapter = object : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {

            private val VIEW_TYPE_MEMBER = 0
            private val VIEW_TYPE_HEADER = 1
            private val VIEW_TYPE_FOOTER = 2

            private var mOriginalTextColor: Int? = null

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
                val inflater = LayoutInflater.from(parent.context)

                return when (viewType) {
                    VIEW_TYPE_HEADER -> HeaderViewHolder(AwesomeActivityGroupHeaderBinding.inflate(inflater, parent, false))
                    VIEW_TYPE_FOOTER -> FooterViewHolder(AwesomeActivityGroupFooterBinding.inflate(inflater, parent, false))
                    else -> MemberViewHolder(GroupMemberViewBinding.inflate(inflater, parent, false))
                }
            }

            override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
                if (holder is HeaderViewHolder) {
                    headerViewHolder = holder

                    checkNotificationState()

                    var avatarUrl: String? = mRoomSummary?.avatarUrl

                    if (avatarUrl?.isNotEmpty() == true) {
                        avatarUrl = mSession?.contentUrlResolver()?.resolveThumbnail(avatarUrl, 512, 512, ThumbnailMethod.SCALE)
                        GlideUtils.loadImageFromUri(mApp?.applicationContext, Uri.parse(avatarUrl), headerViewHolder?.avatar)
                    }
                    else {
                        val avatar = GroupAvatar(mRoomSummary?.displayName, false)
                        headerViewHolder?.avatar?.setImageDrawable(avatar)
                    }

                    headerViewHolder?.avatar?.setOnClickListener { startAvatarTaker() }

                    headerViewHolder?.qr?.setOnClickListener {
                        try {
                            mCoroutineScope.launch {
                                mRoom?.getRoomAliases()?.firstOrNull().let {
                                    if (it.isNullOrBlank()) return@let

                                    lifecycleScope.launch {
                                        @Suppress("BlockingMethodInNonBlockingContext")
                                        val inviteString = OnboardingManager.generateInviteLink(URLEncoder.encode(it, "UTF-8"))
                                        OnboardingManager.inviteScan(this@GroupDisplayActivity, inviteString)
                                    }
                                }
                            }
                        }
                        catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    headerViewHolder?.tvRoomName?.text = mRoomSummary?.displayName
                    headerViewHolder?.tvRoomTopic?.text = mRoomSummary?.topic
                    mActionShare = headerViewHolder?.actionShare

                    headerViewHolder?.actionShare?.setOnClickListener {
                        try {
                            mCoroutineScope.launch {
                                mRoom?.getRoomAliases()?.firstOrNull().let {
                                    if (it.isNullOrBlank()) {
                                        createAlias()
                                    }
                                    else {
                                        showShareSheet(it)
                                    }
                                }
                            }
                        }
                        catch (e: Exception) {
                            Timber.e(e, "couldn't generate QR code")
                        }
                    }

                    mActionAddFriends = headerViewHolder?.actionAddFriends
                    showAddFriends()

                    headerViewHolder?.actionNotifications?.setOnClickListener {
                        headerViewHolder?.checkNotifications?.isChecked = !(headerViewHolder?.checkNotifications?.isChecked ?: false)
                    }

                    headerViewHolder?.checkNotifications?.setOnCheckedChangeListener { _: CompoundButton?, isChecked: Boolean ->
                        setNotificationsEnabled(isChecked)
                    }

                    headerViewHolder?.checkNotifications?.isEnabled = true

                    if (Preferences.doGroupEncryption() && canInviteOthers(mYou)) {
                        headerViewHolder?.checkGroupEncryption?.isChecked = isGroupEncryptionEnabled

                        if (isGroupEncryptionEnabled) {
                            headerViewHolder?.checkGroupEncryption?.visibility = View.GONE
                            headerViewHolder?.actionGroupEncryption?.visibility = View.GONE
                        }
                        else {
                            headerViewHolder?.checkGroupEncryption?.setOnCheckedChangeListener { _: CompoundButton?, isChecked: Boolean ->
                                isGroupEncryptionEnabled = isChecked
                            }
                            headerViewHolder?.actionGroupEncryption?.visibility = View.VISIBLE
                            headerViewHolder?.checkGroupEncryption?.visibility = View.VISIBLE
                            headerViewHolder?.checkGroupEncryption?.isEnabled = true
                        }
                    }
                    else {
                        headerViewHolder?.actionGroupEncryption?.visibility = View.GONE
                    }

                    if (!canChangeName(mYou)) {
                        headerViewHolder?.ivEditRoomName?.visibility = View.GONE
                        headerViewHolder?.ivEditRoomTopic?.visibility = View.GONE
                    }
                    else {
                        headerViewHolder?.ivEditRoomName?.setOnClickListener { showEditAlert(R.string.Change_room_name, headerViewHolder?.tvRoomName?.text, ::changeRoomName) }
                        headerViewHolder?.ivEditRoomName?.visibility = View.VISIBLE
                        headerViewHolder?.ivEditRoomName?.isEnabled = true

                        headerViewHolder?.ivEditRoomTopic?.setOnClickListener { showEditAlert(R.string.Change_room_topic, headerViewHolder?.tvRoomTopic?.text, ::changeRoomTopic) }
                        headerViewHolder?.ivEditRoomTopic?.visibility = View.VISIBLE
                        headerViewHolder?.ivEditRoomTopic?.isEnabled = true
                    }
                }
                else if (holder is FooterViewHolder) {
                    // Tint the "leave" text and drawable(s)
                    val colorAccent = ResourcesCompat.getColor(resources, R.color.holo_orange_light, theme)

                    for (d in holder.actionLeave.compoundDrawables) {
                        d?.let { DrawableCompat.setTint(d, colorAccent) }
                    }

                    holder.actionLeave.setTextColor(colorAccent)
                    holder.actionLeave.setOnClickListener { confirmLeaveGroup() }
                }
                else if (holder is MemberViewHolder) {
                    if (mMembers.isEmpty()) {
                        holder.line1.setText(R.string.loading)
                        holder.line2.text = ""
                    }
                    else {
                        // Reset the padding to match other views in this hierarchy.
                        val padding = resources.getDimensionPixelOffset(R.dimen.detail_view_padding)
                        holder.itemView.setPadding(padding, holder.itemView.paddingTop, padding, holder.itemView.paddingBottom)

                        val idxMember = position - 1
                        val member = mMembers[idxMember]
                        var nickname = member.nickname

                        if (member.username == mYou.username) {
                            nickname += " " + getString(R.string.group_you)
                        }

                        holder.line1.text = nickname
                        holder.line2.text = member.username

                        when {
                            canGrantAdmin(member) -> {
                                holder.avatarCrown.setImageResource(R.drawable.ic_crown)
                                holder.avatarCrown.visibility = View.VISIBLE
                            }
                            member.affiliation == Membership.INVITE.value -> {
                                holder.avatarCrown.setImageResource(R.drawable.ic_message_wait_grey)
                                holder.avatarCrown.visibility = View.VISIBLE
                            }
                            else -> {
                                holder.avatarCrown.visibility = View.GONE
                            }
                        }

                        if (mOriginalTextColor == null) mOriginalTextColor = holder.line1.currentTextColor
                        holder.line1.setTextColor(if (member.role is Role.Default) Color.GRAY else mOriginalTextColor!!)

                        holder.avatar.visibility = View.VISIBLE

                        GlideUtils.loadAvatar(this@GroupDisplayActivity, member.avatarUrl, AvatarPlaceholder(nickname), holder.avatar)

                        holder.itemView.setOnClickListener { showMemberInfo(member) }
                    }
                }
            }

            override fun getItemCount(): Int {
                return if (mMembers.isEmpty()) 3 else (2 + mMembers.size)
            }

            override fun getItemViewType(position: Int): Int {
                return when (position) {
                    0 -> VIEW_TYPE_HEADER
                    itemCount - 1 -> VIEW_TYPE_FOOTER
                    else -> VIEW_TYPE_MEMBER
                }
            }
        }

        mBinding.root.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
    }

    private fun updateSession() {
        initData()
        initRecyclerView()
    }

    override fun onResume() {
        super.onResume()

        updateSession()
    }

    fun startAvatarTaker() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA))
            {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                Snackbar.make(mBinding.root, R.string.grant_perms, Snackbar.LENGTH_LONG).show()
            }
            else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA),
                        MY_PERMISSIONS_REQUEST_CAMERA)
            }
        }
        else {
            mPickAvatar.launch(pickImageChooserIntent)
        }
    }

    private fun updateMembers() {
        updatePowerLevels(mRoom?.getStateEvent(STATE_ROOM_POWER_LEVELS, NoCondition))

        val builder = RoomMemberQueryParams.Builder()
        builder.memberships = arrayListOf(Membership.JOIN, Membership.INVITE)
        val query = builder.build()

        mRoom?.let { updateRoomMembers(it.getRoomMembers(query)) }

        mRoom?.getRoomMembersLive(query)?.observe(this, this@GroupDisplayActivity::updateRoomMembers)

        mRoom?.getStateEventLive(STATE_ROOM_POWER_LEVELS, NoCondition)?.observe(this, { eventOptional: Optional<Event> ->
            updatePowerLevels(eventOptional.get())
        })
    }

    private fun updatePowerLevels(eventPowerLevels: Event?) {
        if (eventPowerLevels == null) return

        val mapLevels = eventPowerLevels.getClearContent()

        MoshiProvider.providesMoshi().adapter(PowerLevelsContent::class.java).fromJsonValue(mapLevels)?.let {
            mPlHelper = PowerLevelsHelper(it)
        }
    }

    private fun updateRoomMembers(roomMemberSummaries: List<RoomMemberSummary>) {
        mMembers.clear()

        for ((membership, userId, displayName, avatarUrl) in roomMemberSummaries) {
            val member = GroupMember(
                    username = userId,
                    nickname = displayName,
                    role = mPlHelper?.getUserRole(userId) ?: Role.Default,
                    affiliation = membership.name,
                    avatarUrl = mSession?.contentUrlResolver()?.resolveThumbnail(avatarUrl, 120, 120, ThumbnailMethod.SCALE))

            if (mSession?.myUserId?.contentEquals(userId) == true) {
                mYou = member
            }

            if (canGrantAdmin(member)) {
                mMembers.add(0, member)
            }
            else {
                mMembers.add(member)
            }
        }

        mBinding.root.adapter?.notifyDataSetChanged()
    }

    private fun inviteContacts(invitees: ArrayList<String>) {
        try {
            for (invitee in invitees) {
                mSession?.getRoom(mAddress)?.invite(invitee, mRoomSummary?.topic, object : MatrixCallback<Unit> {})

                val member = GroupMember()
                val address = BaseAddress(invitee)
                member.username = address.bareAddress
                member.nickname = address.user
                // member.avatar = DatabaseUtils.getAvatarFromAddress(member.username, SMALL_AVATAR_WIDTH, SMALL_AVATAR_HEIGHT);
                mMembers.add(member)
            }
            mBinding.root.adapter?.notifyDataSetChanged()
        }
        catch (e: Exception) {
            Timber.e(e, "error inviting contacts to group")
        }
    }

    fun showMemberInfo(member: GroupMember) {
        if (member === mYou) return

        val canGrantAdmin = canGrantAdmin(mYou)
        val canKickout = canRevokeMembership(mYou)
        //   final boolean isModerator = TextUtils.equals(mYou.role, "moderator")||TextUtils.equals(mYou.role, "admin");

        if (canGrantAdmin || canKickout) {
            val alert = AlertDialog.Builder(this)

            val content = GroupMemberOperationsBinding.inflate(LayoutInflater.from(this))

            val nickname = member.nickname ?: member.username

            content.member.line1.text = nickname
            content.member.line2.text = member.username

            content.member.avatarCrown.visibility = if (canGrantAdmin(member)) View.VISIBLE else View.GONE

            if (member.avatarUrl == null) {
                // h.avatar.setImageDrawable(AvatarGenerator.Companion.avatarImage(GroupDisplayActivity.this, 120, AvatarConstants.Companion.getRECTANGLE(), nickname));
                content.member.avatar.visibility = View.VISIBLE
            }
            else {
                GlideUtils.loadImageFromUri(this, Uri.parse(member.avatarUrl), content.member.avatar)
            }

            alert.setView(content.root)
            val dialog = alert.show()

            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            if (canGrantAdmin) {
                content.actionMakeAdmin.setOnClickListener {
                    dialog.dismiss()
                    grantAdmin(member)
                }
            }
            else {
                content.actionMakeAdmin.visibility = View.GONE
            }

            content.actionViewProfile.setOnClickListener {
                dialog.dismiss()
                showMemberProfile(member)
            }

            if (canKickout) {
                content.actionKickout.setOnClickListener {
                    dialog.dismiss()
                    kickout(member)
                }
            }
            else {
                content.actionKickout.visibility = View.GONE
            }
        }
        else {
            showMemberProfile(member)
        }
    }

    private fun showMemberProfile(member: GroupMember) {
        val intent = Intent(this, ContactDisplayActivity::class.java)
        intent.putExtra("address", member.username)

        startActivity(intent)
    }

    private fun grantAdmin(member: GroupMember) {
        try {
            val username = member.username ?: return
            val myUsername = mYou.username ?: return
            val mapLevels = mRoom?.getStateEvent(STATE_ROOM_POWER_LEVELS, NoCondition)?.getClearContent() ?: return
            val plContent = MoshiProvider.providesMoshi().adapter(PowerLevelsContent::class.java).fromJsonValue(mapLevels) ?: return
            val phHelper = PowerLevelsHelper(plContent)

            plContent.setUserPowerLevel(username, phHelper.getUserPowerLevelValue(myUsername))

            mCoroutineScope.launch {
                mRoom?.sendStateEvent(STATE_ROOM_POWER_LEVELS, null, mapLevels)

                Timber.d("#sendStateEvent success!")
            }
        }
        catch (ignored: Exception) {
        }
    }

    private fun kickout(member: GroupMember) {
        try {
            member.username?.let{ mSession?.getRoom(mAddress)?.kick(it, null, object : MatrixCallback<Unit> {}) }
        }
        catch (ignored: Exception) {
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private val mPickContacts = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode != RESULT_OK) return@registerForActivityResult

        var invitees = ArrayList<String>()

        val username = result.data?.getStringExtra(ContactsPickerActivity.EXTRA_RESULT_USERNAME)

        if (username != null) {
            invitees.add(username)
        }
        else {
            result.data?.getStringArrayListExtra(ContactsPickerActivity.EXTRA_RESULT_USERNAMES)?.let {
                invitees = it
            }
        }

        inviteContacts(invitees)

        mHandler.postDelayed({ updateSession() }, 3000)
    }

    private val mPickAvatar = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode != RESULT_OK) return@registerForActivityResult

        val imageUri = getPickImageResultUri(result.data) ?: return@registerForActivityResult

        mCropImageView = CropImageView(this)

        try {
            mCropImageView?.setImageBitmap(SecureMediaStore.getThumbnailFile(this, imageUri, 512))

            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder(this)
                    .setView(mCropImageView)
                    .setPositiveButton(R.string.ok) { _: DialogInterface?, _: Int -> mCropImageView?.croppedImage?.let{ setAvatar(it) } }
                    .setNegativeButton(R.string.cancel, null)
                    .show()
        } catch (ioe: IOException) {
            Timber.e(ioe, "couldn't load avatar")
        }
    }

    private class HeaderViewHolder(private val mBinding: AwesomeActivityGroupHeaderBinding) : RecyclerView.ViewHolder(mBinding.root) {

        val avatar get() = mBinding.ivAvatar
        val qr get() = mBinding.qrcode
        val tvRoomName get() = mBinding.tvRoomName
        val ivEditRoomName get() = mBinding.ivEditRoomName
        val tvRoomTopic get() = mBinding.tvRoomTopic
        val ivEditRoomTopic get() = mBinding.iVEditRoomTopic
        val actionShare get() = mBinding.tvActionShare
        val actionAddFriends get() = mBinding.tvActionAddFriends
        val actionNotifications get() = mBinding.tvActionNotifications
        val actionGroupEncryption get() = mBinding.tvActionEncryption
        val checkNotifications get() = mBinding.chkNotifications
        val checkGroupEncryption get() = mBinding.chkGroupEncryption
    }

    private class FooterViewHolder(private val mBinding: AwesomeActivityGroupFooterBinding)
        : RecyclerView.ViewHolder(mBinding.root) {

        val actionLeave
            get() = mBinding.tvActionLeave
    }

    private class MemberViewHolder(private val mBinding: GroupMemberViewBinding) : RecyclerView.ViewHolder(mBinding.root) {
        val line1 get() = mBinding.line1
        val line2 get() = mBinding.line2
        val avatar get() = mBinding.avatar
        val avatarCrown get() = mBinding.avatarCrown
    }

    private fun showEditAlert(title: Int, old: CharSequence?, callback: (newValue: String) -> Unit) {
        val content = AlertDialogEdittextBinding.inflate(layoutInflater, mBinding.root, false)
        content.editText.setText(old)

        AlertDialog.Builder(this)
                .setTitle(title)
                .setView(content.root)
                .setPositiveButton(R.string.ok) { _: DialogInterface, _: Int ->
                    callback(content.editText.text.toString())
                }
                .setNegativeButton(R.string.cancel, null)
                .show()
    }

    private fun changeRoomName(name: String) {
        headerViewHolder?.tvRoomName?.text = name

        mCoroutineScope.launch {
            try {
                mRoom?.updateName(name)
            }
            catch (exception: Throwable) {
                lifecycleScope.launch {
                    headerViewHolder?.tvRoomName?.text = mRoomSummary?.displayName
                }

                throw exception
            }
        }

    }

    private fun changeRoomTopic(topic: String) {
        headerViewHolder?.tvRoomTopic?.text = topic

        mCoroutineScope.launch {
            try {
                mRoom?.updateTopic(topic)
            }
            catch (exception: Throwable) {
                lifecycleScope.launch {
                    headerViewHolder?.tvRoomTopic?.text = mRoomSummary?.topic
                }

                throw exception
            }
        }
    }

    var isGroupEncryptionEnabled: Boolean
        get() = mRoom?.isEncrypted() ?: false
        set(enabled) {
            if (enabled) {
                mCoroutineScope.launch {
                    mRoom?.enableEncryption()
                }
            }
        }

    fun checkNotificationState() {
        val data = mRoom?.getLiveRoomNotificationState()
        data?.observe(this, {
            headerViewHolder?.checkNotifications?.isChecked = data.value == RoomNotificationState.ALL_MESSAGES
        })
    }

    fun setNotificationsEnabled(enabled: Boolean) {
        mCoroutineScope.launch {
            mRoom?.setRoomNotificationState(if (enabled) RoomNotificationState.ALL_MESSAGES else RoomNotificationState.MUTE)
        }
    }

    private fun confirmLeaveGroup() {
        AlertDialog.Builder(this)
                .setTitle(getString(R.string.confirm_leave_group_title))
                .setMessage(getString(R.string.confirm_leave_group))
                .setPositiveButton(getString(R.string.action_leave)) { dialog: DialogInterface, _: Int ->
                    dialog.dismiss()
                    leaveGroup()
                }
                .setNeutralButton(getString(R.string.action_archive)) { _: DialogInterface?, _: Int -> archiveGroup() }
                .setNegativeButton(android.R.string.cancel, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show()
    }

    private fun archiveGroup() {
        try {
            // TODO archive

            // Clear the stack and go back to the main activity.
            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)

            startActivity(intent)
        }
        catch (e: Exception) {
            Timber.e(e, "error leaving group")
        }
    }

    private fun leaveGroup() {
        try {
            mRoom?.leave(null, object : MatrixCallback<Unit> {})

            // Clear the stack and go back to the main activity.
            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)

            startActivity(intent)
        }
        catch (e: Exception) {
            Timber.e(e, "error leaving group")
        }
    }

    private fun showAddFriends() {
        if (mActionAddFriends == null) return

        if (!canInviteOthers(mYou)) {
            mActionAddFriends?.visibility = View.GONE
            mActionShare?.visibility = View.GONE
        }
        else {
            mActionAddFriends?.setOnClickListener {
                val usernames = mMembers.map { it.username } as ArrayList<String?>

                val intent = Intent(this@GroupDisplayActivity, ContactsPickerActivity::class.java)
                intent.putExtra(ContactsPickerActivity.EXTRA_EXCLUDED_CONTACTS, usernames)

                mPickContacts.launch(intent)
            }

            mActionShare?.visibility = View.VISIBLE
            mActionAddFriends?.visibility = View.VISIBLE
            mActionAddFriends?.isEnabled = true
        }
    }

    private fun canChangeName(member: GroupMember?): Boolean {
        return member?.role is Role.Admin || member?.role is Role.Moderator
    }

    private fun canInviteOthers(member: GroupMember?): Boolean {
        return canChangeName(member)
    }

    fun canGrantAdmin(granter: GroupMember?): Boolean {
        return granter?.role is Role.Admin || granter?.role is Role.Moderator
    }

    fun canRevokeMembership(revoker: GroupMember?): Boolean {
        return canGrantAdmin(revoker)
    }

    /**
     * Create a chooser intent to select the source to get image from.
     *
     * The source can be camera's (ACTION_IMAGE_CAPTURE) or gallery's (ACTION_GET_CONTENT).
     *
     * All possible sources are added to the intent chooser.
     */
    private val pickImageChooserIntent: Intent
        get() {
            // Determine Uri of camera image to save.
            val outputFileUri = captureImageOutputUri
            val allIntents = ArrayList<Intent>()

            // Collect all camera intents.
            val captureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val listCam = packageManager.queryIntentActivities(captureIntent, 0)

            for (res in listCam) {
                val intent = Intent(captureIntent)
                intent.component = ComponentName(res.activityInfo.packageName, res.activityInfo.name)
                intent.setPackage(res.activityInfo.packageName)
                outputFileUri?.let { intent.putExtra(MediaStore.EXTRA_OUTPUT, it) }

                allIntents.add(intent)
            }

            // Collect all gallery intents.
            val galleryIntent = Intent(Intent.ACTION_GET_CONTENT)
            galleryIntent.type = "image/*"
            val listGallery = packageManager.queryIntentActivities(galleryIntent, 0)

            for (res in listGallery) {
                val intent = Intent(galleryIntent)
                intent.component = ComponentName(res.activityInfo.packageName, res.activityInfo.name)
                intent.setPackage(res.activityInfo.packageName)

                allIntents.add(intent)
            }

            // The main intent is the last in the list (fucking android) so pickup the useless one.
            var mainIntent = allIntents[allIntents.size - 1]
            for (intent in allIntents) {
                if (intent.component?.className == "com.android.documentsui.DocumentsActivity") {
                    mainIntent = intent
                    break
                }
            }
            allIntents.remove(mainIntent)

            // Create a chooser from the main intent.
            val chooserIntent = Intent.createChooser(mainIntent, getString(R.string.choose_photos))

            // Add all other intents.
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toTypedArray<Parcelable>())

            return chooserIntent
        }

    /**
     * Get URI to image received from capture by camera.
     */
    private val captureImageOutputUri: Uri?
        get() {
            if (externalCacheDir == null) return null

            return FileProvider.getUriForFile(this,
                    "$packageName.provider",
                    File(externalCacheDir, "pickImageResult.jpg"))
        }

    /**
     * Get the URI of the selected image from [.getPickImageChooserIntent].
     *
     * @param data the returned data of the activity result
     * @return the correct URI for camera and gallery image.
     */
    private fun getPickImageResultUri(data: Intent?): Uri? {
        if (data?.data != null && data.action != MediaStore.ACTION_IMAGE_CAPTURE) {
            return data.data
        }

        return captureImageOutputUri
    }

    private fun setAvatar(bmp: Bitmap) {
        headerViewHolder?.avatar?.setImageDrawable(BitmapDrawable(resources, bmp))

        mCoroutineScope.launch {
            @Suppress("BlockingMethodInNonBlockingContext")
            val file = File.createTempFile("avatar", "jpg")

            file.outputStream().use { os ->
                bmp.compress(Bitmap.CompressFormat.JPEG, 90, os)
            }

            mRoom?.updateAvatar(Uri.fromFile(file), file.name)

            Timber.d("Room avatar update successful.")
        }

        // TODO: Find out what this was for and reimplement.
//        try {
//            byte[] avatarBytesCompressed = stream.toByteArray();
//        }
//        catch (e: Exception) {
//            Timber.w(e, "error loading image bytes")
//        }
    }

    /**
     * Creates an alias for the room and sets the first alias of all aliases found as the canonical
     * alias. In other words: This should only be used, if the room has no alias, yet.
     */
    private fun createAlias() {
        lifecycleScope.launch {
            val content = AlertDialogEdittextBinding.inflate(layoutInflater, mBinding.root, false)

            AlertDialog.Builder(this@GroupDisplayActivity)
                    .setTitle(R.string.Create_Published_Address)
                    .setMessage(R.string.To_be_able_to_share_a_room_)
                    .setView(content.root)
                    .setNegativeButton(R.string.cancel, null)
                    .setPositiveButton(R.string.ok) { _: DialogInterface, _: Int ->
                        val alias = content.editText.text.toString()

                        if (alias.isBlank()) return@setPositiveButton

                        mCoroutineScope.launch {
                            mRoom?.addAlias(alias)

                            mRoom?.getRoomAliases()?.let {
                                // We take the first, that should be the only one. We shouldn't be here
                                // otherwise. (Or someone changed the call logic...)
                                mRoom?.updateCanonicalAlias(it.firstOrNull(), it)

                                showShareSheet(it.first())
                            }
                        }
                    }
                    .show()
        }
    }

    /**
     * Checks, if the room is set public, and if not tries to set so.
     *
     * When done, creates a QR code and a link to share, so other people can be invited to this
     * room via some other messenger or the likes.
     */
    private fun showShareSheet(alias: String) {

        @Suppress("BlockingMethodInNonBlockingContext")
        val inviteLink = OnboardingManager.generateInviteLink(URLEncoder.encode(alias, "UTF-8"))

        mCoroutineScope.launch {
            if (mRoom?.isPublic() == false) {

                // Set room to public, so users with link can actually join, but not guests!
                mRoom?.updateJoinRule(RoomJoinRules.PUBLIC, GuestAccess.Forbidden)
            }

            QrShareAsyncTask().shareQrCode(this@GroupDisplayActivity, inviteLink)
        }
    }
}