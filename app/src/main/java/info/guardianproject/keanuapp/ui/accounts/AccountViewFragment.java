/*
 * Copyright (C) 2008 Esmertec AG. Copyright (C) 2008 The Android Open Source
 * Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * * Unless required by applicable law or agreed to in writing, software * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package info.guardianproject.keanuapp.ui.accounts;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.provider.BaseColumns;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Locale;

import info.guardianproject.keanu.core.model.ProviderDef;
import info.guardianproject.keanu.core.model.Server;
import info.guardianproject.keanu.core.service.IImConnection;
import info.guardianproject.keanu.core.service.ImServiceConstants;
import info.guardianproject.keanu.core.service.RemoteImService;
import info.guardianproject.keanu.core.util.LogCleaner;
import info.guardianproject.keanu.core.util.XmppUriHelper;
import info.guardianproject.keanuapp.ImApp;
import info.guardianproject.keanuapp.R;
import info.guardianproject.keanuapp.ui.legacy.SimpleAlertHandler;

import static info.guardianproject.keanu.core.KeanuConstants.LOG_TAG;


public class AccountViewFragment extends Fragment {

    public static final String TAG = "AccountActivity";
    private static final String ACCOUNT_URI_KEY = "accountUri";
    private long mProviderId = 0;
    private long mAccountId = 0;
    static final int REQUEST_SIGN_IN = 100001;
    private static final int ACCOUNT_PROVIDER_COLUMN = 1;
    private static final int ACCOUNT_USERNAME_COLUMN = 2;
    private static final int ACCOUNT_PASSWORD_COLUMN = 3;
    
    private static final String USERNAME_VALIDATOR = "[^a-z0-9\\.\\-_\\+]";
    //    private static final int ACCOUNT_KEEP_SIGNED_IN_COLUMN = 4;
    //    private static final int ACCOUNT_LAST_LOGIN_STATE = 5;

    Uri mAccountUri;
    EditText mEditUserAccount;
    EditText mEditPass;
    EditText mEditPassConfirm;
  //  CheckBox mRememberPass;
   // CheckBox mUseTor;
    Button mBtnSignIn;
    Button mBtnQrDisplay;
    AutoCompleteTextView mSpinnerDomains;

    Button mBtnAdvanced;
    TextView mTxtFingerprint;

    //Imps.ProviderSettings.QueryMap settings;

    boolean isEdit = false;
    boolean isSignedIn = false;

    String mUserName = "";
    String mDomain = "";
    int mPort = 0;
    private String mOriginalUserAccount = "";

    private final static int DEFAULT_PORT = 5222;

    private boolean mIsNewAccount = false;

    private AsyncTask<Void, Void, String> mCreateAccountTask = null;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (mApp == null)
            initFragment();
    }

    private void initFragment()
    {

        Intent i = getIntent();

        mApp = (ImApp)getActivity().getApplication();

        String action = i.getAction();

        if (i.hasExtra("isSignedIn"))
            isSignedIn = i.getBooleanExtra("isSignedIn", false);


        final ProviderDef provider;


        ContentResolver cr = getActivity().getContentResolver();

        Uri uri = i.getData();
        action = Intent.ACTION_EDIT;


        if (Intent.ACTION_INSERT.equals(action) && uri.getScheme().equals("ima")) {
            /**
            ImPluginHelper helper = ImPluginHelper.getInstance(getActivity());
            String authority = uri.getAuthority();
            String[] userpass_host = authority.split("@");
            String[] user_pass = userpass_host[0].split(":");
            mUserName = user_pass[0].toLowerCase(Locale.getDefault());
            String pass = user_pass[1];
            mDomain = userpass_host[1].toLowerCase(Locale.getDefault());
            mPort = 0;
            final boolean regWithTor = i.getBooleanExtra("useTor", false);

            Cursor cursor = openAccountByUsernameAndDomain(cr);
            boolean exists = cursor.moveToFirst();
            long accountId;
            if (exists) {
                accountId = cursor.getLong(0);
                mAccountUri = ContentUris.withAppendedId(Imps.Account.CONTENT_URI, accountId);
                pass = cursor.getString(ACCOUNT_PASSWORD_COLUMN);

                setAccountKeepSignedIn(true);
                mSignInHelper.activateAccount(mProviderId, accountId);
                mSignInHelper.signIn(pass, mProviderId, accountId, true);
              //  setResult(RESULT_OK);
                cursor.close();
               // finish();
                return;

            } else {
                mProviderId = helper.createAdditionalProvider(helper.getProviderNames().get(0)); //xmpp FIXME
                accountId = ImApp.insertOrUpdateAccount(cr, mProviderId, -1, mUserName, mUserName, pass);
                mAccountUri = ContentUris.withAppendedId(Imps.Account.CONTENT_URI, accountId);
                mSignInHelper.activateAccount(mProviderId, accountId);
                createNewAccount(mUserName, pass, accountId, regWithTor);
                cursor.close();
                return;
            }
            **/



        } else if (Intent.ACTION_INSERT.equals(action)) {


            mOriginalUserAccount = "";
            // TODO once we implement multiple IM protocols
            mProviderId = ContentUris.parseId(uri);
            //provider = ProviderManager.getProvider(getActivity(),mProviderId);



        } else if (Intent.ACTION_EDIT.equals(action)) {




            isEdit = true;


            /**

            mAccountId = cursor.getLong(cursor.getColumnIndexOrThrow(BaseColumns._ID));

            mProviderId = cursor.getLong(ACCOUNT_PROVIDER_COLUMN);
            //provider = ProviderManager.getProvider(getActivity(),mProviderId);

            Cursor pCursor = cr.query(Imps.ProviderSettings.CONTENT_URI,new String[] {Imps.ProviderSettings.NAME, Imps.ProviderSettings.VALUE},Imps.ProviderSettings.PROVIDER + "=?",new String[] { Long.toString(mProviderId)},null);

            Imps.ProviderSettings.QueryMap settings = new Imps.ProviderSettings.QueryMap(
                    pCursor, cr, mProviderId, false , null );

            try {
                mOriginalUserAccount = cursor.getString(ACCOUNT_USERNAME_COLUMN) + "@"
                                       + settings.getDomain();
                mEditUserAccount.setText(mOriginalUserAccount);
                mEditPass.setText(cursor.getString(ACCOUNT_PASSWORD_COLUMN));
                //mRememberPass.setChecked(!cursor.isNull(ACCOUNT_PASSWORD_COLUMN));
                //mUseTor.setChecked(settings.getUseTor());
                mBtnQrDisplay.setVisibility(View.VISIBLE);
            } finally {
                settings.close();
                cursor.close();
            }
            **/


        } else {
            LogCleaner.warn(LOG_TAG, "<AccountActivity> unknown intent action " + action);
            return;
        }

       setupUIPost();

    }

    private Intent getIntent ()
    {
      return  getActivity().getIntent();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.account_activity, container, false);


        mIsNewAccount = getIntent().getBooleanExtra("register", false);


        mEditUserAccount = (EditText) view.findViewById(R.id.edtName);
        mEditUserAccount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                checkUserChanged();
            }
        });

        mEditPass = (EditText) view.findViewById(R.id.edtPass);

        mEditPassConfirm = (EditText) view.findViewById(R.id.edtPassConfirm);
        mSpinnerDomains = (AutoCompleteTextView) view.findViewById(R.id.spinnerDomains);

        if (mIsNewAccount)
        {
            mEditPassConfirm.setVisibility(View.VISIBLE);
            mSpinnerDomains.setVisibility(View.VISIBLE);
            mEditUserAccount.setHint(R.string.account_setup_new_username);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_dropdown_item_1line, Server.getServersText(getActivity()));
            mSpinnerDomains.setAdapter(adapter);

        }

//        mRememberPass = (CheckBox) findViewById(R.id.rememberPassword);
  //      mUseTor = (CheckBox) findViewById(R.id.useTor);


        mBtnSignIn = (Button) view.findViewById(R.id.btnSignIn);

        if (mIsNewAccount)
            mBtnSignIn.setText(R.string.btn_create_new_account);


        //mBtnAdvanced = (Button) findViewById(R.id.btnAdvanced);
       // mBtnQrDisplay = (Button) findViewById(R.id.btnQR);

        /*
        mRememberPass.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updateWidgetState();
            }
        });*/

        return view;

    }

    private void setupUIPost ()
    {
        Intent i = getIntent();

        if (isSignedIn) {
            mBtnSignIn.setText(getString(R.string.menu_sign_out));
            mBtnSignIn.setBackgroundResource(R.drawable.btn_red);
        }

        mEditUserAccount.addTextChangedListener(mTextWatcher);
        mEditPass.addTextChangedListener(mTextWatcher);

        mBtnAdvanced.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                showAdvanced();
            }
        });


        mBtnQrDisplay.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v) {

               showQR();

            }

        });


        mBtnSignIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                checkUserChanged();

                /**
                if (mUseTor.isChecked())
                {
                    OrbotHelper oh = new OrbotHelper(AccountActivity.this);
                    if (!oh.isOrbotRunning())
                    {
                        oh.requestOrbotStart(AccountActivity.this);
                        return;
                    }
                }*/


                final String pass = mEditPass.getText().toString();
                final String passConf = mEditPassConfirm.getText().toString();
                final boolean rememberPass = true;
                final boolean isActive = false; // TODO(miron) does this ever need to be true?
                ContentResolver cr = getActivity().getContentResolver();

                if (mIsNewAccount)
                {
                    mDomain = mSpinnerDomains.getText().toString();
                    String fullUser = mEditUserAccount.getText().toString();

                    if (fullUser.indexOf("@")==-1)
                        fullUser += '@' + mDomain;

                    if (!parseAccount(fullUser)) {
                        mEditUserAccount.selectAll();
                        mEditUserAccount.requestFocus();
                        return;
                    }

                    mProviderId = 1234;//helper.createAdditionalProvider(helper.getProviderNames().get(0)); //xmpp FIXME

                }
                else
                {
                    if (!parseAccount(mEditUserAccount.getText().toString())) {
                        mEditUserAccount.selectAll();
                        mEditUserAccount.requestFocus();
                        return;
                    }
                    else
                    {
                        settingsForDomain(mDomain,mPort);//apply final settings
                    }
                }




//                mAccountUri = ContentUris.withAppendedId(Imps.Account.CONTENT_URI, mAccountId);

                //if remember pass is true, set the "keep signed in" property to true
                if (mIsNewAccount)
                {
                    if (pass.equals(passConf))
                    {
                        setAccountKeepSignedIn(rememberPass);

//                        createNewAccount(mUserName, pass, mAccountId, false);

                    }
                    else
                    {
                       Toast.makeText(getActivity(), getString(R.string.error_account_password_mismatch), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    if (isSignedIn) {
                        signOut();
                        isSignedIn = false;
                    } else {
                        setAccountKeepSignedIn(rememberPass);
                        //mSignInHelper.signIn(pass, mProviderId, mAccountId, isActive);

                        isSignedIn = true;
                    }
                    updateWidgetState();
                }

            }
        });

        /**
        mUseTor.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updateUseTor(isChecked);
            }
        });*/

        updateWidgetState();

        if (i.hasExtra("title"))
        {
            String title = i.getExtras().getString("title");
        }

        if (i.hasExtra("newuser"))
        {
            String newuser = i.getExtras().getString("newuser");
            mEditUserAccount.setText(newuser);

            parseAccount(newuser);
            settingsForDomain(mDomain,mPort);

        }

        if (i.hasExtra("newpass"))
        {
            mEditPass.setText(i.getExtras().getString("newpass"));
            mEditPass.setVisibility(View.GONE);
            //mRememberPass.setChecked(true);
            //mRememberPass.setVisibility(View.GONE);
        }

        if (i.getBooleanExtra("hideTor", false))
        {
            //mUseTor.setVisibility(View.GONE);
        }

    }



    /*
    @Override
    protected void onDestroy() {

        if (mCreateAccountTask != null && (!mCreateAccountTask.isCancelled()))
        {
            mCreateAccountTask.cancel(true);
        }

        if (mSignInHelper != null)
            mSignInHelper.stop();

        super.onDestroy();
    }*/

/*
    private void getOTRKeyInfo() {

        if (mApp != null && FFF != null) {
            try {
                otrKeyManager = mApp.getRemoteImService().getOtrKeyManager(mOriginalUserAccount);

                if (otrKeyManager == null) {
                    mTxtFingerprint = ((TextView) findViewById(R.id.txtFingerprint));

                    String localFingerprint = otrKeyManager.getLocalFingerprint();
                    if (localFingerprint != null) {
                        ((TextView) findViewById(R.id.lblFingerprint)).setVisibility(View.VISIBLE);
                        mTxtFingerprint.setText(processFingerprint(localFingerprint));
                    } else {
                        ((TextView) findViewById(R.id.lblFingerprint)).setVisibility(View.GONE);
                        mTxtFingerprint.setText("");
                    }
                } else {
                    //don't need to notify people if there is nothing to show here
//                    Toast.makeText(this, "OTR is not initialized yet", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                Log.e(ImApp.LOG_TAG, "error on create", e);

            }
        }

    }*/

    private void checkUserChanged() {
        if (mEditUserAccount != null)
        {
            String username = mEditUserAccount.getText().toString().trim().toLowerCase();

            if ((!username.equals(mOriginalUserAccount)) && parseAccount(username)) {
                //Log.i(TAG, "Username changed: " + mOriginalUserAccount + " != " + username);
                settingsForDomain(mDomain, mPort);
                mOriginalUserAccount = username;

            }
        }


    }

    boolean parseAccount(String userField) {
        boolean isGood = true;
        String[] splitAt = userField.trim().split("@");
        mUserName = splitAt[0].toLowerCase(Locale.ENGLISH).replaceAll(USERNAME_VALIDATOR, "");
        mDomain = "";
        mPort = 0;

        if (splitAt.length > 1) {
            mDomain = splitAt[1].toLowerCase(Locale.ENGLISH);
            String[] splitColon = mDomain.split(":");
            mDomain = splitColon[0].toLowerCase(Locale.ENGLISH);
            if (splitColon.length > 1) {
                try {
                    mPort = Integer.parseInt(splitColon[1]);
                } catch (NumberFormatException e) {
                    // TODO move these strings to strings.xml
                    isGood = false;
                    Toast.makeText(
                            getActivity(),
                            "The port value '" + splitColon[1]
                                    + "' after the : could not be parsed as a number!",
                            Toast.LENGTH_LONG).show();
                }
            }
        }

        //its okay if domain is null;

//        if (mDomain == null) {
  //          isGood = false;
            //Toast.makeText(AccountActivity.this,
            //	R.string.account_wizard_no_domain_warning,
            //	Toast.LENGTH_LONG).show();
    //    }
        /*//removing requirement of a . in the domain
        else if (mDomain.indexOf(".") == -1) {
            isGood = false;
            //	Toast.makeText(AccountActivity.this,
            //		R.string.account_wizard_no_root_domain_warning,
            //	Toast.LENGTH_LONG).show();
        }*/

        return isGood;
    }

    /*
     * If we know the direct XMPP server for a domain, we should turn off DNS lookup
     * because it is slow, error prone, and a way to leak information from third parties
     */


    private void settingsForDomain(String domain, int port) {

        /**
        settings.setRequireTls(true);
        settings.setTlsCertVerify(true);
        settings.setAllowPlainAuth(false);
        settings.setPort(DEFAULT_PORT);

        settings.setDomain(domain);
        settings.setPort(port);

        settings.requery();
         **/
    }

    private Handler mHandler = new Handler();
    private ImApp mApp = null;

    void signOut() {


    }

    void signOut(long providerId, long accountId) {


    }


    void updateWidgetState() {
        boolean goodUsername = mEditUserAccount.getText().length() > 0;
        boolean goodPassword = mEditPass.getText().length() > 0;
        boolean hasNameAndPassword = goodUsername && goodPassword;

        mEditPass.setEnabled(goodUsername);
        mEditPass.setFocusable(goodUsername);
        mEditPass.setFocusableInTouchMode(goodUsername);

        //mRememberPass.setEnabled(hasNameAndPassword);
        //mRememberPass.setFocusable(hasNameAndPassword);

        mEditUserAccount.setEnabled(!isSignedIn);
        mEditPass.setEnabled(!isSignedIn);

        if (!isSignedIn) {
            mBtnSignIn.setEnabled(hasNameAndPassword);
            mBtnSignIn.setFocusable(hasNameAndPassword);
        }
        else
        {

        }
    }

    private final TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int before, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int after) {
            updateWidgetState();

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void deleteAccount ()
    {


    }

    private void showAdvanced() {

        checkUserChanged();

        Intent intent = new Intent(getActivity(), AccountSettingsActivity.class);
        intent.putExtra(ImServiceConstants.EXTRA_INTENT_PROVIDER_ID, mProviderId);
        startActivity(intent);
    }



    public void showQR ()
    {
           String localFingerprint = "";// OtrAndroidKeyManagerImpl.getInstance(getActivity()).getLocalFingerprint(mOriginalUserAccount);
           String uri = XmppUriHelper.getUri(mOriginalUserAccount, localFingerprint);
         //  new IntentIntegrator(this).shareText(uri);
    }

    private void setAccountKeepSignedIn(final boolean rememberPass) {
        ContentValues values = new ContentValues();
       // values.put(Imps.AccountColumns.KEEP_SIGNED_IN, rememberPass ? 1 : 0);
        getActivity().getContentResolver().update(mAccountUri, values, null, null);
    }
}
