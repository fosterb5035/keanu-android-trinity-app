package info.guardianproject.keanuapp.ui.contacts

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import info.guardianproject.keanuapp.ImApp
import info.guardianproject.keanuapp.MainActivity
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.AwesomeActivityContactBinding
import info.guardianproject.keanuapp.ui.BaseActivity
import info.guardianproject.keanu.core.util.GlideUtils
import org.matrix.android.sdk.api.session.content.ContentUrlResolver.ThumbnailMethod
import org.matrix.android.sdk.api.session.user.model.User

class ContactDisplayActivity : BaseActivity() {

    private var mUserId: String = ""

    private var mUser: User? = null

    private lateinit var mBinding: AwesomeActivityContactBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = AwesomeActivityContactBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        setSupportActionBar(mBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeActionContentDescription(R.string.action_done)

        mUserId = intent.getStringExtra("address") ?: ""

        val session = (application as? ImApp)?.matrixSession
        mUser = session?.getUser(mUserId)

        title = ""
        mBinding.tvNickname.text = mUser?.displayName ?: mUserId
        mBinding.tvUsername.text = mUserId

        mBinding.btnStartChat.setOnClickListener { inviteContactToChat() }

        try {
            val avatarUrl = session?.contentUrlResolver()?.resolveThumbnail(mUser?.avatarUrl, 120, 120, ThumbnailMethod.SCALE)

            GlideUtils.loadImageFromUri(this, Uri.parse(avatarUrl), mBinding.imageAvatar)

            mBinding.imageAvatar.visibility = View.VISIBLE
            mBinding.imageSpacer.visibility = View.GONE
        }
        catch (ignored: Exception) {
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_contact_detail, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun inviteContactToChat() {
        val intent = Intent(this@ContactDisplayActivity, MainActivity::class.java)
        intent.putExtra("address", mUserId)
        intent.putExtra("new",true)

        startActivity(intent)

        finish()
    }

    fun viewDevicesClicked(cView: View) {
        val intent = Intent(this, DevicesActivity::class.java)
        intent.putExtra("userId", mUserId)

        startActivity(intent)
    }

    fun addFriendClicked(cView: View) {
        inviteContactToChat()
    }
}