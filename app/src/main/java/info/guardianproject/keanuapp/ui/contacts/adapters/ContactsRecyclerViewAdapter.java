package info.guardianproject.keanuapp.ui.contacts.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.matrix.android.sdk.api.session.Session;
import org.matrix.android.sdk.api.session.content.ContentUrlResolver;
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams;
import org.matrix.android.sdk.api.session.room.model.RoomSummary;
import org.matrix.android.sdk.api.session.user.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import info.guardianproject.keanuapp.ImApp;
import info.guardianproject.keanuapp.R;
import info.guardianproject.keanuapp.ui.contacts.ContactDisplayActivity;
import info.guardianproject.keanuapp.ui.contacts.ContactListItem;
import info.guardianproject.keanuapp.ui.contacts.ContactViewHolder;
import info.guardianproject.keanuapp.ui.contacts.ContactsListFragment;

public class ContactsRecyclerViewAdapter
        extends RecyclerView.Adapter<ContactViewHolder> {

    private final TypedValue mTypedValue = new TypedValue();
    private int mBackground;
    private Activity mContext;
    private ContactsListFragment mFrag;
    private String mSearchString;

    private Session mSession;
    private ArrayList<User> mContactList = new ArrayList<>();

    public ContactsRecyclerViewAdapter(final ContactsListFragment frag) {
        super();
        mFrag = frag;
        mContext = frag.getActivity();
        mContext.getTheme().resolveAttribute(R.attr.chatBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        setHasStableIds(true);

        mSession = ((ImApp)mContext.getApplication()).getMatrixSession();

        RoomSummaryQueryParams.Builder builder = new RoomSummaryQueryParams.Builder();

        updateContacts(mSession.getRoomSummaries(builder.build()));

        LiveData<List<RoomSummary>> list = mSession.getRoomSummariesLive(builder.build());

        list.observe(frag, roomSummaries -> {

            updateContacts (roomSummaries);

        });
    }

    private void updateContacts (List<RoomSummary> roomSummaries)
    {
        HashMap<String,User> userMap = new HashMap<>();

        for (RoomSummary rs : roomSummaries)
        {
            if (rs.isDirect()||(rs.getJoinedMembersCount()<=2&&rs.getOtherMemberIds().size()>0)) {

                if (!rs.getOtherMemberIds().isEmpty())
                    userMap.put(rs.getOtherMemberIds().get(0),mSession.getUser(rs.getOtherMemberIds().get(0)));
            }
        }

        mContactList = new ArrayList<>(userMap.values());

        notifyDataSetChanged();
        mFrag.updateListVisibility();
    }

    public long getItemId (int position)
    {
        return position;
    }

    @Override
    public int getItemCount() {
        return mContactList.size();
    }


    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_view, parent, false);
        view.setBackgroundResource(mBackground);

        ContactViewHolder viewHolder = (ContactViewHolder)view.getTag();

        if (viewHolder == null) {
            viewHolder = new ContactViewHolder(view);
            view.setTag(viewHolder);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder viewHolder, int position) {


        User user = mContactList.get(position);
        String avatarUrl = null;


        final String nickname = user.getDisplayName();
        final String address = user.getUserId();

        avatarUrl = user.getAvatarUrl();
        avatarUrl = mSession.contentUrlResolver().resolveThumbnail(avatarUrl,120,120, ContentUrlResolver.ThumbnailMethod.SCALE);

        ContactListItem clItem = ((ContactListItem) viewHolder.itemView);

        clItem.bind(viewHolder,address,nickname,avatarUrl,null,null);

        clItem.setOnClickListener(v -> {
            Context context = v.getContext();

            Intent intent = new Intent(context, ContactDisplayActivity.class);
            intent.putExtra("address", address);
            context.startActivity(intent);
        });



    }



}