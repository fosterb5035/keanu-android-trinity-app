package info.guardianproject.keanuapp.ui.onboarding

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/**
 * Created by n8fr8 on 6/4/15.
 */
@Parcelize

class OnboardingAccount @JvmOverloads constructor(
        @JvmField
        val nickname: String? = null,

        @JvmField
        val username: String? = null,

        @JvmField
        val server: String? = null,

        @JvmField
        val password: String? = null,

        @JvmField
        val providerId: Long = 0,

        @JvmField
        val accountId: Long = 0
) : Parcelable