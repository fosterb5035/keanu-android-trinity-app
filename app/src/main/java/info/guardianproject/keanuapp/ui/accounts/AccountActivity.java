/*
 * Copyright (C) 2008 Esmertec AG. Copyright (C) 2008 The Android Open Source
 * Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * * Unless required by applicable law or agreed to in writing, software * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package info.guardianproject.keanuapp.ui.accounts;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.provider.BaseColumns;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import info.guardianproject.keanu.core.model.ProviderDef;
import info.guardianproject.keanu.core.service.IImConnection;
import info.guardianproject.keanu.core.service.ImServiceConstants;
import info.guardianproject.keanu.core.service.RemoteImService;
import info.guardianproject.keanu.core.util.LogCleaner;
import info.guardianproject.keanu.core.util.XmppUriHelper;

import java.util.HashMap;
import java.util.Locale;

import info.guardianproject.keanuapp.R;
import info.guardianproject.keanuapp.ImApp;
import info.guardianproject.keanuapp.ui.BaseActivity;
import info.guardianproject.keanuapp.ui.qr.zxing.integration.android.IntentIntegrator;

import static info.guardianproject.keanu.core.KeanuConstants.LOG_TAG;

public class AccountActivity extends BaseActivity {

    public static final String TAG = "AccountActivity";
    private static final String ACCOUNT_URI_KEY = "accountUri";
    private long mProviderId = 0;
    private long mAccountId = 0;
    static final int REQUEST_SIGN_IN = RESULT_FIRST_USER + 1;
    private static final int ACCOUNT_PROVIDER_COLUMN = 1;
    private static final int ACCOUNT_USERNAME_COLUMN = 2;
    private static final int ACCOUNT_PASSWORD_COLUMN = 3;

    private static final String USERNAME_VALIDATOR = "[^a-z0-9\\.\\-_\\+]";
    //    private static final int ACCOUNT_KEEP_SIGNED_IN_COLUMN = 4;
    //    private static final int ACCOUNT_LAST_LOGIN_STATE = 5;

    Uri mAccountUri;
    EditText mEditUserAccount;
    EditText mEditPass;
    EditText mEditPassConfirm;
   // CheckBox mRememberPass;
   // CheckBox mUseTor;
 //   Button mBtnSignIn;
//    Button mBtnQrDisplay;
    AutoCompleteTextView mSpinnerDomains;

    Button mBtnAdvanced;
    TextView mTxtFingerprint;

    //Imps.ProviderSettings.QueryMap settings;

    boolean isEdit = false;
    boolean isSignedIn = false;

    String mUserName = "";
    String mDomain = "";
    int mPort = 0;
    private String mOriginalUserAccount = "";


    private boolean mIsNewAccount = false;

    private AsyncTask<Void, Void, String> mCreateAccountTask = null;

    @Override
    protected void onCreate(Bundle icicle) {

        super.onCreate(icicle);
        Intent i = getIntent();

        mApp = (ImApp)getApplication();

        String action = i.getAction();

        if (i.hasExtra("isSignedIn"))
            isSignedIn = i.getBooleanExtra("isSignedIn", false);


        ContentResolver cr = getContentResolver();

        Uri uri = i.getData();
        // check if there is account information and direct accordingly

        action = Intent.ACTION_EDIT;


        if (Intent.ACTION_INSERT.equals(action) && uri.getScheme().equals("ima")) {

            String authority = uri.getAuthority();
            String[] userpass_host = authority.split("@");
            String[] user_pass = userpass_host[0].split(":");
            mUserName = user_pass[0].toLowerCase(Locale.getDefault());
            String pass = user_pass[1];
            mDomain = userpass_host[1].toLowerCase(Locale.getDefault());
            mPort = 0;

            /**

            Cursor cursor = openAccountByUsernameAndDomain(cr);
            boolean exists = cursor.moveToFirst();
            long accountId;
            if (exists) {
                accountId = cursor.getLong(0);
                //mAccountUri = ContentUris.withAppendedId(Imps.Account.CONTENT_URI, accountId);
                pass = cursor.getString(ACCOUNT_PASSWORD_COLUMN);

                setAccountKeepSignedIn(true);
                setResult(RESULT_OK);
                cursor.close();
                finish();
                return;

            } else {
                mProviderId = 1234; //TODO generate unique id
                accountId = ImApp.insertOrUpdateAccount(cr, mProviderId, mAccountId,mUserName, mUserName, pass);

                mAccountUri = ContentUris.withAppendedId(Imps.Account.CONTENT_URI, accountId);
                mSignInHelper.activateAccount(mProviderId, accountId);
                createNewAccount(mUserName, pass, accountId, regWithTor);

                cursor.close();
                return;
            }

            **/


        } else if (Intent.ACTION_INSERT.equals(action)) {


            setupUIPre();

            mOriginalUserAccount = "";
            // TODO once we implement multiple IM protocols
            mProviderId = ContentUris.parseId(uri);

            if (mProviderId == -1)
            {
                setTitle(getResources().getString(R.string.add_account, "matrix"));

            }
            else
            {
                finish();
            }


        } else if (Intent.ACTION_EDIT.equals(action)) {


            setupUIPre();

            isEdit = true;

            setTitle(R.string.sign_in);

            /**
            mAccountId = cursor.getLong(cursor.getColumnIndexOrThrow(BaseColumns._ID));

            mProviderId = cursor.getLong(ACCOUNT_PROVIDER_COLUMN);


            Cursor pCursor = cr.query(Imps.ProviderSettings.CONTENT_URI,new String[] {Imps.ProviderSettings.NAME, Imps.ProviderSettings.VALUE},Imps.ProviderSettings.PROVIDER + "=?",new String[] { Long.toString(mProviderId)},null);

            Imps.ProviderSettings.QueryMap settings = new Imps.ProviderSettings.QueryMap(
                    pCursor, cr, mProviderId, false , null );

            try {
                mOriginalUserAccount = cursor.getString(ACCOUNT_USERNAME_COLUMN) + "@"
                                       + settings.getDomain();
                mEditUserAccount.setText(mOriginalUserAccount);
                mEditPass.setText(cursor.getString(ACCOUNT_PASSWORD_COLUMN));

                mPort = settings.getPort();
                
            } finally {
                settings.close();
                cursor.close();
            }**/


        } else {
            LogCleaner.warn(LOG_TAG, "<AccountActivity> unknown intent action " + action);
            finish();
            return;
        }

       setupUIPost();

    }

    private void setupUIPre ()
    {
        ((ImApp)getApplication()).setAppTheme(this);

        setContentView(R.layout.account_activity);

        getSupportActionBar().setHomeButtonEnabled(true);

        mIsNewAccount = getIntent().getBooleanExtra("register", false);

        mEditUserAccount = (EditText) findViewById(R.id.edtName);
        mEditUserAccount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                checkUserChanged();
            }
        });

        mEditPass = (EditText) findViewById(R.id.edtPass);

        mEditPassConfirm = (EditText) findViewById(R.id.edtPassConfirm);
        mSpinnerDomains = (AutoCompleteTextView) findViewById(R.id.spinnerDomains);

        if (mIsNewAccount)
        {
            mEditPassConfirm.setVisibility(View.VISIBLE);
            mSpinnerDomains.setVisibility(View.VISIBLE);
            mEditUserAccount.setHint(R.string.account_setup_new_username);

            /*
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.account_domains));
            mSpinnerDomains.setAdapter(adapter);
            */

        }

        //mRememberPass = (CheckBox) findViewById(R.id.rememberPassword);
        //mUseTor = (CheckBox) findViewById(R.id.useTor);


       // mBtnSignIn = (Button) findViewById(R.id.btnSignIn);

//        if (mIsNewAccount)
  //          mBtnSignIn.setText(R.string.btn_create_new_account);

        mBtnAdvanced = (Button) findViewById(R.id.btnAdvanced);

        /**
        mRememberPass.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updateWidgetState();
            }
        });*/

    }

    private void setupUIPost ()
    {
        Intent i = getIntent();



        mEditUserAccount.addTextChangedListener(mTextWatcher);
        mEditPass.addTextChangedListener(mTextWatcher);

        /**
        mBtnAdvanced.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                showAdvanced();
            }
        });*/


        /**

        mBtnSignIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                checkUserChanged();

                final String pass = mEditPass.getText().toString();
                final String passConf = mEditPassConfirm.getText().toString();
                final boolean rememberPass =true;// mRememberPass.isChecked();
                final boolean isActive = false; // TODO(miron) does this ever need to be true?
                ContentResolver cr = getContentResolver();
                final boolean useTor = false;// mUseTor.isChecked();

                if (mIsNewAccount)
                {
                    mDomain = mSpinnerDomains.getText().toString();
                    String fullUser = mEditUserAccount.getText().toString();

                    if (fullUser.indexOf("@")==-1)
                        fullUser += '@' + mDomain;

                    if (!parseAccount(fullUser)) {
                        mEditUserAccount.selectAll();
                        mEditUserAccount.requestFocus();
                        return;
                    }

                    ImPluginHelper helper = ImPluginHelper.getInstance(AccountActivity.this);
                    mProviderId = helper.createAdditionalProvider(helper.getProviderNames().get(0)); //xmpp FIXME

                }
                else
                {
                    if (!parseAccount(mEditUserAccount.getText().toString())) {
                        mEditUserAccount.selectAll();
                        mEditUserAccount.requestFocus();
                        return;
                    }
                    else
                    {
                        settingsForDomain(mDomain,mPort);//apply final settings
                    }
                }


                mAccountId = ImApp.insertOrUpdateAccount(cr, mProviderId, mAccountId, mUserName, mUserName,
                        rememberPass ? pass : null);

                mAccountUri = ContentUris.withAppendedId(Imps.Account.CONTENT_URI, mAccountId);

                //if remember pass is true, set the "keep signed in" property to true
                if (mIsNewAccount)
                {
                    if (pass.equals(passConf))
                    {
                        setAccountKeepSignedIn(rememberPass);

                        createNewAccount(mUserName, pass, mAccountId, useTor);

                    }
                    else
                    {
                       Toast.makeText(AccountActivity.this, getString(R.string.error_account_password_mismatch), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    if (isSignedIn) {((MainActivity)getActivity()).showGroupChatDialog();
                        signOut();
                        isSignedIn = false;
                    } else {
                        setAccountKeepSignedIn(rememberPass);


                        boolean hasKey = checkForKey (mUserName + '@' + mDomain);

                        mSignInHelper.signIn(pass, mProviderId, mAccountId, isActive);


                        isSignedIn = true;
                        setResult(RESULT_OK);
                        finish();
                    }
                    updateWidgetState();


                }

            }
        });*/

        /**
        mUseTor.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updateUseTor(isChecked);
            }
        });*/

        updateWidgetState();

        if (i.hasExtra("title"))
        {
            String title = i.getExtras().getString("title");
            setTitle(title);
        }

        if (i.hasExtra("newuser"))
        {
            String newuser = i.getExtras().getString("newuser");
            mEditUserAccount.setText(newuser);

            parseAccount(newuser);
         //   settingsForDomain(mDomain,mPort);

        }

        if (i.hasExtra("newpass"))
        {
            mEditPass.setText(i.getExtras().getString("newpass"));
            mEditPass.setVisibility(View.GONE);
      //     mRememberPass.setChecked(true);((MainActivity)getActivity()).showGroupChatDialog();
       //     mRememberPass.setVisibility(View.GONE);
        }

        if (i.getBooleanExtra("hideTor", false))
        {
       //     mUseTor.setVisibility(View.GONE);
        }

    }




    @Override
    protected void onDestroy() {

        if (mCreateAccountTask != null && (!mCreateAccountTask.isCancelled()))
        {
            mCreateAccountTask.cancel(true);
        }

        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            checkUserChanged();
        }
        return super.onKeyDown(keyCode, event);
    }



    private void checkUserChanged() {
        if (mEditUserAccount != null)
        {
            String username = mEditUserAccount.getText().toString().trim().toLowerCase();

            if ((!username.equals(mOriginalUserAccount)) && parseAccount(username)) {
                //Log.i(TAG, "Username changed: " + mOriginalUserAccount + " != " + username);
//                settingsForDomain(mDomain, mPort);
                mOriginalUserAccount = username;

            }
        }


    }

    boolean parseAccount(String userField) {
        boolean isGood = true;
        String[] splitAt = userField.trim().split("@");
        mUserName = splitAt[0].toLowerCase(Locale.ENGLISH).replaceAll(USERNAME_VALIDATOR, "");
        mDomain = "";
        

        if (splitAt.length > 1) {
            mDomain = splitAt[1].toLowerCase(Locale.ENGLISH);
        }

        return isGood;
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mAccountUri = savedInstanceState.getParcelable(ACCOUNT_URI_KEY);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(ACCOUNT_URI_KEY, mAccountUri);
    }

    void signOutUsingActivity() {


    }

    private Handler mHandler = new Handler();
    private ImApp mApp = null;

    void signOut() {


    }

    void signOut(long providerId, long accountId) {


    }

    void createNewaccount (long accountId)
    {


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == REQUEST_SIGN_IN) {
            if (resultCode == RESULT_OK) {

                finish();
            } else {
                // sign in failed, let's show the screen!
            }
        }
    }

    void updateWidgetState() {
        boolean goodUsername = mEditUserAccount.getText().length() > 0;
        boolean goodPassword = mEditPass.getText().length() > 0;
        boolean hasNameAndPassword = goodUsername && goodPassword;

        mEditPass.setEnabled(goodUsername);
        mEditPass.setFocusable(goodUsername);
        mEditPass.setFocusableInTouchMode(goodUsername);

        //mRememberPass.setEnabled(hasNameAndPassword);
        //mRememberPass.setFocusable(hasNameAndPassword);

        mEditUserAccount.setEnabled(!isSignedIn);
        mEditPass.setEnabled(!isSignedIn);

        if (!isSignedIn) {
         //   mBtnSignIn.setEnabled(hasNameAndPassword);
          //  mBtnSignIn.setFocusable(hasNameAndPassword);
        }
        else
        {

        }
    }

    private final TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int before, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int after) {
            updateWidgetState();

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void deleteAccount ()
    {

        finish();
    }

    private void showAdvanced() {

        checkUserChanged();

        Intent intent = new Intent(this, AccountSettingsActivity.class);
        intent.putExtra(ImServiceConstants.EXTRA_INTENT_PROVIDER_ID, mProviderId);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_account_settings, menu);

        if (isEdit) {
            //add delete menu option
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

        case android.R.id.home:
            finish();
            return true;
/*
        case R.id.menu_account_delete:
            deleteAccount();
            return true;
*/
        }
        return super.onOptionsItemSelected(item);
    }

    public void createNewAccount (final String usernameNew, final String passwordNew, final long newAccountId, final boolean useTor)
    {
        if (mCreateAccountTask != null && (!mCreateAccountTask.isCancelled()))
        {
            mCreateAccountTask.cancel(true);
        }

        mCreateAccountTask = new AsyncTask<Void, Void, String>() {

            private ProgressDialog dialog;

            @Override
            protected void onPreExecute() {
                dialog = new ProgressDialog(AccountActivity.this);
                dialog.setCancelable(true);
                dialog.setMessage(getString(R.string.registering_new_account_));
                dialog.show();
            }

            @Override
            protected String doInBackground(Void... params) {

                return null;
              }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);

                try
                {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                        dialog = null;
                    }
                }
                catch ( IllegalArgumentException iae)
                {
                    //dialog may not be attached to window if Activity was closed
                }

                if (result != null)
                {
                    Toast.makeText(AccountActivity.this, "error creating account: " + result, Toast.LENGTH_LONG).show();
                    //AccountActivity.this.setResult(Activity.RESULT_CANCELED);
                    //AccountActivity.this.finish();
                }
                else
                {

                    AccountActivity.this.setResult(Activity.RESULT_OK);
                    AccountActivity.this.finish();
                }
            }
        }.execute();
    }

    public void showQR ()
    {
           String localFingerprint = "";//OtrAndroidKeyManagerImpl.getInstance(this).getLocalFingerprint(mOriginalUserAccount);
           String uri = XmppUriHelper.getUri(mOriginalUserAccount, localFingerprint);
           new IntentIntegrator(this).shareText(uri);
    }

    private void setAccountKeepSignedIn(final boolean rememberPass) {
        ContentValues values = new ContentValues();
     //   values.put(Imps.AccountColumns.KEEP_SIGNED_IN, rememberPass ? 1 : 0);
        getContentResolver().update(mAccountUri, values, null, null);
    }
}
