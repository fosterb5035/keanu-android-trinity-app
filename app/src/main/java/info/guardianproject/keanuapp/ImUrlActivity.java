/*
 * Copyright (C) 2008 Esmertec AG. Copyright (C) 2008 The Android Open Source
 * Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package info.guardianproject.keanuapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.UUID;

import info.guardianproject.keanu.core.model.ImErrorInfo;
import info.guardianproject.keanu.core.service.IChatSession;
import info.guardianproject.keanu.core.service.IChatSessionListener;
import info.guardianproject.keanu.core.service.IChatSessionManager;
import info.guardianproject.keanu.core.service.IImConnection;
import info.guardianproject.keanu.core.util.LogCleaner;
import info.guardianproject.keanu.core.util.SecureMediaStore;
import info.guardianproject.keanu.core.util.SystemServices;
import info.guardianproject.keanu.core.util.SystemServices.FileInfo;
import info.guardianproject.keanuapp.ui.LockScreenActivity;
import info.guardianproject.keanuapp.ui.accounts.AccountViewFragment;
import info.guardianproject.keanuapp.ui.contacts.AddContactActivity;
import info.guardianproject.keanuapp.ui.contacts.ContactsPickerActivity;
import info.guardianproject.keanu.core.ui.room.RoomActivity;
import info.guardianproject.keanuapp.ui.onboarding.OnboardingManager;

import static info.guardianproject.keanu.core.KeanuConstants.IMPS_CATEGORY;
import static info.guardianproject.keanu.core.KeanuConstants.LOG_TAG;

public class ImUrlActivity extends Activity {
    private static final String TAG = "ImUrlActivity";

    private static final int REQUEST_PICK_CONTACTS = RESULT_FIRST_USER + 1;
    private static final int REQUEST_CREATE_ACCOUNT = RESULT_FIRST_USER + 2;
    private static final int REQUEST_SIGNIN_ACCOUNT = RESULT_FIRST_USER + 3;
    private static final int REQUEST_START_MUC =  RESULT_FIRST_USER + 4;

    private String mProviderName;
    private String mToAddress;
    private String mFromAddress;
    private String mHost;

    private IImConnection mConn;
    private IChatSessionManager mChatSessionManager;

    private Uri mSendUri;
    private String mSendType;
    private String mSendText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        doOnCreate();
    }



    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        setIntent(intent);
    }

    public void onDBLocked() {

        Intent intent = new Intent(getApplicationContext(), RouterActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }


    private void showContactList(long accountId) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
      //  intent.setData(Imps.Contacts.CONTENT_URI);
        intent.addCategory(IMPS_CATEGORY);
        intent.putExtra("accountId", accountId);

        startActivity(intent);
    }

    private void openChat(long provider, long account) {


            Intent intent = new Intent(this, RoomActivity.class);
            intent.putExtra("address",mToAddress);
            startActivity(intent);



    }

    private boolean resolveInsertIntent(Intent intent) {
        Uri data = intent.getData();

        if (data.getScheme().equals("ima"))
        {
            createNewAccount();

            return true;
        }
        return false;
    }

   // private static final String USERNAME_PATTERN = "^[a-z0-9_-]{3,15}$";

    //private static final String USERNAME_NON_LETTERS_UNICODE = "[^\\p{L}\\p{Nd}]+";
    //private static final String USERNAME_NON_LETTERS_ALPHANUM = "[\\d[^\\w]]+";
    private static final String USERNAME_ONLY_ALPHANUM = "[^A-Za-z0-9]";

    private boolean resolveIntent(Intent intent) {
        Uri data = intent.getData();

        if (data == null)
            return false;

        mHost = data.getHost();

        if (data.getScheme().equals("https"))
        {
            //special keanu.im invite link: https://keanu.im/invite/<base64 encoded username?k=otrFingerprint

            try {
                //parse each string and if they are for a new user then add the user
                OnboardingManager.DecodedInviteLink diLink = OnboardingManager.decodeInviteLink(data.toString());
                ImApp app = (ImApp)getApplication();

                if (diLink.username.startsWith("@")) {
                    Intent intentAdd = new Intent(this, AddContactActivity.class);
                    intentAdd.putExtra("username", diLink.username);
                    startActivity(intentAdd);
                }
                else if (diLink.username.startsWith("!")) {
                    Intent intentAdd = new Intent(this, MainActivity.class);
                    intentAdd.setAction("join");
                    intentAdd.putExtra("group", diLink.username);
                    startActivity(intentAdd);
                }
                 //if they are for a group chat, then add the group
                return false; //the work is done so we will finish!
            }
            catch (Exception e)
            {
                Log.w(LOG_TAG, "error parsing QR invite link", e);
            }

        }
        else if (data.getScheme().equals("otr-in-band")) {
            this.openOtrInBand(data, intent.getType());

            return true;
        }


        if (Log.isLoggable(LOG_TAG, Log.DEBUG)) {
            log("resolveIntent: host=" + mHost);
        }


        if (Log.isLoggable(LOG_TAG, Log.DEBUG)) {
            log("resolveIntent: provider=" + mProviderName + ", to=" + mToAddress);
        }

        return true;
    }



    private boolean isValidToAddress() {
        if (TextUtils.isEmpty(mToAddress)) {
            return false;
        }

        if (mToAddress.indexOf('/') != -1) {
            return false;
        }

        return true;
    }

    private static void log(String msg) {
        Log.d(LOG_TAG, "<ImUrlActivity> " + msg);
    }

    void createNewAccount() {

        String username = getIntent().getData().getUserInfo();
        String appCreateAcct = String.format(getString(R.string.allow_s_to_create_a_new_chat_account_for_s_),username);

        new AlertDialog.Builder(this)
        .setTitle(R.string.prompt_create_new_account_)
        .setMessage(appCreateAcct)
        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                mHandlerRouter.sendEmptyMessage(1);
                dialog.dismiss();
            }
        })
        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                finish();
            }
        })
        .create().show();
    }

    Handler mHandlerRouter = new Handler ()
    {

        @Override
        public void handleMessage(Message msg) {

            if (msg.what == 1)
            {
                Uri uriAccountData = getIntent().getData();

                if (uriAccountData.getScheme().equals("immu"))
                {
                    //need to generate proper IMA url for account setup
                    String randomJid = ((int)(Math.random()*1000))+"";
                    String regUser = mFromAddress + randomJid;
                    String regPass =  UUID.randomUUID().toString().substring(0,16);
                    String regDomain = mHost.replace("conference.", "");
                    uriAccountData = Uri.parse("ima://" + regUser + ':' + regPass + '@' + regDomain);
                }

                Intent intent = new Intent(ImUrlActivity.this, AccountViewFragment.class);
                intent.setAction(Intent.ACTION_INSERT);
                intent.setData(uriAccountData);
                startActivityForResult(intent,REQUEST_CREATE_ACCOUNT);

            }
            else if (msg.what == 2)
            {
                doOnCreate();
            }
        }

    };

    void openOtrInBand(final Uri data, final String type) {

        if (type != null)
            mSendType = type;
        else
            mSendType = SystemServices.getMimeType(data.toString());
        
        if (mSendType != null ) {
            
            mSendUri = data;
            startContactPicker();
            return;
        }
        /**
        else  if (data.toString().startsWith(OtrDataHandler.URI_PREFIX_OTR_IN_BAND))
        {
             String localUrl = data.toString().replaceFirst(OtrDataHandler.URI_PREFIX_OTR_IN_BAND, "");
             FileInfo info = null;
             if (TextUtils.equals(data.getAuthority(), "com.android.contacts")) {
                 info = SystemServices.getContactAsVCardFile(this, data);
             } else {
                 info = SystemServices.getFileInfoFromURI(ImUrlActivity.this, data);
             }
             if (info != null && info.file.exists()) {
                 mSendUri = Uri.fromFile(info.file);
                 mSendType = type != null ? type : info.type;
                 startContactPicker();
                 return;
             }
        }**/
        
        Toast.makeText(this, R.string.unsupported_incoming_data, Toast.LENGTH_LONG).show();
        finish(); // make sure not to show this Activity's blank white screen
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent resultIntent) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_PICK_CONTACTS) {

                String username = resultIntent.getStringExtra(ContactsPickerActivity.EXTRA_RESULT_USERNAME);

                if (username != null) {
                    long providerId = resultIntent.getLongExtra(ContactsPickerActivity.EXTRA_RESULT_PROVIDER, -1);
                    long accountId = resultIntent.getLongExtra(ContactsPickerActivity.EXTRA_RESULT_ACCOUNT, -1);

                    sendMedia(username, providerId, accountId, null);

                    startChat(providerId, accountId, username, true);

                }
                else {

                    //send to multiple
                    ArrayList<String> usernames = resultIntent.getStringArrayListExtra(ContactsPickerActivity.EXTRA_RESULT_USERNAMES);
                    if (usernames != null)
                    {
                        ArrayList<Integer> providers = resultIntent.getIntegerArrayListExtra(ContactsPickerActivity.EXTRA_RESULT_PROVIDER);
                        ArrayList<Integer> accounts = resultIntent.getIntegerArrayListExtra(ContactsPickerActivity.EXTRA_RESULT_ACCOUNT);

                        if (providers != null && accounts != null)
                            for (int i = 0; i < providers.size(); i++)
                            {
                                sendMedia(usernames.get(i), providers.get(i), accounts.get(i), null);
                            }


                        if (usernames.size() > 1)
                            startActivity(new Intent(this,MainActivity.class));
                        else
                        {
                            startChat(providers.get(0), accounts.get(0), usernames.get(0), true);

                        }

                    }

                    finish();
                }


            }
            else if (requestCode == REQUEST_SIGNIN_ACCOUNT || requestCode == REQUEST_CREATE_ACCOUNT)
            {

                mHandlerRouter.postDelayed(new Runnable()
                {
                    @Override
                    public void run ()
                    {
                        doOnCreate();
                    }
                }, 500);

            }

        } else {
            finish();
        }
    }

    public void startChat (long providerId, long accountId, String username, final boolean openChat)
    {

       Intent intent = new Intent(ImUrlActivity.this, RoomActivity.class);
        intent.putExtra("address", username);
        startActivity(intent);
    }

    private void sendMedia(String username, long providerId, long accountId, String replyId) {
        sendMediaAsync(username, providerId, accountId, replyId);
    }

    private void sendMediaAsync(String username, long providerId, long accountId, String replyId) {

        try {


            IChatSession session = getChatSession(username);

            if (mSendText != null)
                session.sendMessage(mSendText, false, false, true, replyId);
            else if (mSendUri != null) {

                try {


                    String offerId = UUID.randomUUID().toString();

                    if (SecureMediaStore.isVfsUri(mSendUri)) {


                        boolean sent = session.offerData(offerId, null, mSendUri.toString(), mSendType);

                        if (sent)
                            return;
                    } else {
                        String fileName = mSendUri.getLastPathSegment();
                        FileInfo importInfo = SystemServices.getFileInfoFromURI(this, mSendUri);

                        if (!TextUtils.isEmpty(importInfo.type)) {
                            if (importInfo.type.startsWith("image"))
                                mSendUri = SecureMediaStore.resizeAndImportImage(this, session.getId() + "", mSendUri, importInfo.type);
                            else {
                                Uri importedMediaUri = SecureMediaStore.importContent(this, session.getId() + "", fileName, getContentResolver().openInputStream(mSendUri));
                                generateVideoThumbnail(mSendUri,importedMediaUri);
                                mSendUri = importedMediaUri;
                            }

                            boolean sent = session.offerData(offerId, null, mSendUri.toString(), importInfo.type);
                            if (sent)
                                return;
                        }
                    }


                } catch (Exception e) {

                    Log.e(TAG, "error sending external file", e);
                }

//                Toast.makeText(this, R.string.unable_to_securely_share_this_file, Toast.LENGTH_LONG).show();

            }
        } catch (RemoteException e) {
            Log.e(TAG, "Error sending data", e);
        }

    }

    private void generateVideoThumbnail (Uri contentUri, Uri sendUri) throws FileNotFoundException {
        Bitmap bitmap = null;
        String videoPath = new SystemServices().getUriRealPath(this, contentUri);
        if (!TextUtils.isEmpty(videoPath)) {
            bitmap = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Images.Thumbnails.MINI_KIND);

            if (bitmap != null){
                String thumbPath = sendUri.getPath() + ".thumb.jpg";
                java.io.File fileThumb = new java.io.File(thumbPath);
                bitmap.compress(Bitmap.CompressFormat.JPEG,100,new java.io.FileOutputStream(fileThumb));
                return;
            }
        }

        if (bitmap == null)
        {

            long videoId = -1;
            String lastPath = contentUri.getLastPathSegment();
            try {
                videoId = Long.parseLong(lastPath);
            }
            catch (Exception e)
            {
                String[] parts = lastPath.split(":");
                if (parts.length > 1)
                    videoId = Long.parseLong(parts[1]);
            }

            if (videoId != -1)
            {
                bitmap = MediaStore.Video.Thumbnails.getThumbnail(
                        getContentResolver(), videoId,
                        MediaStore.Video.Thumbnails.MINI_KIND,
                        (BitmapFactory.Options) null);

                if (bitmap != null) {
                    String thumbPath = sendUri.getPath() + ".thumb.jpg";
                    java.io.File fileThumb = new java.io.File(thumbPath);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, new java.io.FileOutputStream(fileThumb));
                    return;
                }
            }
        }

        MediaMetadataRetriever mMMR = new MediaMetadataRetriever();
        mMMR.setDataSource(this, contentUri);
        bitmap = mMMR.getFrameAtTime();
        if (bitmap != null) {
            String thumbPath = sendUri.getPath() + ".thumb.jpg";
            java.io.File fileThumb = new java.io.File(thumbPath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, new java.io.FileOutputStream(fileThumb));
            return;
        }
    }

    private IChatSession getChatSession(String username) {
        if (mChatSessionManager != null) {
            try {
                IChatSession session = mChatSessionManager.getChatSession(username);

                if (session == null)
                    session = mChatSessionManager.createChatSession(username, false, new IChatSessionListener() {
                        @Override
                        public void onChatSessionCreated(IChatSession session) throws RemoteException {

                        }

                        @Override
                        public void onChatSessionCreateError(String name, ImErrorInfo error) throws RemoteException {

                        }

                        @Override
                        public IBinder asBinder() {
                            return null;
                        }
                    });

                return session;
            } catch (RemoteException e) {
                LogCleaner.error(LOG_TAG, "send message error",e);
            }
        }
        return null;
    }

    private void startContactPicker() {

        Intent i = new Intent(this, ContactsPickerActivity.class);
        i.putExtra(ContactsPickerActivity.EXTRA_SHOW_GROUPS,true);
        i.putExtra(ContactsPickerActivity.EXTRA_SHOW_ADD_OPTIONS,false);
        startActivityForResult(i, REQUEST_PICK_CONTACTS);

        /**
        Uri.Builder builder = Imps.Contacts.CONTENT_URI.buildUpon();

        Collection<IImConnection> listConns = ((ImApp)getApplication()).getActiveConnections();

        for (IImConnection conn : listConns)
        {
            try {
                mChatSessionManager = conn.getChatSessionManager();
                long mProviderId = conn.getProviderId();
                long mAccountId = conn.getAccountId();

                ContentUris.appendId(builder,  mProviderId);
                ContentUris.appendId(builder,  mAccountId);
                Uri data = builder.build();

                i.setData(data);
                ArrayList<String> extras = new ArrayList<>();
                extras.add("");
                i.putExtra(EXTRA_EXCLUDED_CONTACTS,extras);
                i.putExtra(ContactsPickerActivity.EXTRA_SHOW_GROUPS,true);

                break;

            } catch (RemoteException e) {
                throw new RuntimeException(e);
            }
        }**/
    }

    void showLockScreen() {
        Intent intent = new Intent(this, LockScreenActivity.class);
      //  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(RouterActivity.EXTRA_ORIGINAL_INTENT, getIntent());
        startActivity(intent);
        finish();

    }

    private void doOnCreate ()
    {
        Intent intent = getIntent();

        Bundle extras = intent.getExtras();
        for (String key : extras.keySet())
        {
            Log.d("ImUrl","bundle extra: " + key + "=" + extras.get(key));
        }



        if (Intent.ACTION_INSERT.equals(intent.getAction())) {
            if (!resolveInsertIntent(intent)) {
                finish();
                return;
            }
        } else if (Intent.ACTION_SEND.equals(intent.getAction())) {

            Uri streamUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
            String mimeType = intent.getType();
            String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);

            if (streamUri != null)
                openOtrInBand(streamUri, mimeType);
            else if (intent.getData() != null)
                openOtrInBand(intent.getData(), mimeType);
            else if (sharedText != null)
            {
                //do nothing for now :(
                mSendText = sharedText;

                startContactPicker();

            }
            else
                finish();

        } else if (Intent.ACTION_SENDTO.equals(intent.getAction())|| Intent.ACTION_VIEW.equals(intent.getAction())) {
            if (!resolveIntent(intent)) {
                finish();
                return;
            }

            if (TextUtils.isEmpty(mToAddress)) {
                LogCleaner.warn(LOG_TAG, "<ImUrlActivity>Invalid to address");
                //  finish();
                return;
            }
        }


    }


}
