package info.guardianproject.keanuapp

import android.app.SearchManager
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.github.javiersantos.appupdater.AppUpdater
import com.github.javiersantos.appupdater.enums.Display
import com.github.javiersantos.appupdater.enums.UpdateFrom
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.google.android.material.tabs.TabLayout.TabLayoutOnPageChangeListener
import info.guardianproject.keanu.core.KeanuConstants
import info.guardianproject.keanu.core.Preferences
import info.guardianproject.keanu.core.ui.MoreFragment
import info.guardianproject.keanu.core.ui.chatlist.ChatListFragment
import info.guardianproject.keanu.core.util.SecureMediaStore
import info.guardianproject.keanu.core.util.SystemServices
import info.guardianproject.keanuapp.databinding.AwesomeActivityMainBinding
import info.guardianproject.keanuapp.ui.BaseActivity
import info.guardianproject.keanuapp.ui.LockScreenActivity
import info.guardianproject.keanuapp.ui.accounts.AccountFragment
import info.guardianproject.keanuapp.ui.camera.CameraActivity
import info.guardianproject.keanuapp.ui.contacts.AddContactActivity
import info.guardianproject.keanuapp.ui.contacts.ContactsListFragment
import info.guardianproject.keanuapp.ui.contacts.ContactsPickerActivity
import info.guardianproject.keanu.core.ui.room.RoomActivity
import info.guardianproject.keanu.core.ui.room.StoryActivity
import info.guardianproject.keanuapp.ui.legacy.SettingActivity
import info.guardianproject.keanuapp.ui.onboarding.OnboardingActivity
import org.matrix.android.sdk.api.MatrixCallback
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.room.model.RoomHistoryVisibility
import org.matrix.android.sdk.api.session.room.model.create.CreateRoomParams
import timber.log.Timber
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.lang.ref.WeakReference
import java.util.*

class MainActivity : BaseActivity() {

    private var mLastPhoto: Uri? = null

    private val mApp: ImApp?
        get() = application as? ImApp

    private val mSession: Session?
        get() = mApp?.matrixSession

    private val mPrefs: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(this)

    private var mSbStatus: Snackbar? = null

    private lateinit var mBinding: AwesomeActivityMainBinding

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Preferences.doBlockScreenshots()) {
            window.setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                    WindowManager.LayoutParams.FLAG_SECURE)
        }

        mBinding = AwesomeActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mBinding.bannerClose.setOnClickListener { mBinding.bannerLayout.visibility = View.GONE }

        setSupportActionBar(mBinding.toolbar)

        val tabChangeListener = TabLayoutOnPageChangeListener(mBinding.tabs)

        mBinding.viewpager.adapter = FragmentAdapter(this)
        mBinding.viewpager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageScrollStateChanged(state: Int) {
                tabChangeListener.onPageScrollStateChanged(state)
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                tabChangeListener.onPageScrolled(position, positionOffset, positionOffsetPixels)
            }

            override fun onPageSelected(position: Int) {
                tabChangeListener.onPageSelected(position)
            }
        })

        var tab = mBinding.tabs.newTab()
        tab.setIcon(R.drawable.ic_discuss)
        tab.tag = getString(R.string.chats)
        tab.setContentDescription(R.string.chats)
        mBinding.tabs.addTab(tab)

        tab = mBinding.tabs.newTab()
        tab.setIcon(R.drawable.ic_people_white_36dp)
        tab.tag = getString(R.string.contacts)
        tab.setContentDescription(R.string.contacts)
        mBinding.tabs.addTab(tab)

        tab = mBinding.tabs.newTab()
        tab.setIcon(R.drawable.ic_explore_white_24dp)
        tab.tag = getString(R.string.title_more)
        tab.setContentDescription(R.string.title_more)
        mBinding.tabs.addTab(tab)

        tab = mBinding.tabs.newTab()
        tab.setIcon(R.drawable.ic_face_white_24dp)
        tab.tag = getString(R.string.title_me)
        tab.setContentDescription(R.string.title_me)
        mBinding.tabs.addTab(tab)

        mBinding.tabs.addOnTabSelectedListener(object : OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab) {
                mBinding.viewpager.currentItem = tab.position
                setToolbarTitle(tab.position)
                applyStyle()
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}

            override fun onTabReselected(tab: TabLayout.Tab) {
                setToolbarTitle(tab.position)
                applyStyle()
            }
        })

        mBinding.fab.setOnClickListener {
            when (mBinding.viewpager.currentItem) {
                0 -> {
                    if (getFragment<ContactsListFragment>()?.contactCount ?: 0 > 0) {
                        mChooseContact.launch(Intent(this@MainActivity, ContactsPickerActivity::class.java))
                    } else {
                        inviteContact()
                    }
                }
                1 -> inviteContact()
                2 -> mTakePicture.launch(Intent(this, CameraActivity::class.java))

            }
        }

        setToolbarTitle(0)

        applyStyle()

        if (mSession == null) showOnboarding()
    }

    private fun showOnboarding() {

        val intent = Intent(this, OnboardingActivity::class.java)
//        intent.putExtra(RouterActivity.EXTRA_ORIGINAL_INTENT, intent)
  //      intent.putExtra(RouterActivity.EXTRA_DO_SIGNIN, true)

        startActivity(intent)

        finish()
    }

    private fun setToolbarTitle(tabPosition: Int) {
        val sb = StringBuilder()
        sb.append(getString(R.string.app_name))
        sb.append(" | ")

        when (tabPosition) {
            0 -> if (getFragment<ChatListFragment>()?.showArchived == true) sb.append(getString(R.string.action_archive)) else sb.append(getString(R.string.chats))

            1 -> if ((getFragment<ContactsListFragment>()?.currentType ?: 0) and KeanuConstants.CONTACT_TYPE_FLAG_HIDDEN != 0) sb.append(getString(R.string.action_archive)) else sb.append(getString(R.string.friends))

            2 -> sb.append(getString(R.string.title_more))

            3 -> {
                sb.append(getString(R.string.me_title))
                getFragment<AccountFragment>()?.userVisibleHint = true
            }
        }

        mBinding.toolbar.title = sb.toString()

        when (tabPosition) {
            1 -> {
                mBinding.fab.setImageResource(R.drawable.ic_person_add_white_36dp)
            }
            2 -> {
                //    mBinding.fab.setImageResource(R.drawable.ic_photo_camera_white_36dp);
                //   mBinding.fab.setVisibility(View.GONE);
            }
            3 -> {
                // mBinding.fab.setVisibility(View.GONE);
            }
            else -> {
                mBinding.fab.setImageResource(R.drawable.ic_add_white_24dp)
            }
        }
    }

    fun inviteContact() {
        mAddContact.launch(Intent(this@MainActivity, AddContactActivity::class.java))
    }

    public override fun onResume() {
        super.onResume()

        applyStyle()

        handleIntent(intent)

        checkForUpdates()

        getFragment<ChatListFragment>()?.refresh()
    }

    private fun checkConnection(): Boolean {
        return try {
            if (mSbStatus?.isShown == true) mSbStatus?.dismiss()

            if (!isNetworkAvailable) {
                mSbStatus = Snackbar.make(mBinding.viewpager, R.string.status_no_internet, Snackbar.LENGTH_INDEFINITE)
                mSbStatus?.show()

                Handler(Looper.getMainLooper()).postDelayed({ checkConnection() }, 5000) //Timer is in ms here.

                return false
            }

            true
        } catch (e: Exception) {
            false
        }
    }

    @Suppress("DEPRECATION")
    private val isNetworkAvailable: Boolean
        get() {
            val cm = getSystemService(CONNECTIVITY_SERVICE) as? ConnectivityManager

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val capabilities = cm?.getNetworkCapabilities(cm.activeNetwork)
                        ?: return false

                return capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                        && capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED)
            }
            else {
                return cm?.activeNetworkInfo?.isConnected == true
            }
        }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)

        setIntent(intent)

        handleIntent(intent)
    }

    private fun handleIntent(intent: Intent?) {
        if (intent?.hasExtra("username") == true) {
            //launch a new chat based on the intent value
            intent.getStringExtra("username")?.let { startChat(it) }
        }
        else if (intent?.hasExtra("group") == true) {
            intent.getStringExtra("group")?.let { joinGroupChat(it) }
        }
        else if (intent?.hasExtra(ContactsPickerActivity.EXTRA_RESULT_USERNAME) == true) {
            intent.getStringExtra(ContactsPickerActivity.EXTRA_RESULT_USERNAME)?.let { startChat(it) }
        }
        else if (intent?.hasExtra("address") == true) {
            if (intent.getBooleanExtra("new", false)) {
                intent.getStringExtra("address")?.let { startChat(it) }
            } else {
                intent.getStringExtra("address")?.let { showChat(it) }
            }
        }
        else if (intent?.getBooleanExtra("firstTime", false) == true) {
            inviteContact()
        }
        else if (intent?.hasExtra(EXTRA_PRESELECT_TAB) == true) {
            mBinding.tabs.selectTab(mBinding.tabs.getTabAt(intent.getIntExtra(EXTRA_PRESELECT_TAB, 0)))
        }

        setIntent(null)
    }

    private val mChangeSettings = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == RESULT_OK) {
            finish()
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    private val mAddContact = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == RESULT_OK) {
            result.data?.getStringExtra(ContactsPickerActivity.EXTRA_RESULT_USERNAME)?.let {
                startChat(it)
            }
        }
    }

    private val mChooseContact = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == RESULT_OK) {
            val username = result.data?.getStringExtra(ContactsPickerActivity.EXTRA_RESULT_USERNAME)
            if (username != null) {
                startChat(username)
            }
            else {
                result.data?.getStringArrayListExtra(ContactsPickerActivity.EXTRA_RESULT_USERNAMES)?.let {
                    startGroupChat(it)
                }
            }
        }
    }

    private val mTakePicture = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == RESULT_OK) {
            try {
                if (mLastPhoto != null) importPhoto()
            } catch (e: Exception) {
                Timber.w(e, "error importing photo")
            }
        }
    }

    public override fun onSaveInstanceState(savedInstanceState: Bundle) {
        super.onSaveInstanceState(savedInstanceState)

        mLastPhoto?.let {
            savedInstanceState.putString("lastphoto", it.toString())
        }
    }

    public override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        savedInstanceState.getString("lastphoto")?.let {
            mLastPhoto = Uri.parse(it)
        }
    }

    @Throws(FileNotFoundException::class, UnsupportedEncodingException::class)
    private fun importPhoto() {

        // import
        val info = SystemServices.getFileInfoFromURI(this, mLastPhoto)
        val sessionId = "self"

        try {
            SecureMediaStore.resizeAndImportImage(this, sessionId, mLastPhoto, info.type)
            delete(mLastPhoto)

            //adds in an empty message, so it can exist in the gallery and be forwarded
            //TODO
            mLastPhoto = null
        }
        catch (ioe: IOException) {
            Timber.e(ioe, "error importing photo")
        }
    }

    private fun delete(uri: Uri?): Boolean {
        return when (uri?.scheme) {
            "content" -> {
                val deleted = contentResolver.delete(uri, null, null)
                deleted == 1
            }

            "file" -> {
                val file = File(uri.toString().substring(5))
                file.delete()
            }

            else -> false
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        val searchManager = getSystemService(SEARCH_SERVICE) as? SearchManager
        val searchView = menu.findItem(R.id.menu_search).actionView as? SearchView

        searchView?.setSearchableInfo(searchManager?.getSearchableInfo(componentName))
        searchView?.setIconifiedByDefault(false)

        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(query: String): Boolean {
                if (mBinding.tabs.selectedTabPosition == 0) {
                    getFragment<ChatListFragment>()?.doSearch(query)
                }
                else if (mBinding.tabs.selectedTabPosition == 1) {
                    getFragment<ContactsListFragment>()?.doSearch(query)
                }

                return true
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                if (mBinding.tabs.selectedTabPosition == 0) {
                    getFragment<ChatListFragment>()?.doSearch(query)
                }
                else if (mBinding.tabs.selectedTabPosition == 1) {
                    getFragment<ContactsListFragment>()?.doSearch(query)
                }

                return true
            }
        })

        searchView?.setOnCloseListener {
            getFragment<ChatListFragment>()?.doSearch(null)
            false
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> return true

            R.id.menu_settings -> {
                mChangeSettings.launch(Intent(this, SettingActivity::class.java))
                return true
            }

            R.id.menu_list_normal -> {
                clearFilters()
                Timber.v("Filter: Normal")
                return true
            }

            R.id.menu_list_archive -> {
                Timber.v("Filter: Archive")
                enableArchiveFilter()
                return true
            }

            R.id.menu_lock -> {
                handleLock()
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun clearFilters() {
        if (mBinding.tabs.selectedTabPosition == 0) {
            getFragment<ChatListFragment>()?.showArchived = false
            Timber.v("Filter: clear_1")
        }
        else {
            Timber.v("Filter: clear_2")
            getFragment<ContactsListFragment>()?.setArchiveFilter(false)
        }

        setToolbarTitle(mBinding.tabs.selectedTabPosition)
    }

    private fun enableArchiveFilter() {
        if (mBinding.tabs.selectedTabPosition == 0) {
            getFragment<ChatListFragment>()?.showArchived = true
            Timber.v("Filter: clear_11")
        }
        else {
            Timber.v("Filter: clear_22")
            getFragment<ContactsListFragment>()?.setArchiveFilter(true)
        }

        setToolbarTitle(mBinding.tabs.selectedTabPosition)
    }

    private fun handleLock() {
        if (mPrefs.contains(KeanuConstants.PREFERENCE_KEY_TEMP_PASS)) {
            // Need to setup new user passphrase.
            val intent = Intent(this, LockScreenActivity::class.java)
            intent.action = LockScreenActivity.ACTION_CHANGE_PASSPHRASE
            startActivity(intent)
        }
        else {
            // Time to do the lock.
            val intent = Intent(this, RouterActivity::class.java)
            intent.action = RouterActivity.ACTION_LOCK_APP
            startActivity(intent)
            finish()
        }
    }

    private fun startChat(username: String) {
        if (!checkConnection()) return

        startGroupChat(arrayListOf(username))
    }

    @JvmOverloads
    fun startGroupChat(invitees: List<String> = emptyList(), roomSubject: String? = null, isSession: Boolean = false) {

        mSbStatus = Snackbar.make(mBinding.viewpager, R.string.connecting_to_group_chat_, Snackbar.LENGTH_INDEFINITE)
        mSbStatus?.show()

        if (invitees.size == 1) {
            mSession?.createDirectRoom(invitees.first(), object : MatrixCallback<String> {

                override fun onSuccess(data: String) {
                    mSbStatus?.dismiss()

                    val intent = Intent(this@MainActivity, RoomActivity::class.java)
                    intent.putExtra("address", data)
                    intent.putExtra("firsttime", true)
                    intent.putExtra("isNew", true)
                    intent.putExtra("subject", roomSubject)
                    intent.putExtra("nickname", roomSubject)

                    startActivity(intent)
                }

                override fun onFailure(failure: Throwable) {
                    Timber.d(failure)
                }
            })
        }
        else {
            val params = CreateRoomParams()
            params.historyVisibility = RoomHistoryVisibility.INVITED
            params.enableEncryptionIfInvitedUsersSupportIt = true

            mSession?.createRoom(params, object : MatrixCallback<String> {

                override fun onSuccess(data: String) {
                    mSbStatus?.dismiss()

                    val room = mSession?.getRoom(data)

                    for (invitee in invitees) room?.invite(invitee, "", object : MatrixCallback<Unit> {

                        override fun onSuccess(data: Unit) {
                            Timber.d("\"$invitee\" successfully invited.")
                        }

                        override fun onFailure(failure: Throwable) {
                            Timber.d(failure)
                        }
                    })

                    val intent = Intent(this@MainActivity, if (isSession) StoryActivity::class.java else RoomActivity::class.java)
                    intent.putExtra("address", data)
                    intent.putExtra("firsttime", true)
                    intent.putExtra("isNew", invitees.isEmpty())
                    intent.putExtra("subject", roomSubject)
                    intent.putExtra("nickname", roomSubject)
                    if (isSession) intent.putExtra(StoryActivity.ARG_CONTRIBUTOR_MODE, true)

                    startActivity(intent)
                }

                override fun onFailure(failure: Throwable) {
                    Timber.d(failure)
                }
            })
        }
    }

    private fun joinGroupChat(roomAddress: String) {
        mSession?.joinRoom(roomAddress, "", emptyList(), object : MatrixCallback<Unit> {

            override fun onSuccess(data: Unit) {
                Timber.d("\"$roomAddress\" successfully joined.")
            }

            override fun onFailure(failure: Throwable) {
                Timber.d(failure)
            }
        })
    }

    private fun showChat(address: String) {
        val intent = Intent(this, RoomActivity::class.java)
        intent.putExtra("address", address)

        startActivity(intent)
    }

    private fun checkForUpdates() {
        // Remove this for store builds!

        // Only check github for updates if there is no Google Play.
        try {
            val version = packageManager.getPackageInfo(packageName, 0).versionName

            //if this is a full release, without -beta -rc etc, then check the appupdater!
            if (!version.contains("-")) {
                val timeNow = Date().time
                val timeSinceLastCheck = mPrefs.getLong("updatetime", -1)

                //only check for updates once per day
                if (timeSinceLastCheck == -1L || timeNow - timeSinceLastCheck > 86400) {
                    val appUpdater = AppUpdater(this)
                    appUpdater.setDisplay(Display.SNACKBAR)

                    if (hasGooglePlay()) {
                        appUpdater.setUpdateFrom(UpdateFrom.GOOGLE_PLAY)
                    }
                    else {
                        appUpdater.setUpdateFrom(UpdateFrom.XML)
                        appUpdater.setUpdateXML(ImApp.URL_UPDATER)
                    }

                    appUpdater.start()

                    mPrefs.edit().putLong("updatetime", timeNow).apply()
                }
            }
        }
        catch (e: Exception) {
            Timber.d(e, "error checking app updates")
        }
    }

    private fun hasGooglePlay(): Boolean {
        try {
            application.packageManager.getPackageInfo("com.android.vending", 0)
        }
        catch (e: PackageManager.NameNotFoundException) {
            return false
        }

        return true
    }

    fun applyStyle() {
        // Not set color.
        val themeColorHeader = mPrefs.getInt("themeColor", -1)
        var themeColorText = mPrefs.getInt("themeColorText", -1)
        (mBinding.viewpager.adapter as? FragmentAdapter)?.backgroundColor = mPrefs.getInt("themeColorBg", -1)

        if (themeColorHeader != -1) {
            if (themeColorText == -1) themeColorText = getContrastColor(themeColorHeader)

            if (Build.VERSION.SDK_INT >= 21) {
                window.navigationBarColor = themeColorHeader
                window.statusBarColor = themeColorHeader

                @Suppress("DEPRECATION")
                window.setTitleColor(getContrastColor(themeColorHeader))
            }

            mBinding.toolbar.setBackgroundColor(themeColorHeader)
            mBinding.toolbar.setTitleTextColor(getContrastColor(themeColorHeader))
            mBinding.tabs.setBackgroundColor(themeColorHeader)
            mBinding.tabs.setTabTextColors(themeColorText, themeColorText)
            mBinding.fab.setBackgroundColor(themeColorHeader)
        }
    }

    private inline fun <reified F: Fragment> getFragment(): F? {
        return (mBinding.viewpager.adapter as? FragmentAdapter)?.fragments?.firstOrNull { it?.get() is F }?.get() as? F
    }

    internal class FragmentAdapter(activity: MainActivity) : FragmentStateAdapter(activity) {

        private var mFragmentClasses = listOf(ChatListFragment::class.java,
                ContactsListFragment::class.java,
                MoreFragment::class.java,
                AccountFragment::class.java)

        var fragments: ArrayList<WeakReference<Fragment>?> = ArrayList(mFragmentClasses.map { null })

        var backgroundColor = -1

        override fun getItemCount(): Int {
            return mFragmentClasses.size
        }

        override fun createFragment(position: Int): Fragment {
            var fragment = fragments[position]?.get()

            if (fragment == null) {
                fragment = mFragmentClasses[position].newInstance()
                fragments[position] = WeakReference(fragment)
            }

            if (backgroundColor > -1) fragment?.requireView()?.setBackgroundColor(backgroundColor)

            return fragment!!
        }

    }

    companion object {
        const val EXTRA_PRESELECT_TAB = "preselect_tab"

        fun getContrastColor(colorIn: Int): Int {
            val y = (299 * Color.red(colorIn) + 587 * Color.green(colorIn) + 114 * Color.blue(colorIn)) / 1000.0
            return if (y >= 128) Color.BLACK else Color.WHITE
        }
    }
}