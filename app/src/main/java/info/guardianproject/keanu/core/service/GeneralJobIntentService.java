package info.guardianproject.keanu.core.service;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.core.content.ContextCompat;
import android.util.Log;

import info.guardianproject.keanu.core.util.Debug;

import static info.guardianproject.keanu.core.KeanuConstants.LOG_TAG;
import static info.guardianproject.keanu.core.KeanuConstants.PREFERENCE_KEY_TEMP_PASS;

public class GeneralJobIntentService extends JobIntentService {

    public static final int JOB_ID = 0x01;

    private boolean isBooted = false;

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, GeneralJobIntentService.class, JOB_ID, work);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {

        Context context = getApplicationContext();

        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction()))
            handleBoot(context,intent);
    }

    private void handleBoot (Context context, Intent intent)
    {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        boolean prefStartOnBoot = prefs.getBoolean("pref_start_on_boot", true);

        Debug.onServiceStart();
        if (prefStartOnBoot) {

            if (!isBooted) {

                Intent serviceIntent = new Intent(context, RemoteImService.class);
                //   serviceIntent.setComponent(ImServiceConstants.IM_SERVICE_COMPONENT);
                serviceIntent.putExtra(ImServiceConstants.EXTRA_CHECK_AUTO_LOGIN, true);
                ContextCompat.startForegroundService(context,serviceIntent);
                
                isBooted = true;
            }
        }
    }
}
