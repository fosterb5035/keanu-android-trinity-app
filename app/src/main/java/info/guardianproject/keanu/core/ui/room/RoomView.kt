package info.guardianproject.keanu.core.ui.room

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Message
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.*
import android.view.View.OnFocusChangeListener
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.inputmethod.InputContentInfoCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.tougee.recorderview.AudioRecordView
import com.vanniktech.emoji.EmojiEditText
import com.vanniktech.emoji.EmojiPopup
import com.vanniktech.emoji.SingleEmojiTrait
import info.guardianproject.keanu.core.ui.RoundedAvatarDrawable
import info.guardianproject.keanu.core.util.SecureMediaStore
import info.guardianproject.keanuapp.ImApp
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.ui.contacts.GroupDisplayActivity
import info.guardianproject.keanuapp.ui.conversation.MessageListItem
import info.guardianproject.keanuapp.ui.legacy.SimpleAlertHandler
import info.guardianproject.keanuapp.ui.stickers.StickerManager
import info.guardianproject.keanuapp.ui.stickers.StickerPagerAdapter
import info.guardianproject.keanuapp.ui.widgets.ShareRequest
import org.matrix.android.sdk.api.MatrixCallback
import org.matrix.android.sdk.api.session.room.Room
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.message.MessageType
import timber.log.Timber
import java.io.IOException
import java.util.*
import kotlin.math.hypot

open class RoomView(protected var mActivity: RoomActivity, protected var mRoomId: String) {

    companion object {
        private const val VIEW_TYPE_CHAT = 1
        private const val SHOW_DATA_ERROR = 9997
        private const val SHOW_TYPING = 9996

        private class ChatViewHandler(activity: Activity?) : SimpleAlertHandler(activity) {

            override fun handleMessage(msg: Message) {
                when (msg.what) {
                    ImApp.EVENT_CONNECTION_DISCONNECTED -> {
                        Timber.d("Handle event connection disconnected.")
                        promptDisconnectedEvent(msg)
                        return
                    }
                    SHOW_DATA_ERROR -> {
                        msg.data.getString("file")
                        val error = msg.data.getString("err")
                        Toast.makeText(mActivity, "Error transferring file: $error", Toast.LENGTH_LONG).show()
                    }
                    SHOW_TYPING -> {
                        val isTyping = msg.data.getBoolean("typing")

                        (mActivity as? RoomActivity)?.typingView?.visibility = if (isTyping) View.VISIBLE else View.GONE
                    }
                }

                super.handleMessage(msg)
            }
        }
    }

    private val mApp
        get() = mActivity.application as? ImApp

    private val mRoom: Room?

    private val mHandler: SimpleAlertHandler

    private var mShareDraft: ShareRequest? = null

    private val mButtonTalk: TextView? = null

    private var mViewDeleteVoice: View? = null

    private var mIsSelected = false

    private var mMessageAdapter: MessageListAdapter? = null

    private var mRemoteNickname: String

    var icon: RoundedAvatarDrawable? = null

    var accountId: Long = -1

    val remotePresence = 0

    var type = 0
        private set

    private var mRequeryCallback: RequeryCallback? = null

    private val mUpdateChatCallback = Runnable { updateChat() }

    private var mIsListening = false

    private var mStickerPager: ViewPager? = null

    private val mStickerBox: RelativeLayout
        get() = mActivity.stickerBox

    private var emojiPopup: EmojiPopup? = null

    private inner class RequeryCallback : Runnable {
        override fun run() {
            Timber.d("RequeryCallback")

            requeryCursor()
        }
    }

    init {
        mHandler = ChatViewHandler(mActivity)
        mRoom = mApp?.matrixSession?.getRoom(mRoomId)
        mRemoteNickname = mRoom?.roomSummary()?.displayName ?: ""
        bindQuery()
        initViews()
        setViewType(VIEW_TYPE_CHAT)
    }


    fun setSelected(isSelected: Boolean) {
        mIsSelected = isSelected

        if (mIsSelected) {
            startListening()

            mActivity.composeMessage.requestFocus()

            userActionDetected()
            updateGroupTitle()
            try {
                mApp?.dismissChatNotification(-1, mRoomId)
            }
            catch (e: Exception) {
                Timber.d(e)
            }

            if (!hasJoined()) showJoinGroupUI()
        }
        else {
            stopListening()
            sendTypingStatus(false)
        }
    }

    private fun registerForConnEvents() {
        mApp?.registerForConnEvents(mHandler)
    }

    private fun unregisterForConnEvents() {
        mApp?.unregisterForConnEvents(mHandler)
    }

    @SuppressLint("ClickableViewAccessibility")
    protected fun initViews(): Boolean {
        val llm = LinearLayoutManager(mActivity.historyView.context)
        llm.stackFromEnd = true
        mActivity.historyView.layoutManager = llm
        mActivity.historyView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                val visibleItemCount = llm.childCount
                val totalItemCount = llm.itemCount
                val pastVisibleItems = llm.findFirstVisibleItemPosition()

                when {
                    pastVisibleItems == 0 -> {
                        //top of list
                        mMessageAdapter?.pageBack()
                    }
                    pastVisibleItems + visibleItemCount >= totalItemCount -> {
                        //End of list
                        mMessageAdapter?.onScrollStateChanged(null, RecyclerView.SCROLL_STATE_IDLE)
                    }
                    else -> {
                        mMessageAdapter?.onScrollStateChanged(null, RecyclerView.SCROLL_STATE_DRAGGING)
                    }
                }
            }
        })

        mActivity.composeMessage.setReachContentClickListner { inputContentInfoCompat: InputContentInfoCompat ->
            val request = ShareRequest()
            request.deleteFile = false
            request.resizeImage = false
            request.importContent = true
            request.media = inputContentInfoCompat.contentUri
            request.mimeType = "image/gif"

            mActivity.sendShareRequest(request)
        }

        mViewDeleteVoice = mActivity.findViewById(R.id.viewDeleteVoice)

        mActivity.btnDeleteVoice.setOnTouchListener { _, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_MOVE) {
                mActivity.btnDeleteVoice.setBackgroundColor(ResourcesCompat.getColor(
                        mActivity.btnDeleteVoice.resources, android.R.color.holo_red_light, null))
            }

            false
        }

        mActivity.btnAttach.setOnClickListener { toggleAttachMenu() }

        mActivity.mediaPreviewCancel.setOnClickListener { clearMediaDraft() }

        mActivity.btnAttachPicture.setOnClickListener {
            mActivity.attachView.visibility = View.INVISIBLE
            mActivity.startImagePicker()
        }

        mActivity.btnTakePicture.setOnClickListener {
            mActivity.attachView.visibility = View.INVISIBLE
            mActivity.startPhotoTaker()
        }

        mActivity.btnAttachAudio.setOnClickListener {
            mActivity.attachView.visibility = View.INVISIBLE
            mActivity.startFilePicker("audio/*")
        }

        mActivity.btnAttachFile.setOnClickListener {
            mActivity.attachView.visibility = View.INVISIBLE
            mActivity.startFilePicker("*/*")
        }

        mActivity.btnAttachSticker.setOnClickListener { toggleStickers() }

        mActivity.btnCreateStory.setOnClickListener {
            mActivity.attachView.visibility = View.INVISIBLE
            mActivity.startStoryEditor()
        }

        mActivity.audioRecordView.activity = mActivity
        mActivity.audioRecordView.callback = object : AudioRecordView.Callback {

            override fun onRecordStart(audio: Boolean) {
                Timber.d("onRecordStart: $audio")
                mActivity.startAudioRecording()
            }

            override fun onRecordEnd() {
                Timber.d("onRecordEnd")
                if (mActivity.isAudioRecording) {
                    mActivity.stopAudioRecording(true)
                }
            }

            override fun isReady(): Boolean {
                return true
            }

            override fun onRecordCancel() {
                Timber.d("onRecordCancel")
                if (mActivity.isAudioRecording) {
                    mActivity.stopAudioRecording(false)
                }
            }
        }

        mActivity.composeMessage.setOnTouchListener { _, _ ->
            sendTypingStatus(mActivity.composeMessage.text?.isNotEmpty() ?: false)
            false
        }

        mActivity.composeMessage.onFocusChangeListener = OnFocusChangeListener { _, hasFocus ->
            sendTypingStatus(hasFocus && (mActivity.composeMessage.text?.isNotEmpty() ?: false))
        }

        mActivity.composeMessage.setOnKeyListener(object : View.OnKeyListener {

            override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_CENTER -> {
                            sendMessage()
                            return true
                        }
                        KeyEvent.KEYCODE_ENTER -> {
                            if (event.isAltPressed) {
                                mActivity.composeMessage.append("\n")
                                return true
                            }
                        }
                    }
                }

                return false
            }
        })

        mActivity.composeMessage.setOnEditorActionListener(object : OnEditorActionListener {
            override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent): Boolean {
                if (event.isAltPressed) {
                    return false
                }

                val imm = mActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
                if (imm?.isActive(v) == true) {
                    imm.hideSoftInputFromWindow(v.windowToken, 0)
                }

                sendMessage()

                return true
            }
        })

        // TODO: this is a hack to implement BUG #1611278, when dispatchKeyEvent() works with
        // the soft keyboard, we should remove this hack.
        mActivity.composeMessage.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence, start: Int, before: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, after: Int) {}

            override fun afterTextChanged(s: Editable) {
                userActionDetected()
            }
        })

        mActivity.btnSend.setOnClickListener { onSendButtonClicked() }

        mMessageAdapter = createRecyclerViewAdapter()

        mActivity.historyView.adapter = mMessageAdapter

        return true
    }

    protected open fun onSendButtonClicked() {
        if (mActivity.composeMessage.visibility == View.VISIBLE) {
            sendMessage()
        }
        else {
            mActivity.btnSend.setImageResource(R.drawable.ic_send_secure)
            mActivity.btnSend.visibility = View.GONE

            mButtonTalk?.visibility = View.GONE

            mActivity.composeMessage.visibility = View.VISIBLE

            mActivity.audioRecordView.visibility = View.VISIBLE
        }
    }

    protected open fun createRecyclerViewAdapter(): MessageListAdapter? {
        return MessageListAdapter(mActivity, this, mRoomId)
    }

    private fun sendTypingStatus(isTyping: Boolean) {
        if (isTyping) {
            mRoom?.userIsTyping()
        }
        else {
            mRoom?.userStopsTyping()
        }
    }

    fun sendMessageRead(msgId: String) {
        mRoom?.setReadReceipt(msgId, object : MatrixCallback<Unit> {

            override fun onSuccess(data: Unit) {}

            override fun onFailure(failure: Throwable) {
                Timber.d(failure)
            }
        })
    }

    private fun startListening() {
        mIsListening = true

        registerForConnEvents()
    }

    private fun stopListening() {
        cancelRequery()

        unregisterForConnEvents()

        mIsListening = false
    }

    private fun updateChat() {
        setViewType(VIEW_TYPE_CHAT)

        updateGroupTitle()
    }

    private fun hasJoined(): Boolean {
        val userId = mApp?.matrixSession?.myUserId ?: return false

        //TODO
        return mRoom?.getRoomMember(userId)?.membership?.name.equals(Membership.JOIN.name, ignoreCase = true)
    }

    private fun showJoinGroupUI() {
        val joinGroupView = mActivity.joinGroupView
        joinGroupView.visibility = View.VISIBLE

        val btnJoinAccept = mActivity.btnJoinAccept
        val btnJoinDecline = mActivity.btnJoinDecline

        val title = mActivity.roomJoinTitle
        title.text = title.context.getString(R.string.room_invited)

        btnJoinAccept.setOnClickListener {
            mRoom?.join("", emptyList(), object : MatrixCallback<Unit> {

                override fun onSuccess(data: Unit) {
                    initViews()
                }

                override fun onFailure(failure: Throwable) {
                    Timber.d(failure)

                    mActivity.finish()
                }
            })

            joinGroupView.visibility = View.GONE
        }

        btnJoinDecline.setOnClickListener {
            mRoom?.leave("", object : MatrixCallback<Unit> {

                override fun onSuccess(data: Unit) {}

                override fun onFailure(failure: Throwable) {
                    Timber.d(failure)
                }
            })

            joinGroupView.visibility = View.GONE

            mActivity.finish()
        }
    }

    val title: String
        get() = if (mRemoteNickname.isNotBlank()) mRemoteNickname else mRoomId

    private fun updateGroupTitle() {
        mRoom?.roomSummary()?.displayName?.let { mRemoteNickname = it }

        // Update title
        mActivity.supportActionBar?.title = mRemoteNickname
    }

    private fun bindQuery(): Boolean {
        mHandler.post(mUpdateChatCallback)
        return true
    }

    private fun setViewType(type: Int) {
        this.type = type

        if (type == VIEW_TYPE_CHAT) {
            setChatViewEnabled(true)
        }
    }

    private fun setChatViewEnabled(enabled: Boolean) {
        mActivity.composeMessage.isEnabled = enabled
        mActivity.btnSend.isEnabled = enabled

        if (!enabled) mActivity.historyView.adapter = null
    }

    fun jumpToBottom() {
        mHandler.post {
            val count = mMessageAdapter?.itemCount ?: 0

            if (count > 0) {
                mActivity.historyView.layoutManager?.scrollToPosition(count - 1)
            }
        }
    }

    fun jumpToPosition(scrollpos: Int) {
        mHandler.post {
            if (mMessageAdapter?.itemCount ?: 0 > 0) {
                mActivity.historyView.layoutManager?.scrollToPosition(scrollpos)
            }
        }
    }

    private fun cancelRequery() {
        if (mRequeryCallback != null) {
            Timber.d("cancelRequery")

            if (mRequeryCallback != null) {
                mHandler.removeCallbacks(mRequeryCallback!!)

                mRequeryCallback = null
            }
        }
    }

    fun requeryCursor() {
        mMessageAdapter?.notifyDataSetChanged()
    }

    fun closeChatSession() {
        mRoom?.leave("", object : MatrixCallback<Unit> {})
    }

    fun showGroupInfo() {
        val intent = Intent(mActivity, GroupDisplayActivity::class.java)
        intent.putExtra("address", mRoomId)
        mActivity.startActivity(intent)
    }

    protected open fun sendMessage() {
        if (mShareDraft != null) {
            Timber.v("sendMessage")

            mActivity.sendShareRequest(mShareDraft!!)
            mShareDraft = null

            mActivity.mediaPreviewContainer.visibility = View.GONE
        }

        val msg = mActivity.composeMessage.text.toString()
        val replyId = (mMessageAdapter?.lastSelectedView as? MessageListItem)?.packetId

        if (msg.isNotEmpty()) sendMessageAsync(msg, replyId)
    }

    @Throws(IOException::class)
    fun setMediaDraft(mediaDraft: ShareRequest) {
        mShareDraft = mediaDraft

        mActivity.mediaPreviewContainer.visibility = View.VISIBLE

        val bmpPreview = SecureMediaStore.getThumbnailFile(mActivity, mediaDraft.media, 1000)
        mActivity.mediaPreview.setImageBitmap(bmpPreview)

        mActivity.attachView.visibility = View.GONE

        mActivity.composeMessage.setText(" ")

        toggleInputMode()
    }

    private fun clearMediaDraft() {
        mShareDraft = null

        mActivity.composeMessage.setText("")

        mActivity.mediaPreviewContainer.visibility = View.GONE
    }

    fun deleteMessage(eventId: String) {
        mRoom?.getTimeLineEvent(eventId)?.root?.let { mRoom.redactEvent(it, "") }

        requeryCursor()
    }

    fun resendMessage(resendMsg: String, mimeType: String?) {
        if (resendMsg.isNotEmpty()) {
            if (SecureMediaStore.isVfsUri(resendMsg) || SecureMediaStore.isContentUri(resendMsg)) {
                // What do we do with this?
                val request = ShareRequest()
                request.deleteFile = false
                request.resizeImage = false
                request.importContent = false
                request.media = Uri.parse(resendMsg)
                request.mimeType = mimeType

                try {
                    mActivity.sendShareRequest(request)
                }
                catch (e: Exception) {
                    Timber.w(e, "error setting media draft")
                }
            }
            else {
                sendMessageAsync(resendMsg, null)
            }
        }
    }

    private fun sendMessageAsync(msg: String, replyId: String?) {
        sendMessage(msg, replyId, false)
        mActivity.composeMessage.setText("")
        mActivity.composeMessage.requestFocus()
        sendTypingStatus(false)
    }

    private fun sendMessage(msg: String, targetEventId: String?, isQuickReaction: Boolean): Boolean {

        // Don't send empty messages.
        if (msg.isBlank()) return false

        if (isQuickReaction && targetEventId?.isNotEmpty() == true) {
            mRoom?.sendReaction(targetEventId, msg)
        }
        else if (targetEventId?.isNotEmpty() == true) {
            val event = mRoom?.getTimeLineEvent(targetEventId)

            if (event != null) {
                mRoom?.replyToMessage(event, msg, true)
            }
        }
        else {
            mRoom?.sendTextMessage(msg, MessageType.MSGTYPE_TEXT, true)
        }

        return true
    }

    private fun userActionDetected() {
        sendTypingStatus(mActivity.composeMessage.text?.isNotEmpty() == true)
        toggleInputMode()
    }

    private fun toggleInputMode() {
        if (mButtonTalk?.visibility == View.GONE) {
            if (mActivity.composeMessage.text?.isNotEmpty() == true && mActivity.btnSend.visibility == View.GONE) {
                mActivity.audioRecordView.visibility = View.GONE
                mActivity.btnAttachSticker.visibility = View.GONE
                mActivity.btnSend.visibility = View.VISIBLE
                mActivity.btnSend.setImageResource(R.drawable.ic_send_secure)
            }
            else if (mActivity.composeMessage.text.isNullOrEmpty()) {
                mActivity.btnAttachSticker.visibility = View.VISIBLE
                mActivity.audioRecordView.visibility = View.VISIBLE
                mActivity.btnSend.visibility = View.GONE
            }
        }
    }

    fun sendQuickReaction(reaction: String, messageId: String?) {
        sendMessage(reaction, messageId, true)
    }

    private fun toggleAttachMenu() {
        if (mActivity.attachView.visibility == View.INVISIBLE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                // Get the center for the clipping circle.
                val cx = mActivity.attachView.left
                val cy = mActivity.attachView.height

                // Get the final radius for the clipping circle.
                val finalRadius = hypot(cx.toDouble(), cy.toDouble()).toFloat()

                // Create the animator for this view (the start radius is zero).
                val anim = ViewAnimationUtils.createCircularReveal(mActivity.attachView, cx, cy, 0f, finalRadius)

                // Make the view visible and start the animation.
                mActivity.attachView.visibility = View.VISIBLE

                anim.start()
            }
            else {
                mActivity.attachView.visibility = View.VISIBLE
            }

            // Check if no view has focus:
            val view = mActivity.currentFocus
            if (view != null) {
                val imm = mActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
                imm?.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }
        else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                // Get the center for the clipping circle.
                val cx = mActivity.attachView.left
                val cy = mActivity.attachView.height

                // Get the initial radius for the clipping circle.
                val initialRadius = hypot(cx.toDouble(), cy.toDouble()).toFloat()

                // Create the animation (the final radius is zero).
                val anim = ViewAnimationUtils.createCircularReveal(mActivity.attachView, cx, cy, initialRadius, 0f)

                // Make the view invisible when the animation is done.
                anim.addListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        super.onAnimationEnd(animation)
                        mActivity.attachView.visibility = View.INVISIBLE
                    }
                })

                // Start the animation.
                anim.start()
            }
            else {
                mActivity.attachView.visibility = View.INVISIBLE
            }
        }

        mStickerBox.visibility = View.GONE
    }

    private fun toggleStickers() {
        if (mStickerPager == null) initStickers()

        mStickerBox.visibility = if (mStickerBox.visibility == View.GONE) View.VISIBLE else View.GONE
    }

    @Synchronized
    private fun initStickers() {
        mStickerPager = mActivity.stickerPager

        val emojiGroups = StickerManager.getInstance(mActivity).emojiGroups

        if (emojiGroups.isNotEmpty()) {
            val emojiPagerAdapter = StickerPagerAdapter(mActivity, ArrayList(emojiGroups)
            ) { s ->
                sendStickerCode(s.assetUri)

                toggleStickers()
            }
            mStickerPager?.adapter = emojiPagerAdapter
        }
    }

    /**
     * Generate a :pack-sticker: code
     */
    private fun sendStickerCode(assetUri: Uri) {
        val stickerCode = StringBuffer()
        stickerCode.append(":")
        stickerCode.append(assetUri.pathSegments[assetUri.pathSegments.size - 2])
        stickerCode.append("-")
        stickerCode.append(assetUri.lastPathSegment?.split("\\.")?.firstOrNull())
        stickerCode.append(":")

        sendMessageAsync(stickerCode.toString(), null)
    }

    fun showQuickReactionsPopup(messageId: String?) {
        if (emojiPopup != null) return

        val rootView = mActivity.historyView.parent as View

        try {
            val editText = EmojiEditText(mActivity)
            editText.imeOptions = EditorInfo.IME_ACTION_SEND
            editText.inputType = InputType.TYPE_NULL
            editText.setBackgroundColor(-0x80000000)
            editText.setOnClickListener { v -> // If tap on background, close popup!
                if (emojiPopup != null) {
                    val imm = v.context.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
                    imm?.hideSoftInputFromWindow(v.windowToken, 0)
                    emojiPopup?.dismiss()
                }
            }

            SingleEmojiTrait.install(editText)

            (rootView as ViewGroup).addView(editText, ViewGroup.LayoutParams.MATCH_PARENT, 1)

            rootView.postDelayed({
                emojiPopup = EmojiPopup.Builder.fromRootView(rootView)
                        .setOnEmojiPopupDismissListener {
                            emojiPopup = null
                            rootView.removeView(editText)
                        }
                        .setOnEmojiClickListener { _, variant ->
                            sendQuickReaction(variant.unicode, messageId)
                            emojiPopup?.dismiss()
                        }
                        .build(editText)

                emojiPopup?.toggle()
            }, 200)
        }
        catch (e: Exception) {
            Timber.d(e)
        }
    }
}