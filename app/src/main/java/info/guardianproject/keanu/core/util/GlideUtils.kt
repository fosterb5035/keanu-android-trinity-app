package info.guardianproject.keanu.core.util

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.net.Uri
import android.widget.ImageView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import info.guardianproject.keanuapp.ImApp
import timber.log.Timber
import java.io.File
import java.io.FileInputStream

object GlideUtils {

    private var noDiskCacheOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)

    @JvmStatic
    fun loadAvatar(context: Context, avatarUrl: String?, default: Drawable, av: ImageView) {
        val glideRb = Glide.with(context).asDrawable()

        @SuppressLint("CheckResult")
        if (avatarUrl?.isNotEmpty() == true) {
            glideRb.load(avatarUrl)
        }
        else {
            glideRb.load(default)
        }

        glideRb.into(av)
    }

    @JvmStatic
    fun loadImageFromUri(context: Context?, uri: Uri, imageView: ImageView?) {
        if (context == null) return
        if (imageView == null) return

        val progress = CircularProgressDrawable(context)
        progress.strokeWidth = 5f
        progress.centerRadius = 30f
        progress.start()

        val glideRb = Glide.with(context)
                .asDrawable()
                .placeholder(progress)
                .apply(noDiskCacheOptions)

        when {
            SecureMediaStore.isVfsUri(uri) -> {
                try {
                    val path = uri.path ?: return
                    val fileImage = File(path)

                    if (fileImage.exists()) {
                        glideRb.load(FileInputStream(fileImage))
                    }
                } catch (e: Exception) {
                    Timber.w(e, "unable to load image: $uri")
                }
            }

            uri.scheme == "mxc" -> {
                val url = ImApp.sImApp?.matrixSession?.contentUrlResolver()?.resolveFullSize(uri.toString())

                glideRb.load(url)
            }

            uri.scheme == "asset" -> {
                val assetPath = "file:///android_asset/" + uri.path?.substring(1)

                glideRb.load(assetPath)
            }

            else -> {
                glideRb.load(uri)
            }
        }

        glideRb.into(imageView)
    }
}