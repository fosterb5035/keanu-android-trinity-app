/*
 * Copyright (C) 2008 Esmertec AG. Copyright (C) 2008 The Android Open Source
 * Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package info.guardianproject.keanu.core.util;


import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import info.guardianproject.keanu.core.ui.RoundedAvatarDrawable;

public class DatabaseUtils {

    private static final String TAG = "DBUtils";

    private DatabaseUtils() {
    }


    /**
    public static RoundedAvatarDrawable getAvatarFromCursor(Cursor cursor, int dataColumn, int width, int height) throws DecoderException {
        String hexData = cursor.getString(dataColumn);
        if (hexData.equals("NULL")) {
            return null;
        }

        byte[] data = Hex.decodeHex(hexData.substring(2, hexData.length() - 1).toCharArray());
        return decodeRoundAvatar(data, width, height);
    }

    public static BitmapDrawable getHeaderImageFromCursor(Cursor cursor, int dataColumn, int width, int height) throws DecoderException {
        String hexData = cursor.getString(dataColumn);
        if (hexData.equals("NULL")) {
            return null;
        }

        byte[] data = Hex.decodeHex(hexData.substring(2, hexData.length() - 1).toCharArray());
        return decodeSquareAvatar(data, width, height);
    }**/



    public final static String ROOM_AVATAR_ACCESS = "avatarcache";
    public static byte[] getAvatarBytesFromAddress(String address)  {

        byte[] data = null;

        if (!TextUtils.isEmpty(address)) {

            java.io.File fileAvatar = null;
            try {
                fileAvatar = openSecureStorageFile(ROOM_AVATAR_ACCESS, address);

                if (fileAvatar.exists()) {
                    try {
                        data = new byte[(int) fileAvatar.length()];
                        BufferedInputStream buf = new BufferedInputStream(new java.io.FileInputStream(fileAvatar));
                        buf.read(data, 0, data.length);
                        buf.close();
                        return data;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            /**
            String[] projection = {Imps.Avatars.DATA};
            String[] args = {address};
            String query = Imps.Avatars.CONTACT + " LIKE ?";
            Cursor cursor = cr.query(Imps.Avatars.CONTENT_URI, projection,
                    query, args, Imps.Avatars.DEFAULT_SORT_ORDER);


            if (cursor != null) {
                if (cursor.moveToFirst())
                    data = cursor.getBlob(0);

                cursor.close();
            }**/
        }

        return data;

    }

    /**
    public static Uri getAvatarUri(Uri baseUri, long providerId, long accountId) {
        Uri.Builder builder = baseUri.buildUpon();
        ContentUris.appendId(builder, providerId);
        ContentUris.appendId(builder, accountId);
        return builder.build();
    }**/


    private static final String ReservedChars = "[|\\?*<\":>+/!']";

    public static File openSecureStorageFile(String sessionId, String filename) throws FileNotFoundException {
//        debug( "openFile: url " + url) ;
        String localFilename = "/" + sessionId + "/download/" + filename.replaceAll(ReservedChars, "_");

        //  debug( "openFile: localFilename " + localFilename) ;
        java.io.File fileNew = new java.io.File(localFilename);
        fileNew.getParentFile().mkdirs();

        return fileNew;
    }


    public static boolean hasAvatarContact(ContentResolver resolver, Uri updateUri,
            String username) {
        /**
        ContentValues values = new ContentValues(3);
        values.put(Imps.Avatars.CONTACT, username);

        StringBuilder buf = new StringBuilder(Imps.Avatars.CONTACT);
        buf.append("=?");

        String[] selectionArgs = new String[] { username };

        return resolver.update(updateUri, values, buf.toString(), selectionArgs) > 0;
         **/
        File fileAvatar = null;
        try {
            fileAvatar = openSecureStorageFile(ROOM_AVATAR_ACCESS,username);
            return fileAvatar.exists();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }


    }

    public static boolean doesAvatarHashExist(ContentResolver resolver, Uri queryUri,
            String jid, String hash) {

        /**
        StringBuilder buf = new StringBuilder(Imps.Avatars.CONTACT);
        buf.append("=?");
        buf.append(" AND ");
        buf.append(Imps.Avatars.HASH);
        buf.append("=?");

        String[] selectionArgs = new String[] { jid, hash };

        Cursor cursor = resolver.query(queryUri, null, buf.toString(), selectionArgs, null);
        if (cursor == null)
            return false;
        try {
            return cursor.getCount() > 0;
        } finally {
            cursor.close();
        }**/
        return false;
    }


    private static BitmapDrawable decodeSquareAvatar(byte[] data, int width, int height) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(data, 0, data.length, options);
        options.inSampleSize = calculateInSampleSize(options, width, height);
        options.inJustDecodeBounds = false;
        Bitmap b = BitmapFactory.decodeByteArray(data, 0, data.length,options);
        if (b != null)
        {
            return new BitmapDrawable(b);
        }
        else
            return null;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
    // Raw height and width of image
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;

    if (height > reqHeight || width > reqWidth) {

        // Calculate ratios of height and width to requested height and width
        final int heightRatio = Math.round((float) height / (float) reqHeight);
        final int widthRatio = Math.round((float) width / (float) reqWidth);

        // Choose the smallest ratio as inSampleSize value, this will guarantee
        // a final image with both dimensions larger than or equal to the
        // requested height and width.
        inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
    }

    return inSampleSize;
}




}
