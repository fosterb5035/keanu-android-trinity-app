package info.guardianproject.keanu.core.ui.chatlist

import android.app.Activity
import android.content.Intent
import android.graphics.*
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanuapp.ImApp
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.ChatListItemBinding
import info.guardianproject.keanu.core.ui.room.RoomActivity
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.content.ContentUrlResolver.ThumbnailMethod
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.session.room.model.message.MessageAudioContent
import org.matrix.android.sdk.api.session.room.model.message.MessageFileContent
import org.matrix.android.sdk.api.session.room.model.message.MessageImageContent
import org.matrix.android.sdk.api.session.room.model.message.MessageVideoContent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import java.util.*
import java.util.regex.Pattern

class ChatListAdapter(private val mFragment: ChatListFragment) : RecyclerView.Adapter<ChatListItemHolder>(), Observer<List<RoomSummary>> {

    private val mActivity: Activity?
        get() = mFragment.activity

    private val mSession: Session?
        get() = (mActivity?.application as? ImApp)?.matrixSession

    private val mTypedValue = TypedValue()
    private val mBackground: Int
    private var mRoomList = ArrayList<RoomSummary>()
    private var mFilterString: String? = null

    private val queryParams by lazy {
        val builder = RoomSummaryQueryParams.Builder()
        builder.memberships = listOf(Membership.JOIN, Membership.INVITE)
        builder.build()
    }

    init {
        mActivity?.theme?.resolveAttribute(R.attr.chatBackground, mTypedValue, true)
        mBackground = mTypedValue.resourceId
        setHasStableIds(true)

        onChanged(mSession?.getRoomSummaries(queryParams))

        mSession?.getRoomSummariesLive(queryParams)?.observe(mFragment, this)
    }

    fun filter(filterString: String?) {
        mFilterString = filterString

        onChanged(mSession?.getRoomSummaries(queryParams))
    }

    var showArchived = false
        set(value) {
            field = value

            onChanged(mSession?.getRoomSummaries(queryParams))
        }

    fun get(id: Long) : RoomSummary? {
        return mRoomList.getOrNull(id.toInt())
    }

    override fun onChanged(roomSummaries: List<RoomSummary>?) {
        val newRoomList = ArrayList<RoomSummary>()
        var searchPattern: Pattern? = null

        if (mFilterString?.isNotEmpty() == true) searchPattern = Pattern.compile(Pattern.quote(mFilterString!!), Pattern.CASE_INSENSITIVE)

        for (rs in roomSummaries ?: listOf()) {
            val hasArchiveTag = rs.tags.find { it.name == ChatListFragment.ARCHIVED_TAG } != null

            if ((showArchived && !hasArchiveTag) || (!showArchived && hasArchiveTag)) {
                continue
            }

            if (searchPattern?.matcher(rs.displayName)?.find() == false) {
                continue
            }

            if (newRoomList.isEmpty()) {
                newRoomList.add(rs)
            }
            else {
                if (rs.membership == Membership.INVITE) {
                    newRoomList.add(0, rs)
                    continue
                }

                var shouldAdd = true

                for (i in newRoomList.indices) {
                    val (_, _, _, _, _, _, _, _, _, _, latestPreviewableEvent) = newRoomList[i]

                    if (latestPreviewableEvent != null && rs.latestPreviewableEvent != null) {
                        if (rs.latestPreviewableEvent?.root?.ageLocalTs ?: 0 > latestPreviewableEvent.root.ageLocalTs ?: 0) {
                            newRoomList.add(i, rs)
                            shouldAdd = false
                            break
                        }
                    } else {
                        newRoomList.add(rs)
                        shouldAdd = false
                        break
                    }
                }

                if (shouldAdd) newRoomList.add(rs)
            }
        }

        mRoomList = newRoomList

        notifyDataSetChanged()

        mFragment.updateListVisibility()
    }

    fun refresh() {
        onChanged(mSession?.getRoomSummaries(queryParams))
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {
        return mRoomList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatListItemHolder {
        val binding = ChatListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        binding.root.setBackgroundResource(mBackground)

        var viewHolder = binding.root.tag as? ChatListItemHolder

        if (viewHolder == null) {
            viewHolder = ChatListItemHolder(binding)
            binding.root.tag = viewHolder
        }

        return viewHolder
    }

    override fun onBindViewHolder(viewHolder: ChatListItemHolder, position: Int) {
        val clItem = viewHolder.itemView as? ChatListItem
        val roomSummary = mRoomList[position]
        val address = roomSummary.roomId
        val nickname = roomSummary.displayName
        var lastMsg = roomSummary.topic
        var lastMsgDate = Date().time
        val lastMsgType: String? = null
        val avatarUrl = mSession?.contentUrlResolver()?.resolveThumbnail(roomSummary.avatarUrl, 120, 120, ThumbnailMethod.SCALE)

        viewHolder.itemView

        if (roomSummary.membership == Membership.INVITE) {
            mActivity?.getString(R.string.invitation_prompt, nickname)?.let { lastMsg = it }
        }
        else {
            val tlEvent = roomSummary.latestPreviewableEvent

            if (tlEvent != null) {
                val event = tlEvent.root
                event.originServerTs?.let { lastMsgDate = it }
                val messageMap = event.getClearContent()

                val sender = event.senderId?.let { mSession?.getUser(it) }

                lastMsg = when {
                    sender?.displayName?.isNotBlank() == true -> sender.displayName!!

                    event.senderId?.isNotBlank() == true -> event.senderId!!

                    else -> ""
                }

                val mc = tlEvent.getLastMessageContent()

                if (mc != null) {
                    lastMsg += ": " + when (mc) {
                        is MessageImageContent -> mActivity?.getString(R.string.sent_image)

                        is MessageAudioContent -> mActivity?.getString(R.string.sent_audio)

                        is MessageVideoContent -> mActivity?.getString(R.string.sent_video)

                        is MessageFileContent -> mActivity?.getString(R.string.sent_file)

                        else -> if (mc.body.isNotBlank()) mc.body else ""
                    }
                }
                else if (event.getClearType() == "m.reaction") {

                   val relatesTo = messageMap?.get("m.relates_to") as? AbstractMap<*, *>
                   val reaction = relatesTo?.get("key")

                    if (reaction != null) lastMsg += ": $reaction"
                }
                else {
                    lastMsg = ""
                }
            }
        }

        clItem?.bind(viewHolder, address, nickname, avatarUrl, lastMsg, lastMsgDate, lastMsgType, true)

        if (roomSummary.hasNewMessages || roomSummary.hasUnreadMessages) {
            val ssb = SpannableStringBuilder(viewHolder.line1.text)

            ssb.setSpan(StyleSpan(Typeface.BOLD), 0, ssb.length, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
            ssb.setSpan(ForegroundColorSpan(Color.BLACK), 0, ssb.length, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)

            viewHolder.line1.text = ssb
            viewHolder.markerUnread.visibility = View.VISIBLE
        }
        else {
            viewHolder.markerUnread.visibility = View.GONE
        }

        clItem?.setOnClickListener { v: View ->
            val intent = Intent(v.context, RoomActivity::class.java)
            intent.putExtra("address", address)
            v.context.startActivity(intent)
        }
    }

    class ArchiveTouchHelperCallback(private var mOnSwiped: (position: Int) -> Unit) : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT)
    {

        private val mBackground = Paint()

        private lateinit var mRecyclerView: RecyclerView

        private val mUnarchiveIcon by lazy {
            BitmapFactory.decodeResource(mRecyclerView.context.resources,
                    R.drawable.ic_unarchive_white_24dp)
        }

        private val mArchiveIcon by lazy {
            BitmapFactory.decodeResource(mRecyclerView.context.resources,
                    R.drawable.ic_archive_white_24dp)
        }

        private val mGreen by lazy {
            ResourcesCompat.getColor(mRecyclerView.context.resources, R.color.holo_green_dark, null)
        }

        private val mGray by lazy {
            Color.argb(255, 150, 150, 150)
        }

        override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
                                 dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean)
        {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)

            val iv = viewHolder.itemView

            if (dX > 0) {
                mRecyclerView = recyclerView

                val icon: Bitmap

                if ((recyclerView.adapter as ChatListAdapter).showArchived) {
                    icon = mUnarchiveIcon
                    mBackground.color = mGreen
                }
                else {
                    icon = mArchiveIcon
                    mBackground.color = mGray
                }

                // Draw rectangle with varying right side, equal to displacement dX.
                c.drawRect(iv.left.toFloat(), iv.top.toFloat(), dX, iv.bottom.toFloat(), mBackground)

                // Set the image icon for right swipe.
                c.drawBitmap(icon,
                        iv.left.toFloat() + (16 * (recyclerView.context.resources.displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)),
                        iv.top.toFloat() + (iv.bottom.toFloat() - iv.top.toFloat() - icon.height) / 2,
                        mBackground)


                // Fade out the view as it is swiped out of the parent's bounds.
                iv.alpha = ALPHA_FULL - kotlin.math.abs(dX) / viewHolder.itemView.width.toFloat()
                iv.translationX = dX
            }
            else {
                iv.alpha = ALPHA_FULL
                iv.translationX = 0F
            }
        }

        override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
            super.clearView(recyclerView, viewHolder)

            viewHolder.itemView.alpha = ALPHA_FULL
            viewHolder.itemView.translationX = 0F
        }

        override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
            return false
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            mOnSwiped(viewHolder.adapterPosition)
        }
    }

    companion object {
        const val ALPHA_FULL = 1.0f
    }
}