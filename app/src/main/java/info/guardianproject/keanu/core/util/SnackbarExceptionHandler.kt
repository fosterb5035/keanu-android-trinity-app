package info.guardianproject.keanu.core.util

import android.view.View
import com.google.android.material.snackbar.Snackbar
import info.guardianproject.keanuapp.R
import kotlinx.coroutines.CoroutineExceptionHandler
import org.matrix.android.sdk.api.failure.Failure
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

/**
 * Base exception handler which shows a more-or-less useful error message as a Snackbar,
 * when the root coroutine cancels because of an exception.
 *
 * You might use it like this:
 *
 * ```Kotlin
 * private val mCoroutineScope: CoroutineScope by lazy {
 *     CoroutineScope(Dispatchers.IO + SnackbarExceptionHandler(mBinding.root))
 * }
 * ```
 */
class SnackbarExceptionHandler(private val view: View) : CoroutineExceptionHandler {

    override val key: CoroutineContext.Key<*>
        get() = CoroutineExceptionHandler

    override fun handleException(context: CoroutineContext, exception: Throwable) {
        Timber.d(exception)

        var msg = when (exception) {
            is Failure.ServerError -> {
                exception.error.message
            }
            is Exception -> {
                // If we don't cast, Throwable#localizedMessage will return nothing.
                exception.localizedMessage
            }
            else -> {
                exception.localizedMessage
            }
        }

        if (msg.isNullOrBlank()) msg = exception.message
        if (msg.isNullOrBlank()) msg = exception.toString()
        if (msg.isNullOrBlank()) msg = view.context.getString(R.string.error)

        Snackbar.make(view, msg, Snackbar.LENGTH_LONG).show()
    }
}