package info.guardianproject.keanu.core.ui.room;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.text.TextUtils;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import org.apache.commons.io.IOUtils;
import org.jetbrains.annotations.NotNull;
import org.matrix.android.sdk.api.MatrixCallback;
import org.matrix.android.sdk.api.session.Session;
import org.matrix.android.sdk.api.session.content.ContentUrlResolver;
import org.matrix.android.sdk.api.session.events.model.Event;
import org.matrix.android.sdk.api.session.events.model.EventType;
import org.matrix.android.sdk.api.session.room.Room;
import org.matrix.android.sdk.api.session.room.model.ReactionAggregatedSummary;
import org.matrix.android.sdk.api.session.room.model.ReadReceipt;
import org.matrix.android.sdk.api.session.room.model.message.MessageContent;
import org.matrix.android.sdk.api.session.room.model.message.MessageWithAttachmentContent;
import org.matrix.android.sdk.api.session.room.sender.SenderInfo;
import org.matrix.android.sdk.api.session.room.timeline.EventTypeFilter;
import org.matrix.android.sdk.api.session.room.timeline.Timeline;
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent;
import org.matrix.android.sdk.api.session.room.timeline.TimelineEventFilters;
import org.matrix.android.sdk.api.session.room.timeline.TimelineSettings;
import org.matrix.android.sdk.internal.crypto.MXEventDecryptionResult;
import org.matrix.android.sdk.internal.di.MoshiProvider;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import info.guardianproject.keanu.core.KeanuConstants;
import info.guardianproject.keanu.core.Preferences;
import info.guardianproject.keanuapp.ImApp;
import info.guardianproject.keanuapp.R;
import info.guardianproject.keanuapp.ui.contacts.ContactDisplayActivity;
import info.guardianproject.keanuapp.ui.conversation.MessageListItem;
import info.guardianproject.keanuapp.ui.conversation.QuickReaction;
import info.guardianproject.keanuapp.ui.legacy.Markup;
import info.guardianproject.keanuapp.ui.widgets.ImageViewActivity;
import info.guardianproject.keanuapp.ui.widgets.MessageViewHolder;
import kotlin.Unit;

import static android.content.Context.CLIPBOARD_SERVICE;

public class MessageListAdapter
        extends RecyclerView.Adapter<MessageViewHolder> implements MessageViewHolder.OnImageClickedListener, MessageViewHolder.OnQuickReactionClickedListener {



    EventTypeFilter[] LIMIT_DISPLAYABLE_TYPES = {
            new EventTypeFilter (EventType.MESSAGE, null),
            new EventTypeFilter (EventType.STICKER, null),
            new EventTypeFilter (EventType.ENCRYPTED, null),
    };


    private ActionMode mActionMode;
    private View mLastSelectedView;
    private Activity mContext;
    private RoomView mView;
    Markup mMarkup;


    //    private static final long SHOW_TIME_STAMP_INTERVAL = 30 * 1000; // 15 seconds
    private static final long SHOW_DELIVERY_INTERVAL = 10 * 1000; // 5 seconds
    private static final long SHOW_MEDIA_DELIVERY_INTERVAL = 120 * 1000; // 2 minutes
    private static final long DEFAULT_QUERY_INTERVAL = 2000;
    private static final long FAST_QUERY_INTERVAL = 200;
    private int mScrollState = -1;
    private boolean mExpectingDelivery = false;

    private Room mRoom;
    private Session mSession;
    private List<TimelineEvent> mTimelineList = new ArrayList<>();
    private Timeline mTimeline;

    private boolean mIsBackPage = false;

    private long mLatestReadReceipt = -1;

    private Handler mHandler = new Handler();

    public MessageListAdapter(Activity context, RoomView view, String roomId) {
        super();

        mContext = context;
        mView = view;
        setHasStableIds(true);

        mSession = ((ImApp)context.getApplication()).getMatrixSession();
        mRoom = mSession.getRoom(roomId);

        List<EventTypeFilter> allowedTypes = Arrays.asList(LIMIT_DISPLAYABLE_TYPES);
        TimelineSettings tSettings = new TimelineSettings(20,
                new TimelineEventFilters(true, true, true, true, allowedTypes),
                true);

        mTimeline = mRoom.createTimeline(null, tSettings);

        mTimeline.addListener(new Timeline.Listener() {
            @Override
            public void onTimelineUpdated(@NotNull List<TimelineEvent> list) {
                mTimelineList = list;

                if (mIsBackPage)
                    notifyItemRangeChanged(0, 20);
                else
                    notifyDataSetChanged();

                //if at bottom, then jump to new message
                if (!((RoomActivity) mContext).getHistoryView().canScrollVertically(1))
                {
                    mView.jumpToBottom();

                    if (mTimelineList.size() > 0) {
                        //set read marker if at bottom of chat, not scrolling back
                        mRoom.setReadMarker(mTimelineList.get(mTimelineList.size() - 1).getEventId(), new MatrixCallback<Unit>() {
                            @Override
                            public void onSuccess(Unit unit) {

                            }

                            @Override
                            public void onFailure(@NotNull Throwable throwable) {

                            }
                        });
                    }
                }
                else if (mIsBackPage)
                {
                    if (mTimelineList.size() > 20)
                        mView.jumpToPosition(25);
                }

                mIsBackPage = false;
            }

            @Override
            public void onTimelineFailure(@NotNull Throwable throwable) {

            }

            @Override
            public void onNewTimelineEvents(@NotNull List<String> list) {

                for (String tEventId : list) {
                    TimelineEvent tEvent = mRoom.getTimeLineEvent(tEventId);
                    if (tEvent != null)
                        handleTimelineEventNotification(tEvent);
                }



            }
        });

        mTimeline.start();

        mTimeline.paginate(Timeline.Direction.FORWARDS,10);

    }

    private void handleTimelineEventNotification (TimelineEvent tEvent)
    {
        for (TimelineEvent te : mTimelineList)
        {
            if (te.getEventId().equals(tEvent.getEventId()))
            {
                return;
            }
        }

        mTimelineList.add(tEvent);

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                notifyItemInserted(mTimelineList.size() - 1);
                notifyDataSetChanged();
            }
        });
    }

    public void pageBack () {

        if (mTimeline.hasMoreToLoad(Timeline.Direction.BACKWARDS))
        {
            mIsBackPage = true;
            mTimeline.paginate(Timeline.Direction.BACKWARDS,20);
        }

    }

    public View getLastSelectedView ()
    {
        return mLastSelectedView;
    }

    @Override
    public long getItemId (int position)
    {
        return mTimelineList.get(position).getLocalId();
    }

    @Override
    public int getItemCount() {
        return mTimelineList.size();
    }

    @Override
    public int getItemViewType(int position) {
        int actualPosition = mTimelineList.size()-position-1;

        int messageType = KeanuConstants.MessageType.INCOMING_ENCRYPTED;//TODO

        Event event = mTimelineList.get(actualPosition).getRoot();
        Map<String,Object> messageContent = event.getClearContent();

        if (event.getSenderId().equals(mSession.getMyUserId())) {
            if (mRoom.isEncrypted())
                messageType = KeanuConstants.MessageType.OUTGOING_ENCRYPTED;
            else
                messageType = KeanuConstants.MessageType.OUTGOING;
        }
        else
        {
            if (mRoom.isEncrypted())
                messageType = KeanuConstants.MessageType.INCOMING_ENCRYPTED;
        }

        boolean isLeft = (messageType == KeanuConstants.MessageType.INCOMING_ENCRYPTED)||(messageType == KeanuConstants.MessageType.INCOMING);

        if (isLeft)
            return 0;
        else
            return 1;

    }



    void setLinkifyForMessageView(MessageListItem messageView) {

        if (messageView == null)
            return;

        messageView.setLinkify(Preferences.getDoLinkify());


    }


    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MessageListItem view = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (viewType == 0)
            view = (MessageListItem)inflater
                    .inflate(R.layout.message_view_left, parent, false);
        else
            view = (MessageListItem)inflater
                    .inflate(R.layout.message_view_right, parent, false);

        view.setOnLongClickListener(new View.OnLongClickListener() {
            // Called when the user long-clicks on someView
            public boolean onLongClick(View view) {

                mLastSelectedView = view;

                if (mActionMode != null) {
                    return false;
                }

                // Start the CAB using the ActionMode.Callback defined above
                mActionMode = ((Activity) mContext).startActionMode(mActionModeCallback);

                return true;
            }
        });


        MessageViewHolder mvh = new MessageViewHolder(view);
        mvh.setLayoutInflater(inflater);
        mvh.setOnImageClickedListener(this);
        mvh.setOnQuickReactionClickedListener(this);
        view.applyStyleColors();
        return mvh;
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder viewHolder, int position) {

        MessageListItem messageView = (MessageListItem) viewHolder.itemView;
        setLinkifyForMessageView(messageView);
        messageView.applyStyleColors();

        int messageType = KeanuConstants.MessageType.INCOMING;
        int actualPosition = mTimelineList.size()-position-1;

        TimelineEvent tEvent = mTimelineList.get(actualPosition);
        Event event = mTimelineList.get(actualPosition).getRoot();


        mRoom.setReadMarker(event.getEventId(), new MatrixCallback<Unit>() {
            @Override
            public void onSuccess(Unit unit) {

            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {

            }
        });

        Map<String,Object> messageMap = event.getClearContent();

        if (event.getSenderId().equals(mSession.getMyUserId())) {
            if (mRoom.isEncrypted())
                messageType = KeanuConstants.MessageType.OUTGOING_ENCRYPTED;
            else
                messageType = KeanuConstants.MessageType.OUTGOING;
        }
        else
        {
            if (mRoom.isEncrypted())
                messageType = KeanuConstants.MessageType.INCOMING_ENCRYPTED;
        }

        if (event.isEncrypted()) {

            if (messageMap == null
                    || TextUtils.isEmpty((String)messageMap.get("body"))) {


                    mSession.cryptoService().decryptEventAsync(event, mTimeline.getTimelineID(), new MatrixCallback<MXEventDecryptionResult>() {
                        @Override
                        public void onSuccess(MXEventDecryptionResult mxEventDecryptionResult) {

                            ((RoomActivity) mContext).getHistoryView().post(new Runnable() {
                                @Override
                                public void run() {
                                    notifyDataSetChanged();
                                }
                            });

                        }

                        @Override
                        public void onFailure(@NotNull Throwable throwable) {

                        }
                    });

            }
        }


        SenderInfo senderInfo = tEvent.getSenderInfo();

        final String roomAddress = mRoom.getRoomId();
        final String roomName = mRoom.roomSummary().getDisplayName();

        String userNick = senderInfo.getDisplayName();
        if (TextUtils.isEmpty(userNick))
            userNick = senderInfo.getUserId().substring(1);

        String mimeType = null;
        String mediaUrl = null;
        String thumbUrl = null;
        final int id = 1;
        String body = (String)messageMap.get("body");
        boolean showTimeStamp = true;//(delta > SHOW_TIME_STAMP_INTERVAL);
        final long timestamp = event.getOriginServerTs();
        final String packetId = event.getEventId();
        final String replyId = "def123";

        if (messageMap.containsKey("info")) {
            Moshi moshi = MoshiProvider.INSTANCE.providesMoshi();
            JsonAdapter<MessageContent> ja = moshi.adapter(MessageContent.class);
            MessageContent mc = ja.fromJsonValue(event.getClearContent());

            AbstractMap info = (AbstractMap) messageMap.get("info");
            mimeType = (String) info.get("mimetype");
            mediaUrl = (String) messageMap.get("url");

            if (messageMap.containsKey("thumbnail_url")) {
                body = (String) messageMap.get("thumbnail_url");
            } else {
                body = mediaUrl;
            }

            body = mSession.contentUrlResolver().resolveFullSize(body);

            if (mc instanceof MessageWithAttachmentContent) {
                MessageWithAttachmentContent mac = (MessageWithAttachmentContent) mc;

                if (!mSession.fileService().isFileInCache(mac)) {
                    mSession.fileService().downloadFile(mac, new MatrixCallback<File>() {
                        @Override
                        public void onSuccess(File file) {
                            notifyDataSetChanged();
                        }

                        @Override
                        public void onFailure(@NotNull Throwable throwable) {

                        }
                    });

                    body = null;

                } else {
                    Uri uriMedia = mSession.fileService().getTemporarySharableURI(mac);

                    if (uriMedia != null)
                        body = uriMedia.toString();
                }
            }
        }

        final Date date = showTimeStamp ? new Date(timestamp) : null;

        boolean isDelivered = false;
        List<ReadReceipt> rr = tEvent.getReadReceipts();
        if (rr != null && rr.size() > 0) {
            isDelivered = true;
            for (ReadReceipt receipt : rr)
            {
                if (receipt.getOriginServerTs() > mLatestReadReceipt)
                    mLatestReadReceipt = receipt.getOriginServerTs();
            }
        }

        if (!isDelivered)
        {
            if (event.getOriginServerTs() < mLatestReadReceipt)
                isDelivered = true;
        }

        viewHolder.mPacketId = packetId;
        viewHolder.mMimeType = mimeType;

        MessageListItem.DeliveryState deliveryState = MessageListItem.DeliveryState.NEUTRAL;

        if (!isDelivered && mExpectingDelivery) {
            deliveryState = MessageListItem.DeliveryState.UNDELIVERED;

        }
        else if (isDelivered)
        {
            deliveryState = MessageListItem.DeliveryState.DELIVERED;

        }

        boolean isLastMessage = false;

        if (isDelivered  || (messageType == KeanuConstants.MessageType.OUTGOING||messageType ==KeanuConstants.MessageType.OUTGOING_ENCRYPTED)) {
            mExpectingDelivery = false;
            viewHolder.progress.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(body))
        {
            body = "(unable to decrypt)";
        }

        MessageListItem.EncryptionState encState = MessageListItem.EncryptionState.NONE;
        if (messageType == KeanuConstants.MessageType.INCOMING_ENCRYPTED)
        {
            messageType = KeanuConstants.MessageType.INCOMING;
            encState = MessageListItem.EncryptionState.ENCRYPTED;
        }
        else if (messageType == KeanuConstants.MessageType.OUTGOING_ENCRYPTED)
        {

            messageType = KeanuConstants.MessageType.OUTGOING;
            encState = MessageListItem.EncryptionState.ENCRYPTED;
        }

        switch (messageType) {
            case KeanuConstants.MessageType.INCOMING:



                String avatarUrl = senderInfo.getAvatarUrl();

                avatarUrl = mSession.contentUrlResolver().resolveThumbnail(avatarUrl,120,120, ContentUrlResolver.ThumbnailMethod.SCALE);

                messageView.bindIncomingMessage(viewHolder,id, messageType, avatarUrl, userNick, mimeType,
                        body, date, mMarkup, false, encState, true, mView.getRemotePresence(), null, packetId, replyId);


                break;

            case KeanuConstants.MessageType.OUTGOING:
            case KeanuConstants.MessageType.QUEUED:
            case KeanuConstants.MessageType.SENDING:

                int errCode = 0; //TODO
                if (errCode != 0) {
                    messageView.bindErrorMessage(errCode);
                } else {

                    messageView.bindOutgoingMessage(viewHolder, id, messageType, null, mimeType, body, date, mMarkup, false,
                            deliveryState, encState, packetId);

                }

                break;

            default:
                // Log.v("ImageSend","default in switch");
                messageView.bindPresenceMessage(viewHolder, roomName, userNick, messageType, date, true, false);

        }

        // Set quick reaction listener
        View contextMenuView = (messageType == KeanuConstants.MessageType.INCOMING) ?
                viewHolder.mAvatar :
                (viewHolder.itemView.findViewById(R.id.message_container) != null) ? viewHolder.itemView.findViewById(R.id.message_container) : viewHolder.itemView;
        if (contextMenuView != null) {
            contextMenuView.setOnCreateContextMenuListener((menu, v, menuInfo) -> {
                mContext.getMenuInflater().inflate(R.menu.menu_message_avatar, menu);
                menu.findItem(R.id.menu_message_add_qr).setOnMenuItemClickListener(item -> {
                    // Pick emoji as quick reaction
                    contextMenuView.post(() -> mView.showQuickReactionsPopup(packetId));
                    return true;
                });
            });
            contextMenuView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ContactDisplayActivity.class);
                    intent.putExtra("address",senderInfo.getUserId());
                    mContext.startActivity(intent);
                }
            });

            contextMenuView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        v.showContextMenu(v.getWidth() / 2.0f, v.getHeight() / 2.0f);
                    } else {
                        v.showContextMenu();
                    }
                    return true;
                }


            });
        }

        if (!isDelivered)
            mView.sendMessageRead(packetId);


        final Map<String, QuickReaction> map = new HashMap<>();

        if (tEvent.getAnnotations() != null) {
            List<ReactionAggregatedSummary> listReactions = tEvent.getAnnotations().getReactionsSummary();

            for (ReactionAggregatedSummary ras : listReactions) {
                int count = ras.getCount();
                String reaction = ras.getKey();

                QuickReaction react = new QuickReaction(reaction, new ArrayList<String>());

                List<String> listIds = ras.getSourceEvents();
                for (String eventId : listIds)
                {
                    TimelineEvent teReact = mRoom.getTimeLineEvent(eventId);
                    if (teReact != null && teReact.getSenderInfo() != null) {
                        String senderId = teReact.getSenderInfo().getUserId();
                        react.senders.add(senderId);

                        if (mSession.getMyUserId().equals(senderId))
                            react.sentByMe = true;
                    }
                }

                map.put(reaction, react);

                /**
                listIds = ras.getLocalEchoEvents();
                for (String eventId : listIds)
                {
                    TimelineEvent tEvent = mRoom.getTimeLineEvent(eventId);
                    String senderId = mRoom.getTimeLineEvent(eventId).getSenderInfo().getUserId();
                    react.senders.add(senderId);

                    if (mSession.getMyUserId().equals(senderId))
                        react.sentByMe = true;
                }**/

                map.put(reaction, react);

            }


            viewHolder.setReactions(packetId,new ArrayList<>(map.values()));
        }
        else
            viewHolder.setReactions(packetId, null);



        if (messageView.isSelected())
            viewHolder.mContainer.setBackgroundColor(mContext.getColor(R.color.holo_blue_bright));

    }


    public void onScrollStateChanged(AbsListView viewNew, int scrollState) {
        int oldState = mScrollState;
        mScrollState = scrollState;

        //TODO mark as read?

        if (oldState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
            notifyDataSetChanged();
        }
    }

    boolean isScrolling() {
        return mScrollState != RecyclerView.SCROLL_STATE_IDLE;
    }

    ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_message_context, menu);
            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

            int itemId = item.getItemId();

            if (itemId == R.id.menu_message_delete) {
                mView.deleteMessage(((MessageListItem) mLastSelectedView).getPacketId());
                mode.finish(); // Action picked, so close the CAB
                return true;
            }
            else if (itemId == R.id.menu_message_share) {
                    ((MessageListItem)mLastSelectedView).exportMediaFile();
                    mode.finish(); // Action picked, so close the CAB
                    return true;
            }
            else if (itemId == R.id.menu_message_forward) {
                    ((MessageListItem)mLastSelectedView).forwardMediaFile();
                    mode.finish(); // Action picked, so close the CAB
                    return true;
            }
            else if (itemId == R.id.menu_message_nearby) {
                    ((MessageListItem)mLastSelectedView).nearbyMediaFile();
                    mode.finish(); // Action picked, so close the CAB
                    return true;
            }
            else if (itemId == R.id.menu_message_resend) {
                    mView.resendMessage(((MessageListItem)mLastSelectedView).getLastMessage(),((MessageListItem)mLastSelectedView).getMimeType());
                    mode.finish(); // Action picked, so close the CAB
                    return true;
            }
            else if (itemId == R.id.menu_message_copy) {
                    String messageText = ((MessageListItem)mLastSelectedView).getLastMessage();
                    ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText(mContext.getString(R.string.app_name), messageText);
                    clipboard.setPrimaryClip(clip);
                    mode.finish();
                    Toast.makeText(mContext, R.string.action_copied,Toast.LENGTH_SHORT).show();
                    return true;
            }
            else if (itemId == R.id.menu_downLoad) {
                    if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Uri mediaUri = ((MessageListItem)mLastSelectedView).getMediaUri();
                        File sd = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/Keanu/");
                        String extension = mediaUri.getPath().substring(mediaUri.getPath().lastIndexOf("."));
                        String filename = "Keanu_"+getDateTime()+extension;
                        new DownloadAudio().execute(mediaUri,filename,sd);
                    } else {
                        ActivityCompat.requestPermissions(mContext,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE}, 104);
                    }
                    mode.finish();
                    return true;
            }

            return false;


        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;

            if (mLastSelectedView != null)
                mLastSelectedView.setSelected(false);

            mLastSelectedView = null;
        }
    };

    @Override
    public void onImageClicked(MessageViewHolder viewHolder, Uri image) {

        ArrayList<Uri> urisToShow = new ArrayList<>();
        ArrayList<String> mimeTypesToShow = new ArrayList<>();
        ArrayList<String> messagePacketIds = new ArrayList<>();

        mimeTypesToShow.add(viewHolder.mMimeType);
        urisToShow.add(image);
        messagePacketIds.add(viewHolder.mPacketId);

        //TODO iterate through media here
        /**
         do {
         try {
         String mime = "image/jpeg";//TODO look this up!
         if (!TextUtils.isEmpty(mime) && (
         mime.startsWith("image/") ||
         mime.startsWith("audio/") ||
         mime.startsWith("video/") ||
         mime.contentEquals("application/pdf"))) {
         Uri uri = Uri.parse("file://imagepath.jpg");//TODO set the right path here
         urisToShow.add(uri);
         mimeTypesToShow.add(mime);
         messagePacketIds.add("abc123");//TODO set the actual matrix message/event id here
         }
         } catch (Exception ignored) {
         }
         } while ();
         **/

        if (mContext instanceof RoomActivity) {
            Intent intent = new Intent(mContext, ImageViewActivity.class);

            intent.putExtra("showResend",true);

            // These two are parallel arrays
            intent.putExtra(ImageViewActivity.URIS, urisToShow);
            intent.putExtra(ImageViewActivity.MIME_TYPES, mimeTypesToShow);
            intent.putExtra(ImageViewActivity.MESSAGE_IDS, messagePacketIds);

            int indexOfCurrent = urisToShow.indexOf(image);
            if (indexOfCurrent == -1) {
                indexOfCurrent = 0;
            }
            intent.putExtra(ImageViewActivity.CURRENT_INDEX, indexOfCurrent);

            ((RoomActivity) mContext).getRequestImageView().launch(intent);
        }

    }

    @Override
    public void onQuickReactionClicked(MessageViewHolder viewHolder, QuickReaction quickReaction, String messageId) {
        // TODO - Remove my own reaction, but that is just sending it twice right?
        //if (quickReaction.sentByMe) {
        //}
        mView.sendQuickReaction(quickReaction.reaction,messageId);
    }


    private String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    private class DownloadAudio extends AsyncTask<Object, Void, Long> {
        String storagePath = null;

        @Override
        protected Long doInBackground(Object... params) {
            Uri audioUri = (Uri) params[0];
            String filename = (String) params[1];
            File sd = (File) params[2];
            storagePath = sd.getPath();

            long bytesCopied= 0;
            if(!sd.exists()){
                sd.mkdirs();
            }
            File dest = new File(sd, filename);
            FileInputStream fis = null;
            FileOutputStream fos = null;
            try {
                fis = new FileInputStream(new java.io.File(audioUri.getPath()));
                fos = new java.io.FileOutputStream(dest, false);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            try {
                bytesCopied = IOUtils.copyLarge(fis, fos);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bytesCopied;
        }

        protected void onPostExecute(Long result) {
            if(result > 0){
                Toast.makeText(mContext,mContext.getString(R.string.audio_downloaded)+storagePath.toString(),Toast.LENGTH_LONG).show();
            }
        }
    }
}