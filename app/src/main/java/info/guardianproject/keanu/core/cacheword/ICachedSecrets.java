
package info.guardianproject.keanu.core.cacheword;

public interface ICachedSecrets {

    /**
     * Securely wipes all sensitive data
     */
    public void destroy();

}
