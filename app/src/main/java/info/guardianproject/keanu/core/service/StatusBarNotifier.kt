package info.guardianproject.keanu.core.service

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.media.AudioManager
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.provider.Settings
import android.text.TextUtils
import androidx.core.app.NotificationCompat
import info.guardianproject.keanu.core.KeanuConstants
import info.guardianproject.keanu.core.Preferences
import info.guardianproject.keanu.core.ui.DummyActivity
import info.guardianproject.keanu.core.util.SoundService
import info.guardianproject.keanu.core.verification.NewDeviceActivity
import info.guardianproject.keanu.core.verification.VerificationManager
import info.guardianproject.keanuapp.R
import org.matrix.android.sdk.internal.crypto.model.CryptoDeviceInfo
import java.util.*

class StatusBarNotifier(private val mContext: Context) {

    companion object {
        val VIBRATE_PATTERN = longArrayOf(0, 250, 250, 250)

        @JvmStatic
        var defaultMainClass = "info.guardianproject.keanuapp.MainActivity"

        private const val DBG = false

        private val VIBRATION_TIMINGS = longArrayOf(0, 500, 100, 100, 1000)

        private var UNIQUE_INT_PER_CALL = 10000

        private fun log(msg: String) {
            if (DBG) RemoteImService.debug("[StatusBarNotify] $msg")
        }
    }

    private val mNotificationManager: NotificationManager? = mContext
            .getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager

    private val mNotificationInfos = ArrayList<NotificationInfo>()

    private var mVibrator: Vibrator? = null

    private var mVibeEffect: VibrationEffect? = null

    fun notifyChat(username: String, nickname: String, msg: String, lightWeightNotify: Boolean)
    {
        if (!Preferences.isNotificationEnabled()) {
            log("notification for chat $username is not enabled")
            return
        }

        if (Preferences.hasNotificationFilters()) {
            if (Preferences.getNotificationFilters().contains(msg)) {
                return
            }
        }

        val snippet = mContext.getString(R.string.new_messages_notify) + ' ' + nickname

        val intent = getDefaultIntent()
        intent?.action = Intent.ACTION_VIEW
        intent?.putExtra("address", username)
        intent?.addCategory(KeanuConstants.IMPS_CATEGORY)

        notify(username, nickname, snippet, msg, intent, lightWeightNotify, R.drawable.ic_discuss,true)
    }

    fun notifyLocked() {
        val intent = Intent(mContext, DummyActivity::class.java)

        val title = mContext.getString(R.string.app_name)
        val message = mContext.getString(R.string.account_setup_pers_now_title)

        notify(message, title, message, message, intent, true,
                R.drawable.ic_lock_outline_black_18dp, false)
    }

    fun notifyNewDevice(device: CryptoDeviceInfo) {
        // Don't notify multiple times about the same device!
        if (mNotificationInfos.any { it.sender == device.deviceId }) return

        val intent = Intent(mContext, NewDeviceActivity::class.java)
        intent.putExtra(VerificationManager.EXTRA_USER_ID, device.userId)
        intent.putExtra(VerificationManager.EXTRA_DEVICE_ID, device.deviceId)

        val title = mContext.getString(R.string.app_name)
        val message = mContext.getString(R.string._logged_into_your_account_Please_confirm_it_was_you_, device.displayName() ?: device.deviceId)

        notify(device.deviceId, title, message, message, intent, true, R.drawable.ic_devices, true)
    }

    fun dismissNotifications(providerId: Long) {
        for (info in mNotificationInfos.toTypedArray()) {
            if (info.mProviderId == providerId) {
                mNotificationManager?.cancel(info.computeNotificationId())
                mNotificationInfos.remove(info)
            }
        }
    }

    fun dismissChatNotification(username: String) {
        for (info in mNotificationInfos.toTypedArray()) {
            if (info.sender != null && info.sender == username) {
                mNotificationManager?.cancel(info.computeNotificationId())
                mNotificationInfos.remove(info)
            }
        }
    }

    fun notify(title: String?, tickerText: String?, message: String?,
               intent: Intent, lightWeightNotify: Boolean, doVibrateSound: Boolean)
    {
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP

        val info = NotificationInfo(-1)
        info.setInfo("", title, message, null, intent)

        mNotificationManager?.notify(info.computeNotificationId(),
                info.createNotification(tickerText, lightWeightNotify, R.drawable.ic_stat_status,
                        null, intent, doVibrateSound))
    }

    private fun notify(sender: String, title: String, tickerText: String, message: String,
                       intent: Intent?, lightWeightNotify: Boolean, iconSmall: Int,
                       doVibrateSound: Boolean, iconLarge: Bitmap? = null)
    {
        intent?.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP

        var info: NotificationInfo? = null

        synchronized(mNotificationInfos) {
            info = if (sender != "") mNotificationInfos.firstOrNull { it.sender == sender } else null

            if (info == null) {
                info = NotificationInfo(-1)
                info?.setInfo(sender, title, message, null, intent)
            }
            else {
                val sb = StringBuilder()

                if (!TextUtils.isEmpty(info?.bigMessage)) {
                    sb.append(info?.bigMessage).append('\n')
                }
                else if (!TextUtils.isEmpty(info?.message)) {
                    sb.append(info?.message).append('\n')
                }

                sb.append(message)

                info?.setInfo(sender, title, message, sb.toString(), intent)
            }

            info?.let { mNotificationInfos.add(it) }
        }

        info?.let {
            mNotificationManager?.notify(it.computeNotificationId(),
                it.createNotification(tickerText, lightWeightNotify, iconSmall, iconLarge, intent, doVibrateSound))
        }
    }

    fun getDefaultIntent(): Intent? {
        var intent: Intent? = null

        try {
            intent = Intent(mContext, Class.forName(defaultMainClass))
        }
        catch (e: ClassNotFoundException) {
            e.printStackTrace()
        }

        return intent
    }

    internal inner class NotificationInfo(val mProviderId: Long) {

        var sender: String? = null
            private set

        var title: String? = null
            private set

        var message: String? = null
            private set

        var bigMessage: String? = null
            private set

        var intent: Intent? = null
            private set

        fun computeNotificationId(): Int {
            return if (title == null) mProviderId.toInt() else sender.hashCode()
        }

        fun setInfo(sender: String?, title: String?, message: String?, bigMessage: String?, intent: Intent?) {
            this.sender = sender
            this.title = title
            this.message = message
            this.bigMessage = bigMessage
            this.intent = intent
        }

        fun createNotification(tickerText: String?, lightWeightNotify: Boolean, icon: Int, largeIcon: Bitmap?,
                               intent: Intent?, doNotifyVibrateSound: Boolean): Notification
        {
            val builder = NotificationCompat.Builder(mContext, KeanuConstants.NOTIFICATION_CHANNEL_ID_MESSAGE)

            builder
                    .setSmallIcon(icon)
                    .setTicker(if (lightWeightNotify) null else tickerText)
                    .setWhen(System.currentTimeMillis())
                    .setLights(-0x670000, 300, 1000)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setContentIntent(PendingIntent.getActivity(mContext, UNIQUE_INT_PER_CALL++, intent, PendingIntent.FLAG_UPDATE_CURRENT))
                    .setAutoCancel(true)

            if (doNotifyVibrateSound) {
                if (Preferences.getNotificationSound()) {
                    try {
                        // Play sound
                        val serviceIntent = Intent(mContext, SoundService::class.java)
                        serviceIntent.action = "ACTION_START_PLAYBACK"
                        serviceIntent.putExtra("SOUND_URI", Preferences.getNotificationRingtoneUri().toString())
                        mContext.startService(serviceIntent)
                    }
                    catch (e: IllegalStateException) {
                        //error can't start services in the background
                    }
                }

                if (Preferences.getNotificationVibrate() && isVibrateOn) {
                    // Play vibration
                    if (mVibrator == null) {
                        mVibrator = mContext.getSystemService(Context.VIBRATOR_SERVICE) as? Vibrator
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            mVibeEffect = VibrationEffect.createWaveform(VIBRATION_TIMINGS, -1)
                        }
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        mVibrator?.vibrate(mVibeEffect)
                    } else {
                        @Suppress("DEPRECATION")
                        mVibrator?.vibrate(VIBRATE_PATTERN, -1)
                    }
                }
            }

            if (!TextUtils.isEmpty(bigMessage)) {
                // Sets the big view "big text" style and supplies the
                // text (the user's reminder message) that will be displayed
                // in the detail area of the expanded notification
                // These calls are ignored by the support library for
                // pre-4.1 devices.
                builder.setStyle(NotificationCompat.BigTextStyle()
                        .bigText(bigMessage))
            }

            if (largeIcon != null) builder.setLargeIcon(largeIcon)

            return builder.build()
        }
    }

    private var mAudioManager: AudioManager? = null

    private val isVibrateOn: Boolean
        get() {
            if (mAudioManager == null) mAudioManager = mContext.getSystemService(Context.AUDIO_SERVICE) as? AudioManager

            return when (mAudioManager?.ringerMode) {
                AudioManager.RINGER_MODE_NORMAL -> (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1
                        && Settings.System.getInt(mContext.contentResolver, Settings.System.VIBRATE_WHEN_RINGING, 0) == 1)
                AudioManager.RINGER_MODE_SILENT -> false
                AudioManager.RINGER_MODE_VIBRATE -> true
                else -> false
            }
        }
}