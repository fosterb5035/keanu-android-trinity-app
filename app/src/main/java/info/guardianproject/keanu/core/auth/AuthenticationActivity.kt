package info.guardianproject.keanu.core.auth

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import info.guardianproject.keanu.core.model.Server
import info.guardianproject.keanu.core.verification.VerificationManager
import info.guardianproject.keanuapp.ImApp
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.ActivityAuthenticationBinding
import info.guardianproject.keanuapp.ui.BaseActivity
import info.guardianproject.keanuapp.ui.onboarding.OnboardingAccount
import org.matrix.android.sdk.api.MatrixCallback
import org.matrix.android.sdk.api.auth.AuthenticationService
import org.matrix.android.sdk.api.auth.data.HomeServerConnectionConfig
import org.matrix.android.sdk.api.auth.data.LoginFlowResult
import org.matrix.android.sdk.api.auth.registration.RegistrationResult
import org.matrix.android.sdk.api.auth.registration.RegistrationWizard
import org.matrix.android.sdk.api.auth.registration.Stage
import org.matrix.android.sdk.api.failure.Failure
import org.matrix.android.sdk.api.session.Session
import java.net.MalformedURLException
import java.net.URL
import java.util.*

class AuthenticationActivity : BaseActivity(), View.OnClickListener, CompoundButton.OnCheckedChangeListener,
        MatrixCallback<RegistrationResult> {

    companion object {
        const val EXTRA_IS_SIGN_UP = "extra_is_sign_up"
        const val EXTRA_ONBOARDING_ACCOUNT = "extra_onboarding_account"
    }

    private var isSignUp = false

    private lateinit var mBinding: ActivityAuthenticationBinding

    private var mHomeServer: URL? = null

    private val mApp: ImApp?
        get() = super.getApplication() as? ImApp

    private val mAuthService: AuthenticationService?
        get() = mApp?.matrix?.authenticationService()

    private val mRegistrationWizard: RegistrationWizard?
        get() = mApp?.matrix?.authenticationService()?.getRegistrationWizard()

    private val mDeviceName: String
        get() {
            val id = applicationInfo.labelRes

            val name = if (id == 0) applicationInfo.nonLocalizedLabel.toString() else getString(id)

            return if (name.isEmpty()) "Keanu3" else name
        }

    private var mOnboardingAccount: OnboardingAccount? = null

    private val mAcceptTerms = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == RESULT_OK) {
            mRegistrationWizard?.acceptTerms(this)
        }
        else {
            // User abort.
            mOnboardingAccount = null

            mBinding.btSignIn.isEnabled = true
            mBinding.progress.visibility = View.GONE
        }
    }

    private val mResolveCaptcha = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        val response = it.data?.getStringExtra(RecaptchaActivity.EXTRA_RESPONSE)

        if (it.resultCode == RESULT_OK && response != null) {
            mRegistrationWizard?.performReCaptcha(response, this)
        }
        else {
            // User abort.
            mOnboardingAccount = null

            mBinding.btSignIn.isEnabled = true
            mBinding.progress.visibility = View.GONE
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = ActivityAuthenticationBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        isSignUp = intent.getBooleanExtra(EXTRA_IS_SIGN_UP, false)

        if (!isSignUp) {
            mBinding.tvUsernameHint.visibility = View.GONE
            (mBinding.etConfirm.parent as? View)?.visibility = View.GONE
            mBinding.tvPasswordHint.visibility = View.GONE
        }

        mBinding.tvError.visibility = View.GONE

        mBinding.swAdvancedOptions.setOnCheckedChangeListener(this)
        mBinding.swCustomIdServer.setOnCheckedChangeListener(this)
        mBinding.swKeyBackup.setOnCheckedChangeListener(this)
        mBinding.swChoosePassword.setOnCheckedChangeListener(this)

        val host = Server.getServers(this).firstOrNull()?.domain

        if (host != null) {
            mHomeServer = URL("https", host, "")

            mBinding.etHomeServer.setText(mHomeServer.toString())
            mBinding.etIdServer.setText(mHomeServer.toString())
        }

        mBinding.swKeyBackup.isChecked = true

        showAdvancedOptions(false)
        showCustomServer(false)
        showChoosePassword(true)
        showBackupPassword(false)

        mBinding.btSignIn.setOnClickListener(this)
        mBinding.btSignIn.text = getString(if (isSignUp) R.string.Sign_Up else R.string.sign_in)

        mBinding.progress.visibility = View.GONE

        supportActionBar?.title = getString(if (isSignUp) R.string.Sign_Up else R.string.sign_in)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()

            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {
        val username = mBinding.etUserName.text?.toString()
        val password = mBinding.etPassword.text?.toString()
        var homeServer = mHomeServer
        var idServer = mHomeServer
        val backupPassword: String?


        if (username.isNullOrEmpty()) {
            mBinding.etUserName.error = getString(R.string.This_field_may_not_be_empty_)

            return
        }

        mBinding.etUserName.error = null

        if (password.isNullOrEmpty()) {
            mBinding.etPassword.error = getString(R.string.This_field_may_not_be_empty_)

            return
        }

        mBinding.etPassword.error = null

        if (isSignUp) {
            if (password.length < 8) {
                mBinding.etPassword.error = getString(R.string.Password_is_too_short_)

                return
            }

            if (mBinding.etConfirm.text.toString() != password) {
                mBinding.etPassword.error = getString(R.string.Fields_are_not_equal_)
                mBinding.etConfirm.error = getString(R.string.Fields_are_not_equal_)

                return
            }

            mBinding.etPassword.error = null
            mBinding.etConfirm.error = null
        }

        if (mBinding.swAdvancedOptions.isChecked) {
            try {
                homeServer = URL(mBinding.etHomeServer.text.toString())

                if (homeServer.host.isNullOrEmpty()) throw MalformedURLException()
            }
            catch (e: Exception) {
                mBinding.etHomeServer.error = getString(R.string.Malformed_URL_)

                return
            }

            mBinding.etHomeServer.error = null

            if (mBinding.swCustomIdServer.isChecked) {
                try {
                    idServer = URL(mBinding.etIdServer.text.toString())

                    if (idServer.host.isNullOrEmpty()) throw MalformedURLException()
                }
                catch (e: Exception) {
                    mBinding.etIdServer.error = getString(R.string.Malformed_URL_)

                    return
                }

                mBinding.etIdServer.error = null
            }

            if (mBinding.swKeyBackup.isChecked) {
                if (mBinding.swChoosePassword.isChecked) {
                    backupPassword = mBinding.etKeyBackupPassword.text?.toString()

                    if (backupPassword.isNullOrEmpty()) {
                        mBinding.etKeyBackupPassword.error = getString(R.string.This_field_may_not_be_empty_)

                        return
                    }

                    if (isSignUp && backupPassword.length < 8) {
                        mBinding.etKeyBackupPassword.error = getString(R.string.Password_is_too_short_)

                        return
                    }

                    if (mBinding.etKeyBackupConfirm.text.toString() != backupPassword) {
                        mBinding.etKeyBackupPassword.error = getString(R.string.Fields_are_not_equal_)
                        mBinding.etKeyBackupConfirm.error = getString(R.string.Fields_are_not_equal_)

                        return
                    }

                    mBinding.etKeyBackupPassword.error = null
                    mBinding.etKeyBackupConfirm.error = null
                }
            }
        }

        if (homeServer == null) return

        mBinding.btSignIn.isEnabled = false
        mBinding.progress.visibility = View.VISIBLE

        if (isSignUp) {
            register(username, password, homeServer.toString(), idServer?.toString())
        }
        else {
            login(username, password, homeServer.toString(), idServer?.toString())
        }
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        when (buttonView) {
            mBinding.swAdvancedOptions -> showAdvancedOptions(isChecked)

            mBinding.swCustomIdServer -> showCustomServer(isChecked)

            mBinding.swKeyBackup -> showChoosePassword(isChecked)

            mBinding.swChoosePassword -> showBackupPassword(isChecked)
        }
    }

    private fun showAdvancedOptions(toggle: Boolean) {
        mBinding.llAdvancedItems.visibility = if (toggle) View.VISIBLE else View.GONE
    }

    private fun showCustomServer(toggle: Boolean) {
        mBinding.etIdServer.visibility = if (toggle) View.VISIBLE else View.GONE
    }

    private fun showChoosePassword(toggle: Boolean) {
        mBinding.swChoosePassword.visibility = if (toggle) View.VISIBLE else View.GONE

        if (!toggle) {
            mBinding.swChoosePassword.isChecked = false
        }
    }

    private fun showBackupPassword(toggle: Boolean) {
        mBinding.llKeyBackupPassword.visibility = if (toggle) View.VISIBLE else View.GONE
    }

    private fun showError(throwable: Throwable) {
        var msg = when (throwable) {
            is Failure.ServerError -> {
                throwable.error.message
            }
            is Exception -> {
                // If we don't cast, Throwable#localizedMessage will return nothing.
                throwable.localizedMessage
            }
            else -> {
                throwable.localizedMessage
            }
        }

        if (TextUtils.isEmpty(msg)) msg = throwable.message
        if (TextUtils.isEmpty(msg)) msg = throwable.toString()
        if (TextUtils.isEmpty(msg)) msg = getString(R.string.error)

        mBinding.tvError.text = msg
        mBinding.tvError.visibility = View.VISIBLE

        mBinding.btSignIn.isEnabled = true
        mBinding.progress.visibility = View.GONE
    }

    /**
     * Registration requests callback.
     */
    override fun onSuccess(data: RegistrationResult) {
        super.onSuccess(data)

        if (data is RegistrationResult.Success) {
            mApp?.matrixSession = data.session

            var password = mOnboardingAccount?.password

            if (password != null) {
                // Opportunistically try to enable cross signing, if not done, yet.
                // Fail silently, if not possible here.
                // User will be asked to enable cross-signing on device verification, otherwise.
                VerificationManager.instance.enableCrossSigning(password)
            }

            if (mBinding.swKeyBackup.isChecked) {
                if (!mBinding.etKeyBackupPassword.text.isNullOrEmpty()) {
                    password = mBinding.etKeyBackupPassword.text.toString()
                }

                if (password != null) {
                    // Opportunistically try to restore the latest backup or create a new key backup,
                    // if not enabled, yet.
                    // The password is either a user-provided password which is an
                    // explicit password for the key backup, or, if none provided,
                    // the user account password.
                    // Fail silently, if not possible here.
                    // User needs to enable key backup in DevicesActivity, otherwise,
                    // or will automatically be connected to key backup, when doing
                    // an interactive verification with a device which already has it.
                    VerificationManager.instance.restoreOrAddNewBackup(password)
                }
            }

            val intent = Intent()
            intent.putExtra(EXTRA_ONBOARDING_ACCOUNT, mOnboardingAccount)
            setResult(RESULT_OK, intent)

            finish()
            return
        }

        if (data is RegistrationResult.FlowResponse) {
            val stage = data.flowResult.missingStages.firstOrNull { it.mandatory }

            // Not success, but no mandatory stages left to fulfill?
            // Time to finally create the account!
            if (stage == null) {
                val username = mOnboardingAccount?.username ?: return
                val password = mOnboardingAccount?.password ?: return

                mRegistrationWizard?.createAccount(username, password, mDeviceName, this)

                return
            }

            when (stage) {
                is Stage.ReCaptcha -> {
                    val intent = Intent(this, RecaptchaActivity::class.java)
                    intent.putExtra(RecaptchaActivity.EXTRA_PUBLIC_KEY, stage.publicKey)
                    intent.putExtra(RecaptchaActivity.EXTRA_HOME_SERVER, mOnboardingAccount?.server)
                    mResolveCaptcha.launch(intent)
                }

                is Stage.Email -> {
                    // We're not currently supporting this, yet.
                    showError(Throwable(getString(R.string.internal_server_error)))
                }
                is Stage.Msisdn -> {
                    // We're not currently supporting this, yet.
                    showError(Throwable(getString(R.string.internal_server_error)))
                }

                is Stage.Dummy -> {
                    mRegistrationWizard?.dummy(this)
                }

                is Stage.Terms -> {
                    val intent = Intent(this, TermsActivity::class.java)
                    intent.putExtra(TermsActivity.EXTRA_TERMS, TermsActivityArgument(stage.policies.toLocalizedLoginTerms(Locale.getDefault().language)))
                    mAcceptTerms.launch(intent)
                }

                is Stage.Other -> {
                    // No idea, what we have to do here. Must be a future server with
                    // additional requirements this SDK version cannot fulfill.
                    showError(Throwable(getString(R.string.internal_server_error)))
                }
            }
        }
    }

    /**
     * Registration requests callback.
     */
    override fun onFailure(failure: Throwable) {
        showError(failure)
    }

    private fun getLoginFlow(homeServer: String, idServer: String?, callback: MatrixCallback<LoginFlowResult>) {
        var hServer = Uri.parse(homeServer)

        if (hServer.scheme?.toLowerCase(Locale.ROOT)?.startsWith("http") != true) {
            hServer = hServer.buildUpon().scheme("https").build()
        }

        val builder = HomeServerConnectionConfig.Builder()

        try {
            builder.withHomeServerUri(hServer)

            if (idServer != null) {
                var iServer = Uri.parse(idServer)

                if (iServer.scheme?.toLowerCase(Locale.ROOT)?.startsWith("http") != true) {
                    iServer = iServer.buildUpon().scheme("https").build()
                }

                builder.withIdentityServerUri(iServer)
            }
        }
        catch (failure: Throwable) {
            callback.onFailure(failure)
            return
        }

        mAuthService?.getLoginFlow(builder.build(), object : MatrixCallback<LoginFlowResult> {

            override fun onSuccess(data: LoginFlowResult) {
                if (data is LoginFlowResult.Success) {
                    if (!data.isLoginAndRegistrationSupported) {
                        callback.onFailure(RuntimeException(getString(R.string.not_implemented)))
                    }
                }

                callback.onSuccess(data)
            }

            override fun onFailure(failure: Throwable) {
                callback.onFailure(failure)
            }
        })
    }

    private fun register(username: String, password: String, homeServer: String, idServer: String?)
    {
        mApp?.logout()

        getLoginFlow(homeServer, idServer, object : MatrixCallback<LoginFlowResult> {

            override fun onSuccess(data: LoginFlowResult) {
                mOnboardingAccount = OnboardingAccount(username, username,
                        (data as? LoginFlowResult.Success)?.homeServerUrl, password)

                mRegistrationWizard?.getRegistrationFlow(this@AuthenticationActivity)
            }

            override fun onFailure(failure: Throwable) {
                showError(failure)
            }
        })
    }

    private fun login(username: String, password: String, homeServer: String, idServer: String?)
    {
        mApp?.logout()

        getLoginFlow(homeServer, idServer, object : MatrixCallback<LoginFlowResult> {

            override fun onSuccess(data: LoginFlowResult) {
                mAuthService?.getLoginWizard()?.login(username, password, mDeviceName, object : MatrixCallback<Session> {

                    val server = (data as? LoginFlowResult.Success)?.homeServerUrl

                    override fun onSuccess(data: Session) {
                        mOnboardingAccount = OnboardingAccount(username, username, server, password)

                        this@AuthenticationActivity.onSuccess(RegistrationResult.Success(data))
                    }

                    override fun onFailure(failure: Throwable) {
                        showError(failure)
                    }
                })
            }

            override fun onFailure(failure: Throwable) {
                showError(failure)
            }
        })
    }
}