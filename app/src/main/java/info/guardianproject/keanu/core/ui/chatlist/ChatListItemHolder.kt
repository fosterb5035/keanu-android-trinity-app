package info.guardianproject.keanu.core.ui.chatlist

import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanuapp.databinding.ChatListItemBinding

class ChatListItemHolder(private val mBinding: ChatListItemBinding) : RecyclerView.ViewHolder(mBinding.root) {
    val line1 get() = mBinding.line1
    val line2 get() = mBinding.line2
    val line3 get() = mBinding.line3
    val statusText get() = mBinding.statusText
    val avatar get() = mBinding.avatar
    val statusIcon get() = mBinding.statusIcon
    val messageContainer get() = mBinding.messageContainer
    val mediaThumbnail get() = mBinding.mediaThumbnail
    val markerUnread get() = mBinding.markerUnread
}
