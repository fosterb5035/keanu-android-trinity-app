package info.guardianproject.keanu.core.ui.gallery

import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.text.TextUtils
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import info.guardianproject.keanu.core.util.SecureMediaStore
import info.guardianproject.keanuapp.ImApp
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.ui.widgets.MediaViewHolder
import org.matrix.android.sdk.api.session.content.ContentUrlResolver
import timber.log.Timber
import java.io.File
import java.io.FileInputStream
import java.net.URLConnection

/**
 * Created by n8fr8 on 2/12/16.
 */
class GalleryMediaViewHolder(view: View?, private val context: Context) : MediaViewHolder(view) {

    interface GalleryMediaViewHolderListener {
        fun onImageClicked(viewHolder: GalleryMediaViewHolder, image: Uri)
    }

    private var mListener: GalleryMediaViewHolderListener? = null

    fun setListener(listener: GalleryMediaViewHolderListener?) {
        this.mListener = listener
    }

    fun bind(mimeType: String?, body: String?) {
        if (mimeType != null) {
            mContainer.visibility = View.VISIBLE
            val mediaUri = Uri.parse(body?.split(" ".toRegex())?.toTypedArray()?.firstOrNull() ?: "")
            showMediaThumbnail(mimeType, mediaUri)
        }
        else {
            mContainer.visibility = View.GONE
        }
    }

    private fun showMediaThumbnail(mimeType: String, mediaUri: Uri) {
        @Suppress("NAME_SHADOWING")
        var mimeType = mimeType

        /* Guess the MIME type in case we received a file that we can display or play*/
        if (mimeType.isEmpty() || mimeType.startsWith("application")) {
            val guessed = URLConnection.guessContentTypeFromName(mediaUri.toString())

            if (guessed.isNotEmpty()) {
                mimeType = if (TextUtils.equals(guessed, "video/3gpp")) "audio/3gpp" else guessed
            }
        }

        mMediaThumbnail.setOnClickListener { mListener?.onImageClicked(this, mediaUri) }

        mMediaThumbnail.visibility = View.VISIBLE

        when {
            mimeType.startsWith("image/") || mimeType.startsWith("video/") -> {
                setImageThumbnail(mediaUri)
            }

            mimeType.startsWith("audio") -> {
                mMediaThumbnail.setImageResource(R.drawable.media_audio_play)
                mMediaThumbnail.setBackgroundColor(Color.TRANSPARENT)
            }

            else -> {
                mMediaThumbnail.setImageResource(R.drawable.file_unknown) // generic file icon
            }
        }
    }

    /**
     * @param mediaUri
     */
    private fun setImageThumbnail(mediaUri: Uri) {
        // Pair this holder to the uri. If the holder is recycled, the pairing is broken.
        mMediaUri = mediaUri

        mMediaThumbnail.setImageResource(R.drawable.icon_download)

        val glideRb = Glide.with(context).asDrawable()

        when {
            SecureMediaStore.isVfsUri(mediaUri) -> {
                try {
                    val path = mediaUri.path
                    val fileMedia = if (path != null) File(path) else null

                    if (fileMedia?.exists() == true) {
                        Timber.d("VFS loading: $mediaUri")

                        glideRb.load(FileInputStream(fileMedia))
                    }
                    else {
                        Timber.d("VFS file not found: $mediaUri")

                        glideRb.load(R.drawable.broken_image_large)
                    }
                }
                catch (e: Exception) {
                    Timber.e(e, "Unable to load thumbnail.")

                    glideRb.load(R.drawable.broken_image_large)
                }
            }

            mediaUri.scheme == "asset" -> {
                val assetPath = "file:///android_asset/" + mediaUri.path?.substring(1)

                glideRb.load(assetPath)
            }

            mediaUri.scheme == "mxc" -> {
                val url = ImApp.sImApp?.matrixSession?.contentUrlResolver()?.resolveThumbnail(
                        mediaUri.toString(),
                        256.coerceAtLeast(mMediaThumbnail.measuredWidth),
                        256.coerceAtLeast(mMediaThumbnail.measuredHeight),
                        ContentUrlResolver.ThumbnailMethod.CROP)

                if (url != null) {
                    glideRb.load(Uri.parse(url))
                }
                else {
                    glideRb.load(R.drawable.broken_image_large)
                }
            }

            else -> {
                glideRb.load(mediaUri)
            }
        }

        glideRb.apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE))
                .into(mMediaThumbnail)
    }
}