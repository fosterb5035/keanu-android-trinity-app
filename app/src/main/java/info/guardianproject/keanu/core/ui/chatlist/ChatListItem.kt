package info.guardianproject.keanu.core.ui.chatlist

import agency.tango.android.avatarview.AvatarPlaceholder
import android.content.Context
import android.net.Uri
import android.os.Build
import android.text.Html
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.ImageView
import info.guardianproject.keanu.core.util.SecureMediaStore
import info.guardianproject.keanuapp.R
import info.guardianproject.keanu.core.util.GlideUtils
import org.ocpsoft.prettytime.PrettyTime
import java.io.File
import java.util.*

class ChatListItem(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

    private val mPrettyTime = PrettyTime(currentLocale)

    private var mRoomName: String? = null

    private var mLastMediaUri: Uri? = null

    fun bind(holder: ChatListItemHolder, address: String, roomName: String?, avatarUrl: String?,
             message: String?, messageDate: Long, messageType: String?, showChatMsg: Boolean)
    {
        mRoomName = roomName ?: address.split(":".toRegex()).toTypedArray()[0]
        holder.line1.text = mRoomName
        holder.statusIcon.visibility = GONE
        holder.avatar.visibility = VISIBLE

        GlideUtils.loadAvatar(context, avatarUrl, AvatarPlaceholder(roomName), holder.avatar)

        if (showChatMsg && message?.isNotBlank() == true) {
            holder.mediaThumbnail.scaleType = ImageView.ScaleType.FIT_CENTER
            holder.mediaThumbnail.visibility = GONE

            holder.line2.text = ""

            val vPath = message.split(" ".toRegex()).toTypedArray()[0]

            if (SecureMediaStore.isVfsUri(vPath) || SecureMediaStore.isContentUri(vPath)) {
                if (messageType.isNullOrEmpty()) {
                    holder.mediaThumbnail.visibility = VISIBLE
                    holder.mediaThumbnail.setImageResource(R.drawable.ic_attach_file_black_36dp)
                    holder.mediaThumbnail.scaleType = ImageView.ScaleType.CENTER_INSIDE
                    holder.line2.text = ""
                }
                else if (messageType.startsWith("image")) {
                    holder.mediaThumbnail.visibility = VISIBLE

                    holder.mediaThumbnail.scaleType = if (messageType == "image/png")
                        ImageView.ScaleType.FIT_CENTER else ImageView.ScaleType.CENTER_CROP

                    setThumbnail(holder, Uri.parse(vPath), true)
                    holder.line2.visibility = GONE
                }
                else if (messageType.startsWith("audio")) {
                    mLastMediaUri = null
                    holder.mediaThumbnail.visibility = VISIBLE
                    holder.mediaThumbnail.scaleType = ImageView.ScaleType.CENTER_INSIDE
                    holder.mediaThumbnail.setImageResource(R.drawable.ic_volume_up_black_24dp)
                    holder.line2.text = ""
                }
                else if (messageType.startsWith("video")) {
                    val uriMedia = Uri.parse(vPath)
                    val fileThumb = File(uriMedia.path + ".thumb.jpg")

                    if (!fileThumb.exists()) {
                        mLastMediaUri = null
                        holder.mediaThumbnail.visibility = VISIBLE
                        holder.mediaThumbnail.scaleType = ImageView.ScaleType.CENTER_INSIDE
                        holder.mediaThumbnail.setImageResource(R.drawable.video256)
                        holder.line2.text = ""
                    }
                    else {
                        holder.mediaThumbnail.visibility = VISIBLE
                        holder.mediaThumbnail.scaleType = ImageView.ScaleType.CENTER_CROP
                        setThumbnail(holder, Uri.parse("$vPath.thumb.jpg"), true)
                        holder.line2.visibility = GONE
                    }
                }
                else if (messageType.startsWith("application")) {
                    mLastMediaUri = null
                    holder.mediaThumbnail.visibility = VISIBLE
                    holder.mediaThumbnail.scaleType = ImageView.ScaleType.CENTER_INSIDE
                    holder.mediaThumbnail.setImageResource(R.drawable.ic_attach_file_black_36dp)
                    holder.line2.text = ""
                }
                else {
                    mLastMediaUri = null
                    holder.mediaThumbnail.visibility = GONE
                    holder.line2.text = messageType
                }
            }
            else if (message.startsWith("/")) {
                val cmd = message.toString().substring(1)

                if (cmd.startsWith("sticker")) {
                    val cmds = cmd.split(":".toRegex()).toTypedArray()

                    val mediaUri = Uri.parse("asset://" + cmds[1])
                    mLastMediaUri = null
                    setThumbnail(holder, mediaUri, false)

                    holder.line2.visibility = GONE
                    holder.mediaThumbnail.scaleType = ImageView.ScaleType.FIT_CENTER
                    holder.mediaThumbnail.visibility = VISIBLE
                }
            }
            else if (message.startsWith(":")) {
                val cmds = message.split(":".toRegex()).toTypedArray()

                try {
                    val stickerParts = cmds[1].split("-".toRegex()).toTypedArray()
                    val folder = stickerParts[0]
                    val name = StringBuffer()

                    for (i in 1 until stickerParts.size) {
                        name.append(stickerParts[i])
                        if (i + 1 < stickerParts.size) name.append('-')
                    }

                    val stickerPath = "stickers/$folder/$name.png"

                    // Make sure sticker exists.
                    val afd = context.assets.openFd(stickerPath)
                    afd.length
                    afd.close()

                    // Now setup the new URI for loading local sticker asset.
                    val mediaUri = Uri.parse("asset://localhost/$stickerPath")
                    mLastMediaUri = null
                    setThumbnail(holder, mediaUri, false)

                    holder.line2.visibility = GONE
                    holder.mediaThumbnail.scaleType = ImageView.ScaleType.FIT_CENTER
                }
                catch (e: Exception) {
                    holder.line2.text = renderHtml(message)
                }
            }
            else {
                holder.mediaThumbnail.visibility = GONE
                holder.line2.visibility = VISIBLE
                holder.line2.text = renderHtml(message)
            }

            if (messageDate != -1L) {
                val dateLast = Date(messageDate)
                holder.statusText.text = mPrettyTime.format(dateLast)
            }
            else {
                holder.statusText.text = ""
            }
        } else {
            holder.line2.text = ""
            holder.mediaThumbnail.visibility = GONE
        }

        holder.line1.visibility = VISIBLE
    }

    private fun setThumbnail(aHolder: ChatListItemHolder, mediaUri: Uri, centerCrop: Boolean) {
        if (mLastMediaUri?.path == mediaUri.path) return

        mLastMediaUri = mediaUri
        aHolder.mediaThumbnail.visibility = VISIBLE

        aHolder.mediaThumbnail.scaleType = if (centerCrop) ImageView.ScaleType.CENTER_CROP else ImageView.ScaleType.FIT_CENTER

        GlideUtils.loadImageFromUri(context, mediaUri, aHolder.mediaThumbnail)
    }

    @Suppress("DEPRECATION")
    private val currentLocale: Locale
        get() = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            resources.configuration.locales[0]
        }
        else {
            resources.configuration.locale
        }

    private fun renderHtml(source: String) : String {
        return try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY).toString()
            } else {
                @Suppress("DEPRECATION")
                Html.fromHtml(source).toString()
            }
        }
        catch (exception: RuntimeException) {
            ""
        }
    }
}