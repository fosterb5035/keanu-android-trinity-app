package info.guardianproject.keanu.core.ui.room

import android.Manifest.permission.*
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Color
import android.media.AudioManager
import android.media.MediaRecorder
import android.net.Uri
import android.os.*
import android.view.*
import android.widget.ImageButton
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.engine.impl.GlideEngine
import info.guardianproject.keanu.core.type.CustomTypefaceEditText
import info.guardianproject.keanu.core.util.SnackbarExceptionHandler
import info.guardianproject.keanu.core.util.SystemServices
import info.guardianproject.keanuapp.ImApp
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.AwesomeActivityDetailBinding
import info.guardianproject.keanuapp.ui.BaseActivity
import info.guardianproject.keanuapp.ui.camera.CameraActivity
import info.guardianproject.keanuapp.ui.stories.StoryEditorActivity
import info.guardianproject.keanuapp.ui.widgets.ShareRequest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.matrix.android.sdk.api.session.content.ContentAttachmentData
import timber.log.Timber
import java.io.File
import java.util.*

open class RoomActivity : BaseActivity() {

    companion object {
        @SuppressLint("Range")
        fun getContrastColor(colorIn: Int): Int {
            val y = ((299 * Color.red(colorIn) + 587 * Color.green(colorIn) + 114 * Color.blue(colorIn)) / 1000).toDouble()
            return if (y >= 128) Color.BLACK else Color.WHITE
        }

        const val REQUEST_ADD_MEDIA = 666
    }

    var roomId: String = ""
        private set

    private var mMediaRecorder: MediaRecorder? = null
    private var mAudioFilePath: File? = null
    var isAudioRecording = false

    private val mApp: ImApp?
        get() = application as? ImApp

    var backButtonHandler: View.OnClickListener? = null

    private lateinit var mBinding: AwesomeActivityDetailBinding

    open val root: ViewGroup
        get() = mBinding.root

    open val toolbar: Toolbar
        get() = mBinding.toolbar

    open val mainContent: ViewGroup
        get() = mBinding.mainContent

    open val inputLayout: ViewGroup
        get() = mBinding.inputLayout

    open val historyView: RecyclerView
        get() = mBinding.history

    open val composeMessage: CustomTypefaceEditText
        get() = mBinding.composeMessage

    open val btnSend: ImageButton
        get() = mBinding.btnSend

    open val btnMic
        get() = mBinding.btnMic

    open val btnDeleteVoice
        get() = mBinding.btnDeleteVoice

    open val btnAttach
        get() = mBinding.btnAttach

    open val attachView
        get() = mBinding.attachPanel

    open val btnAttachSticker
        get() = mBinding.btnAttachSticker

    open val btnAttachPicture
        get() = mBinding.btnAttachPicture

    open val btnTakePicture
        get() = mBinding.btnTakePicture

    open val btnAttachAudio
        get() = mBinding.btnAttachAudio

    open val btnAttachFile
        get() = mBinding.btnAttachFile

    open val btnCreateStory
        get() = mBinding.btnCreateStory

    open val audioRecordView
        get() = mBinding.recordView

    open val typingView
        get() = mBinding.tvTyping

    open val stickerBox
        get() = mBinding.stickerBox

    open val stickerPager
        get() = mBinding.stickerPager

    open val mediaPreviewCancel
        get() = mBinding.mediaPreviewCancel

    open val joinGroupView
        get() = mBinding.joinGroupView

    open val btnJoinAccept
        get() = mBinding.btnJoinAccept

    open val btnJoinDecline
        get() = mBinding.btnJoinDecline

    open val roomJoinTitle
        get() = mBinding.roomJoinTitle

    open val mediaPreviewContainer
        get() = mBinding.mediaPreviewContainer

    open val mediaPreview
        get() = mBinding.mediaPreview

    protected open val roomView: RoomView by lazy {
        RoomView(this, roomId)
    }

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO + SnackbarExceptionHandler(root))
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        mBinding = AwesomeActivityDetailBinding.inflate(layoutInflater)
        setContentView(root)

        if (intent != null) {
            roomId = intent.getStringExtra("address") ?: ""

            if (roomId.isNotEmpty()) {
                if (intent.getBooleanExtra("isNew", false)) {
                    roomView.showGroupInfo()
                    intent.putExtra("isNew", false)
                }

                mApp?.currentForegroundRoom = roomId
            }
        }

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        applyStyleForToolbar()

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)

        roomView.setSelected(true)
    }

    override fun applyStyleForToolbar() {
        supportActionBar?.title = roomView.title

        // Not set color.
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val themeColorHeader = prefs.getInt("themeColor", -1)
        var themeColorText = prefs.getInt("themeColorText", -1)
        val themeColorBg = prefs.getInt("themeColorBg", -1)

        if (themeColorHeader != -1) {
            if (themeColorText == -1) themeColorText = getContrastColor(themeColorHeader)

            if (Build.VERSION.SDK_INT >= 21) {
                window.navigationBarColor = themeColorHeader
                window.statusBarColor = themeColorHeader
                window.setTitleColor(themeColorText)
            }

            toolbar.setBackgroundColor(themeColorHeader)
            toolbar.setTitleTextColor(themeColorText)
        }

        if (themeColorBg != -1) {
            mainContent.setBackgroundColor(themeColorBg)
            inputLayout.setBackgroundColor(themeColorBg)

            if (themeColorText != -1) {
                composeMessage.setTextColor(themeColorText)
                composeMessage.setHintTextColor(themeColorText)
            }
        }
    }

    override fun onResume() {
        super.onResume()

        roomView.setSelected(true)
        mApp?.currentForegroundRoom = roomId
    }

    override fun onPause() {
        super.onPause()

        roomView.setSelected(false)
        mApp?.currentForegroundRoom = ""
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)

        setIntent(intent)
        // Set last read date now!
    }

    override fun onBackPressed() {
        if (backButtonHandler != null) {
            backButtonHandler?.onClick(null)
            return
        }

        super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_conversation_detail_group, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                if (backButtonHandler != null) {
                    backButtonHandler?.onClick(null)
                    return true
                }

                finish()
                return true
            }

            R.id.menu_end_conversation -> {
                roomView.closeChatSession()
                finish()
                return true
            }

            R.id.menu_group_info -> {
                roomView.showGroupInfo()
                return true
            }

            R.id.menu_live_mode -> {
                startLiveMode()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_ADD_MEDIA) {
            if (resultCode != RESULT_OK) return

            for (mediaUri in Matisse.obtainResult(data)) {
                handleSendDelete(mediaUri, null)
            }
        }
        else {
            // Need to keep this for the Matisse library, which didn't get an overhaul in the
            // last 1.5 years. Grml.
            @Suppress("DEPRECATION")
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    fun startImagePicker() {
        if (ActivityCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, READ_EXTERNAL_STORAGE))
            {
                Snackbar.make(historyView, R.string.grant_perms, Snackbar.LENGTH_LONG)
                        .setAction(R.string.ok) {
                            startImagePicker()
                        }
                        .show()
            }
            else {
                registerForActivityResult(ActivityResultContracts.RequestPermission()) {
                    if (it) startImagePicker()
                }
                        .launch(READ_EXTERNAL_STORAGE)
            }
        }
        else {
            Matisse.from(this)
                    .choose(MimeType.ofImage())
                    .countable(true)
                    .maxSelectable(100)
                    .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                    .thumbnailScale(0.85f)
                    .imageEngine(GlideEngine())
                    .showPreview(false)
                    .forResult(REQUEST_ADD_MEDIA)
        }
    }

    fun startPhotoTaker() {
        if (ActivityCompat.checkSelfPermission(this, CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, CAMERA))
            {
                Snackbar.make(historyView, R.string.grant_perms, Snackbar.LENGTH_LONG)
                        .setAction(R.string.ok) {
                            startPhotoTaker()
                        }
                        .show()
            }
            else {
                registerForActivityResult(ActivityResultContracts.RequestPermission()) {
                    if (it) startPhotoTaker()
                }
                        .launch(CAMERA)
            }
        }
        else {
            val intent = Intent(this, CameraActivity::class.java)
            intent.putExtra(CameraActivity.SETTING_ONE_AND_DONE, true)

            mTakePicture.launch(intent)
        }
    }

    fun startFilePicker(mimeType: String = "*/*") {
        if (ActivityCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, READ_EXTERNAL_STORAGE))
            {
                Snackbar.make(historyView, R.string.grant_perms, Snackbar.LENGTH_LONG)
                        .setAction(R.string.ok) {
                            startFilePicker(mimeType)
                        }
                        .show()
            }
            else {
                registerForActivityResult(ActivityResultContracts.RequestPermission()) {
                    if (it) startFilePicker(mimeType)
                }
                        .launch(CAMERA)
            }
        }
        else {
            // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
            // browser.
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)

            // Filter to only show results that can be "opened", such as a
            // file (as opposed to a list of contacts or timezones)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            // Filter to show only images, using the image MIME data type.
            // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
            // To search for all documents available via installed storage providers,
            // it would be "*/*".
            intent.type = mimeType

            mSendFile.launch(Intent.createChooser(intent, getString(R.string.invite_share)))
        }
    }

    fun startStoryEditor() {
        mCreateStory.launch(Intent(this, StoryEditorActivity::class.java))
    }

    fun startAudioRecording() {
        if (ActivityCompat.checkSelfPermission(this, RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, RECORD_AUDIO))
            {
                Snackbar.make(historyView, R.string.grant_perms, Snackbar.LENGTH_LONG)
                        .setAction(R.string.ok) {
                            startAudioRecording()
                        }
                        .show()
            }
            else {
                registerForActivityResult(ActivityResultContracts.RequestPermission()) {
                    if (it) startAudioRecording()
                }
                        .launch(RECORD_AUDIO)
            }
        }
        else {
            val am = getSystemService(AUDIO_SERVICE) as? AudioManager

            if (am?.mode == AudioManager.MODE_NORMAL) {
                val fileName = UUID.randomUUID().toString().substring(0, 8) + ".m4a"
                mAudioFilePath = File(filesDir, fileName)

                mMediaRecorder = MediaRecorder()
                mMediaRecorder?.setAudioSource(MediaRecorder.AudioSource.MIC)
                mMediaRecorder?.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
                mMediaRecorder?.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)

                // Maybe we can modify these in the future, or allow people to tweak them.
                mMediaRecorder?.setAudioChannels(1)
                mMediaRecorder?.setAudioEncodingBitRate(22050)
                mMediaRecorder?.setAudioSamplingRate(64000)
                mMediaRecorder?.setOutputFile(mAudioFilePath!!.absolutePath)

                try {
                    isAudioRecording = true
                    mMediaRecorder?.prepare()
                    mMediaRecorder?.start()
                } catch (e: Exception) {
                    Timber.e(e, "couldn't start audio")
                }
            }
        }
    }

    fun stopAudioRecording(send: Boolean) {
        if (mMediaRecorder != null && mAudioFilePath != null && isAudioRecording) {
            try {
                mMediaRecorder?.stop()
                mMediaRecorder?.reset()
                mMediaRecorder?.release()

                if (send) {
                    val uriAudio = Uri.fromFile(mAudioFilePath)
                    handleSendDelete(uriAudio, "audio/mp4")
                } else {
                    mAudioFilePath?.delete()
                }
            } catch (ise: IllegalStateException) {
                Timber.w(ise, "error stopping audio recording")
            } catch (re: RuntimeException) {
                // Stop can fail so we should catch this here.

                Timber.w(re, "error stopping audio recording")
            }

            isAudioRecording = false
        }
    }

    @Suppress("BlockingMethodInNonBlockingContext")
    fun handleSendDelete(contentUri: Uri, defaultType: String?) {
        val sb = Snackbar.make(historyView, R.string.upgrade_progress_action, Snackbar.LENGTH_INDEFINITE)
        sb.show()

        mCoroutineScope.launch {
            try {
                // Import
                val info = SystemServices.getFileInfoFromURI(this@RoomActivity, contentUri)
                val type = info.type ?: defaultType ?: ""

                // Send
                val sent = handleSendData(contentUri, type)

                // Not deleting if not sent.
                if (sent && info.stream != null) info.stream.close()
            } catch (e: Exception) {
                Timber.e(e, "error sending file")
            }

            lifecycleScope.launch {
                sb.dismiss()
            }
        }
    }

    fun sendShareRequest(request: ShareRequest) {
        handleSendDelete(request.media, request.mimeType)
    }

    private fun startLiveMode() {
        val intent = Intent(this, StoryActivity::class.java)
        intent.putExtra("address", roomId)
        intent.putExtra(StoryActivity.ARG_CONTRIBUTOR_MODE, true)

        startActivity(intent)
    }

    private fun handleSendData(uri: Uri, mimeType: String): Boolean {
        val path = uri.path ?: return false

        val type = when {
            mimeType.startsWith("image") -> ContentAttachmentData.Type.IMAGE

            mimeType.startsWith("audio") -> ContentAttachmentData.Type.AUDIO

            mimeType.startsWith("video") -> ContentAttachmentData.Type.VIDEO

            else -> ContentAttachmentData.Type.FILE
        }

        val mediaFile = File(path)

        val cad = ContentAttachmentData(mediaFile.length(), 0L, mediaFile.lastModified(),
                0L, 0L, 0, mediaFile.name, uri, mimeType, type)

        mApp?.matrixSession?.getRoom(roomId)?.sendMedia(cad, true, setOf(roomId))

        return true // Was not sent.
    }

    private val mSendFile by lazy {
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode != RESULT_OK) return@registerForActivityResult

            val uri = it.data?.data ?: return@registerForActivityResult

            handleSendDelete(uri, it.data?.type)
        }
    }

    private val mTakePicture by lazy {
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode != RESULT_OK) return@registerForActivityResult

            val request = ShareRequest()
            request.deleteFile = false
            request.resizeImage = false
            request.importContent = false
            request.media = it.data?.data
            request.mimeType = it.data?.type

            if (request.mimeType == "image/jpeg") {
                try {
                    roomView.setMediaDraft(request)
                } catch (e: Exception) {
                    Timber.w(e, "error setting media draft")
                }
            } else {
                sendShareRequest(request)
            }
        }
    }

    private val mCreateStory by lazy {
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode != RESULT_OK) return@registerForActivityResult

            val request = ShareRequest()
            request.deleteFile = false
            request.resizeImage = false
            request.importContent = false
            request.media = it.data?.data
            request.mimeType = it.data?.type

            sendShareRequest(request)
        }
    }

    val requestImageView by lazy {
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode != RESULT_OK) return@registerForActivityResult

            if (it.data?.hasExtra("resendImageUri") == true) {
                val request = ShareRequest()
                request.deleteFile = false
                request.resizeImage = false
                request.importContent = false
                request.media = Uri.parse(it.data?.getStringExtra("resendImageUri"))
                request.mimeType = it.data?.getStringExtra("resendImageMimeType")

                sendShareRequest(request)
            }

            roomView.requeryCursor()
        }
    }
}
