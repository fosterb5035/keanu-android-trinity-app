package info.guardianproject.keanu.core.ui.chatlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import info.guardianproject.keanu.core.util.SnackbarExceptionHandler
import info.guardianproject.keanuapp.ImApp
import info.guardianproject.keanuapp.MainActivity
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.AwesomeFragmentMessageListBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.matrix.android.sdk.api.session.Session

class ChatListFragment : Fragment() {

    companion object {
        const val ARCHIVED_TAG = "archived"
    }

    private var mAdapter: ChatListAdapter? = null

    private lateinit var mBinding: AwesomeFragmentMessageListBinding

    private val mSession: Session?
        get() = (activity?.application as? ImApp)?.matrixSession

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO + SnackbarExceptionHandler(mBinding.root))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        mBinding = AwesomeFragmentMessageListBinding.inflate(inflater)

        mBinding.emptyViewImage.setOnClickListener { (activity as? MainActivity)?.inviteContact() }
        setupRecyclerView(mBinding.recyclerView)

        return mBinding.root
    }

    private fun setupRecyclerView(recyclerView: RecyclerView) {
        mAdapter = ChatListAdapter(this)

        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = mAdapter

        var helper: ItemTouchHelper? = null

        helper = ItemTouchHelper(ChatListAdapter.ArchiveTouchHelperCallback { position ->
            val itemId = mAdapter?.getItemId(position) ?: return@ArchiveTouchHelperCallback
            val roomId = mAdapter?.get(itemId)?.roomId ?: return@ArchiveTouchHelperCallback
            val room = mSession?.getRoom(roomId) ?: return@ArchiveTouchHelperCallback

            if (showArchived) {
                mCoroutineScope.launch {
                    room.deleteTag(ARCHIVED_TAG)
                }

                Snackbar.make(recyclerView, getString(R.string.action_unarchived), Snackbar.LENGTH_SHORT)
                        .show()
            } else {
                mCoroutineScope.launch {
                    room.addTag(ARCHIVED_TAG, null)
                }

                Snackbar.make(recyclerView, getString(R.string.action_archived), Snackbar.LENGTH_LONG)
                        .setAction(R.string.action_undo) {
                            mCoroutineScope.launch {
                                room.deleteTag(ARCHIVED_TAG)
                            }
                        }
                        .show()
            }

            // Dirty workaround to fix broken views when they get reused.
            // See https://stackoverflow.com/questions/31787272
            helper?.attachToRecyclerView(null)
            helper?.attachToRecyclerView(recyclerView)
        })

        helper.attachToRecyclerView(recyclerView)

        updateListVisibility()
    }

    fun updateListVisibility() {
        if (mAdapter?.itemCount == 0) {
            mBinding.recyclerView.visibility = View.GONE
            mBinding.emptyView.visibility = View.VISIBLE
            mBinding.emptyViewImage.visibility = View.VISIBLE
        }
        else if (mBinding.recyclerView.visibility == View.GONE) {
            mBinding.recyclerView.visibility = View.VISIBLE
            mBinding.emptyView.visibility = View.GONE
            mBinding.emptyViewImage.visibility = View.GONE
        }
    }

    var showArchived: Boolean
        get() = mAdapter?.showArchived ?: false
        set(value) {
            mAdapter?.showArchived = value
        }

    fun doSearch(searchString: String?) {
        mAdapter?.filter(searchString)
    }

    fun refresh() {
        mAdapter?.refresh()
    }
}