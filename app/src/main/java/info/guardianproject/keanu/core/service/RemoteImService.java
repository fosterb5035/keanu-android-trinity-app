package info.guardianproject.keanu.core.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.text.TextUtils;

import androidx.core.app.NotificationCompat;
import androidx.lifecycle.LiveData;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import org.jetbrains.annotations.NotNull;
import org.matrix.android.sdk.api.MatrixCallback;
import org.matrix.android.sdk.api.failure.GlobalError;
import org.matrix.android.sdk.api.session.Session;
import org.matrix.android.sdk.api.session.events.model.Event;
import org.matrix.android.sdk.api.session.events.model.EventType;
import org.matrix.android.sdk.api.session.room.Room;
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams;
import org.matrix.android.sdk.api.session.room.model.ReadReceipt;
import org.matrix.android.sdk.api.session.room.model.RoomSummary;
import org.matrix.android.sdk.api.session.room.model.message.MessageAudioContent;
import org.matrix.android.sdk.api.session.room.model.message.MessageContent;
import org.matrix.android.sdk.api.session.room.model.message.MessageFileContent;
import org.matrix.android.sdk.api.session.room.model.message.MessageImageContent;
import org.matrix.android.sdk.api.session.room.model.message.MessageVideoContent;
import org.matrix.android.sdk.api.session.room.model.message.MessageWithAttachmentContent;
import org.matrix.android.sdk.api.session.room.notification.RoomNotificationState;
import org.matrix.android.sdk.api.session.room.sender.SenderInfo;
import org.matrix.android.sdk.api.session.room.timeline.EventTypeFilter;
import org.matrix.android.sdk.api.session.room.timeline.Timeline;
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent;
import org.matrix.android.sdk.api.session.room.timeline.TimelineEventFilters;
import org.matrix.android.sdk.api.session.room.timeline.TimelineSettings;
import org.matrix.android.sdk.internal.crypto.model.rest.EncryptedFileInfo;
import org.matrix.android.sdk.internal.di.MoshiProvider;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Time;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import info.guardianproject.keanu.core.model.Message;
import info.guardianproject.keanu.core.util.Debug;
import info.guardianproject.keanu.core.util.LogCleaner;
import info.guardianproject.keanu.core.util.MatrixDownloader;
import info.guardianproject.keanuapp.ImApp;
import info.guardianproject.keanuapp.R;

import static info.guardianproject.keanu.core.KeanuConstants.NOTIFICATION_CHANNEL_ID_SERVICE;


public class RemoteImService extends Service implements ImService {

    private static final String PREV_CONNECTIONS_TRAIL_TAG = "prev_connections";
    private static final String CONNECTIONS_TRAIL_TAG = "connections";
    private static final String PREV_SERVICE_CREATE_TRAIL_TAG = "prev_service_create";
    private static final String SERVICE_CREATE_TRAIL_KEY = "service_create";

    public static IRemoteImService mImService;
    private boolean isFirstTime = true;

    private StatusBarNotifier mStatusBarNotifier;

    private Session mSession;


    EventTypeFilter[] LIMIT_DISPLAYABLE_TYPES = {
            new EventTypeFilter (EventType.MESSAGE, null),
            new EventTypeFilter (EventType.STICKER, null),
            new EventTypeFilter (EventType.ENCRYPTED, null),
            new EventTypeFilter (EventType.REACTION, null),

    };



    private ImApp mApp;

    NotificationCompat.Builder mNotifyBuilder;
    private final static int notifyId = 7;
    
    private static final String TAG = "RemoteImService";

    public static void debug(String msg) {
       LogCleaner.debug(TAG, msg);
    }

    public static void debug(String msg, Exception e) {
        LogCleaner.error(TAG, msg, e);
    }

    @Override
    public void onCreate() {
        debug("ImService started");

        mStatusBarNotifier = new StatusBarNotifier(this);
        mApp = (ImApp)getApplication();

        initService();

        initNotificationListener();
    }

    private void initService ()
    {

        mImService = mBinder;

        final String prev = Debug.getTrail(this, SERVICE_CREATE_TRAIL_KEY);
        if (prev != null)
            Debug.recordTrail(this, PREV_SERVICE_CREATE_TRAIL_TAG, prev);
        Debug.recordTrail(this, SERVICE_CREATE_TRAIL_KEY, new Date());
        final String prevConnections = Debug.getTrail(this, CONNECTIONS_TRAIL_TAG);
        if (prevConnections != null)
            Debug.recordTrail(this, PREV_CONNECTIONS_TRAIL_TAG, prevConnections);
        Debug.recordTrail(this, CONNECTIONS_TRAIL_TAG, "0");

        Debug.onServiceStart();

    }

    private final HashMap<String, Boolean> mRoomNotifyMap = new HashMap<>();
    private final HashMap<String, Timeline> mTimelines = new HashMap<>();

    private void initNotificationListener ()
    {
        if (mSession == null)
            mSession = mApp.getMatrixSession();

        if (mSession != null)
        {
            RoomSummaryQueryParams.Builder params = new RoomSummaryQueryParams.Builder();
            List<RoomSummary> listRooms = mSession.getRoomSummaries(params.build());

            for (RoomSummary roomSummary : listRooms) {
                final Room room = mSession.getRoom(roomSummary.getRoomId());

                if (room == null)
                    return;

                if (!mTimelines.containsKey(room.getRoomId())) {
                    LiveData<RoomNotificationState> roomNotify = room.getLiveRoomNotificationState();
                    roomNotify.observeForever(roomNotificationState -> {
                        boolean doNotify = roomNotificationState == RoomNotificationState.ALL_MESSAGES;
                        mRoomNotifyMap.put(room.getRoomId(), doNotify);
                    });

                    List<EventTypeFilter> allowedTypes = Arrays.asList(LIMIT_DISPLAYABLE_TYPES);

                    TimelineSettings tSettings = new TimelineSettings(10,
                            new TimelineEventFilters(true, true, true, true, allowedTypes),
                            true);

                    Timeline timeline = room.createTimeline(null, tSettings);

                    timeline.addListener(new Timeline.Listener() {
                        @Override
                        public void onTimelineUpdated(@NotNull List<TimelineEvent> list) {

                        }

                        @Override
                        public void onTimelineFailure(@NotNull Throwable throwable) {

                        }

                        @Override
                        public void onNewTimelineEvents(@NotNull List<String> list) {

                            for (String tEventId : list) {
                                TimelineEvent tEvent = room.getTimeLineEvent(tEventId);
                                if (tEvent != null)
                                    handleTimelineEventNotification(tEvent);
                            }
                        }
                    });

                    timeline.start();

                    mTimelines.put(room.getRoomId(),timeline);
                }
            }

        }
    }

    private void handleTimelineEventNotification (TimelineEvent tEvent)
    {
        String roomId = tEvent.getRoot().getRoomId();

        if (tEvent != null && (!mApp.isRoomInForeground(roomId))) {

            if (mRoomNotifyMap.containsKey(roomId) && (!mRoomNotifyMap.get(roomId)))
                return; //don't notify
            SenderInfo sInfo = tEvent.getSenderInfo();

            if (mSession.getMyUserId().equals(sInfo.getUserId()))
                return; //don't notify

            for (ReadReceipt rr : tEvent.getReadReceipts()) {
                //already read don't notify
                if (rr.getUser().getUserId().equals(mSession.getMyUserId()))
                    break;
            }

            Event event = tEvent.getRoot();
            Map<String, Object> messageContent = event.getClearContent();

            Moshi moshi = MoshiProvider.INSTANCE.providesMoshi();
            JsonAdapter<MessageContent> ja = moshi.adapter(MessageContent.class);
            MessageContent mc = ja.fromJsonValue(event.getClearContent());

            RoomSummary roomSummary = mSession.getRoomSummary(event.getRoomId());

            if (roomSummary == null)
                return;

            String room = roomSummary.getDisplayName();
            String body = (String) messageContent.get("body");

            final String messageSep = ": ";

            EncryptedFileInfo efi = null;

            if (mc instanceof MessageImageContent) {
                body = sInfo.getDisplayName() + messageSep + getString(R.string.sent_image);
                efi = ((MessageImageContent)mc).getEncryptedFileInfo();

            } else if (mc instanceof MessageAudioContent) {
                body = sInfo.getDisplayName() + messageSep + getString(R.string.sent_audio);
                efi = ((MessageAudioContent)mc).getEncryptedFileInfo();

            } else if (mc instanceof MessageVideoContent) {
                body = sInfo.getDisplayName() + messageSep + getString(R.string.sent_video);
                efi = ((MessageVideoContent)mc).getEncryptedFileInfo();

            } else if (mc instanceof MessageFileContent) {
                body = sInfo.getDisplayName() + messageSep + getString(R.string.sent_file);
                efi = ((MessageFileContent)mc).getEncryptedFileInfo();

            } else if (!TextUtils.isEmpty(body)) {
                body = sInfo.getDisplayName() + messageSep + body;
            }

            if (mc instanceof MessageWithAttachmentContent)
                downloadMedia(RemoteImService.this, mSession, (MessageWithAttachmentContent)mc);

            if (roomId != null && room != null && body != null)
                mStatusBarNotifier.notifyChat(event.getRoomId(), room, body, true);

        }

    }



    private Notification getForegroundNotification() {

        mNotifyBuilder = new NotificationCompat.Builder(this,NOTIFICATION_CHANNEL_ID_SERVICE)
            .setContentTitle(getString(R.string.app_name))
            .setSmallIcon(R.drawable.notify_app);

      //  mNotifyBuilder.setVisibility(NotificationCompat.VISIBILITY_PRIVATE);
      //  mNotifyBuilder.setPriority(NotificationCompat.PRIORITY_MIN);
        mNotifyBuilder.setOngoing(true);
        mNotifyBuilder.setAutoCancel(false);
        mNotifyBuilder.setWhen(System.currentTimeMillis());

        mNotifyBuilder.setContentText(getString(R.string.app_unlocked));

        Intent notificationIntent = mStatusBarNotifier.getDefaultIntent();
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mNotifyBuilder.setContentIntent(contentIntent);

        return mNotifyBuilder.build();

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (isFirstTime) {
            startForeground(notifyId, getForegroundNotification());
            isFirstTime = false;
        }

        if (intent != null)
        {
            if (ImServiceConstants.EXTRA_CHECK_SHUTDOWN.equals((intent.getAction())))
            {
                //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              shutdown();
                stopSelf();
            }

            initNotificationListener();
        }

        return START_STICKY;
    }



    @Override
    public void onLowMemory() {

        debug ("onLowMemory()!");
    }


    /**
     * Release memory when the UI becomes hidden or when system resources become low.
     * @param level the memory-related event that was raised.
     */
    public void onTrimMemory(int level) {

        debug ("OnTrimMemory: " + level);

        // Determine which lifecycle or system event was raised.
        switch (level) {

            case TRIM_MEMORY_UI_HIDDEN:

                /*
                   Release any UI objects that currently hold memory.

                   "release your UI resources" is actually about things like caches.
                   You usually don't have to worry about managing views or UI components because the OS
                   already does that, and that's why there are all those callbacks for creating, starting,
                   pausing, stopping and destroying an activity.
                   The user interface has moved to the background.
                */

                break;

            case TRIM_MEMORY_RUNNING_MODERATE:
            case TRIM_MEMORY_RUNNING_LOW:
            case TRIM_MEMORY_RUNNING_CRITICAL:

                /*
                   Release any memory that your app doesn't need to run.

                   The device is running low on memory while the app is running.
                   The event raised indicates the severity of the memory-related event.
                   If the event is TRIM_MEMORY_RUNNING_CRITICAL, then the system will
                   begin killing background processes.
                */


                break;

            case TRIM_MEMORY_BACKGROUND:
            case TRIM_MEMORY_MODERATE:
            case TRIM_MEMORY_COMPLETE:

                /*
                   Release as much memory as the process can.
                   The app is on the LRU list and the system is running low on memory.
                   The event raised indicates where the app sits within the LRU list.
                   If the event is TRIM_MEMORY_COMPLETE, the process will be one of
                   the first to be terminated.
                */



                break;

            default:
                /*
                  Release any non-critical data structures.
                  The app received an unrecognized memory level value
                  from the system. Treat this as a generic low-memory message.
                */
                break;
        }
    }

    @Override
    public void onDestroy() {
    }



    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private final IRemoteImService.Stub mBinder = new IRemoteImService.Stub() {

        @Override
        public void addConnectionCreatedListener(IConnectionCreationListener listener) {

        }

        @Override
        public void removeConnectionCreatedListener(IConnectionCreationListener listener) {

        }

        @Override
        public IImConnection createConnection(long providerId, long accountId) {
            return null;
        }

        @Override
        public boolean removeConnection(long providerId, long accountId) {
            return true;
        }

        @Override
        public List getActiveConnections() {
            return null;
        }

        @Override
        public IImConnection getConnection(long providerId, long accountId) {
            return null;
        }

        @Override
        public void dismissNotifications(long providerId) {
            mStatusBarNotifier.dismissNotifications(providerId);
        }

        @Override
        public void dismissChatNotification(long providerId, String username) {
            mStatusBarNotifier.dismissChatNotification(username);
        }

        @Override
        public boolean unlockOtrStore (String password)
        {
            return true;
        }


        @Override
        public void setKillProcessOnStop (boolean killProcessOnStop)
        {

        }

        @Override
        public void enableDebugLogging (boolean debug)
        {
            Debug.DEBUG_ENABLED = debug;
        }

        @Override
        public void updateStateFromSettings()  {


        }

        @Override
        public void shutdownAndLock ()
        {
        }
    };


    @Override
    public void showToast(CharSequence text, int duration) {

    }

    public static void downloadMedia (Context context, Session session, MessageWithAttachmentContent message)
    {

        if (!session.fileService().isFileInCache(message))
            session.fileService().downloadFile(message, new MatrixCallback<File>() {
                @Override
                public void onSuccess(File file) {

                }

                @Override
                public void onFailure(@NotNull Throwable throwable) {

                }
            });

        /**
        if (!TextUtils.isEmpty(downloadUrl)) {

            File outputFile = new File(context.getCacheDir(), Uri.parse(efi.getUrl()).getLastPathSegment());


            session.fileService().
            if ((!outputFile.exists())||outputFile.length()==0) {

                try {
                    //should only do this for files under a certain length
                    MatrixDownloader.decryptAttachmentAsync(new URL(downloadUrl), efi, new FileOutputStream(outputFile),
                            new MatrixDownloader.Listener()
                            {
                                @Override
                                public void decryptionComplete(EncryptedFileInfo encryptedFileInfo) {

                                }
                            });

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        }**/
    }
}
