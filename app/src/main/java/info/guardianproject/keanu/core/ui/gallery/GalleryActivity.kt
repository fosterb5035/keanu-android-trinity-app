package info.guardianproject.keanu.core.ui.gallery

import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.*
import android.webkit.MimeTypeMap
import androidx.core.net.toUri
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.core.util.SnackbarExceptionHandler
import info.guardianproject.keanuapp.ImApp
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.AwesomeActivityGalleryBinding
import info.guardianproject.keanuapp.databinding.GalleryItemViewBinding
import info.guardianproject.keanuapp.nearby.NearbyShareActivity
import info.guardianproject.keanuapp.ui.BaseActivity
import info.guardianproject.keanuapp.ui.widgets.ImageViewActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.matrix.android.sdk.api.MatrixCallback
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.message.MessageWithAttachmentContent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import java.io.File
import java.lang.ref.WeakReference

class GalleryActivity : BaseActivity() {

    private val mSession: Session?
        get() = (application as? ImApp)?.matrixSession

    private lateinit var mBinding: AwesomeActivityGalleryBinding

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO + SnackbarExceptionHandler(mBinding.root))
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = AwesomeActivityGalleryBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        setTitle(R.string.photo_gallery)

        updateUi()

        setupRecyclerView(mBinding.recyclerView)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        applyStyleForToolbar()
    }

    override fun applyStyleForToolbar() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val selColor = prefs.getInt("themeColor", -1)

        if (selColor != -1) {
            if (Build.VERSION.SDK_INT >= 21) {
                window.navigationBarColor = selColor
                window.statusBarColor = selColor
            }

            supportActionBar?.setBackgroundDrawable(ColorDrawable(selColor))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }

            R.id.menu_message_nearby -> {
                startActivity(Intent(this, NearbyShareActivity::class.java))
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_gallery, menu)
        return true
    }

    private fun setupRecyclerView(recyclerView: RecyclerView) {
        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)

        val query = RoomSummaryQueryParams.Builder()
        query.memberships = listOf(Membership.INVITE, Membership.JOIN)

        val adapter = GalleryAdapter(this)
        recyclerView.adapter = adapter

        mCoroutineScope.launch {
            mSession?.getRoomSummaries(query.build())?.forEach {
                val room = mSession?.getRoom(it.roomId) ?: return@forEach

                room.getAttachmentMessages().forEach loop@ { event ->
                    val content = event.getLastMessageContent() as? MessageWithAttachmentContent ?: return@loop

                    mSession?.fileService()?.downloadFile(content, object : MatrixCallback<File> {

                        override fun onSuccess(data: File) {
                            adapter.add(data.toUri())

                            lifecycleScope.launch {
                                updateUi()
                            }
                        }
                    })
                }
            }
        }
    }

    private fun updateUi() {
        val hasNoMedia = mBinding.recyclerView.adapter?.itemCount == 0

        mBinding.recyclerView.visibility = if (hasNoMedia) View.GONE else View.VISIBLE
        mBinding.emptyView.visibility = if (hasNoMedia) View.VISIBLE else View.GONE
        mBinding.emptyViewImage.visibility = mBinding.emptyView.visibility
    }

    class GalleryAdapter(context: Context?) : RecyclerView.Adapter<GalleryMediaViewHolder>(), GalleryMediaViewHolder.GalleryMediaViewHolderListener {

        private val mContext = WeakReference(context)

        private val mFiles = ArrayList<Uri>()

        fun add(file: Uri) {
            mFiles.add(file)
            notifyItemInserted(mFiles.size - 1)
        }

        override fun getItemCount(): Int {
            return mFiles.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryMediaViewHolder {
            val binding = GalleryItemViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)

            return GalleryMediaViewHolder(binding.root, parent.context)
        }

        override fun onBindViewHolder(holder: GalleryMediaViewHolder, position: Int) {
            holder.bind(getMimeType(mFiles[position]), mFiles[position].toString())
            holder.setListener(this)
        }

        override fun onImageClicked(viewHolder: GalleryMediaViewHolder, image: Uri) {
            val context = mContext.get() ?: return

            val intent = Intent(context, ImageViewActivity::class.java)
            intent.putExtra(ImageViewActivity.URIS, mFiles)
            intent.putExtra(ImageViewActivity.MIME_TYPES, ArrayList(mFiles.map { getMimeType(it) }))
            intent.putExtra(ImageViewActivity.CURRENT_INDEX, 0.coerceAtLeast(mFiles.indexOf(image)))

            context.startActivity(intent)
        }

        private fun getMimeType(file: Uri): String? {
            return MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(file.toString()))
        }
    }
}